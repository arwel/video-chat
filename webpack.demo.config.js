const isProd = process.argv.indexOf('-p') > -1;
const {resolve, join} = require('path');
const mode = isProd ? 'production' : 'development';
const {VueLoaderPlugin} = require('vue-loader');
const {DefinePlugin} = require('webpack');

const SHARED_DIR = resolve(join(__dirname, 'shared'));
const SRC_DIR = resolve(join(__dirname, 'client', 'src'));
const DIST_DIR = resolve(join(__dirname, 'client', 'dist'));

module.exports = {
  resolve: {
    extensions: ['.ts', '.js', '.vue'],
    alias: {
      '@': SRC_DIR,
      '@js': join(SRC_DIR, 'js'),
      '@vue': join(SRC_DIR, 'vue'),
      '@shared': SHARED_DIR,
    },
  },
  entry: {
    'main.bundle': join(DIST_DIR, 'js', 'main.demo.js'),
  },
  output: {
    path: join(DIST_DIR, 'js'),
    chunkFilename: 'modules/[name].module.js',
    filename: '[name].js',
    publicPath: isProd ? 'https://assets-video.moi.health/js/' : 'https://assets-video.loc/js/',
  },
  mode: mode,
  watch: !isProd,
  devtool: isProd ? false : 'source-map',
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: [SRC_DIR],
        options: {
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader',
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          },
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [SRC_DIR],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
  ],
};
