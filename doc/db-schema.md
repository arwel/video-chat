### Server
  - title **string**
  - description **string**
  - url **string** (unique)
  - ip **string** (unique)
  - token **string** (unique, hashed ?)
  - privateKeyPath **string**
  - publicKeyPath **string**
  - createdAt **Date** (default now)
 

### WebHook
  - key **string**
  - serverId **integer** -> Server(serverId, id)
  - url **string**


### User
  - nick **string**
  - email **string**
  - serverId **integer** -> Server(serverId, id)
  - createdAt **Date** (default now)


### Room
  - title **string**
  - slug **string**
  - userId **integer** -> User(userId, id)
  - createdAt **Date** (default now)
  - expiredAt **Date**
  - serverId **integer** -> Server(serverId, id)
  
  
### RoomUser
  - roomId **integer** -> Room(roomId, id)
  - userId **integer** -> User(userId, id)
