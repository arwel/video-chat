### List
  list of items
  
### IncomingCallList <- List
### RoomUserList <- List
  Show title of room + owner + count
  - call to room
  
### RoomUserItem
  Represents user in room.
  (username, is active, [call])
  
  - call 
  
### CallStackItem 
  Represents each person with actions. Remove from stack after user reaction.
 
  (Incoming call from $user$ [accept]  [decline])
    
  - accept
  - decline
  
  
### VideoWidget
  Show user nickname, video.
  
  - mute
  - close connection [can be disabled]
  - request master [can be disabled]
  - request fullscreen
  - blacklist (???)
  - choose video/audio source (?)
  
  
### Grid
  Represents grid of video widgets + has master frame.  
  Each widget can request master frame.
