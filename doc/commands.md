### DB Commands:
  - Create DB: **npx sequelize-cli db:create**
  - Create Model and Migration: **npx sequelize-cli model:generate --name <MODEL_NAME> --attributes <attribute_name_1>:<attribute_type_1>,<attribute_name_2>:<attribute_type_2>**
  - Migrate **npx sequelize-cli db:migrate**
