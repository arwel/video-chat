const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const {readFileSync, writeFileSync} = require('fs');
const {resolve, join} = require('path');
const {createServer} = require('https');
const {ENV_PROD} = require('../shared/const/env');

const DB_PATH = resolve(join(__dirname, 'db.json'));

const db = JSON.parse(readFileSync(DB_PATH));

app.use(bodyParser.json());
app.use(cors());

app.get('/:path.html', (request, response) => {
  const {path} = request.params;
  response.sendFile(join(__dirname, `${path}.html`));
});

app.post('/get-user-data', (request, response) => {
  const user = db.users[request.body.id];

  if (!user) {
    response.status(404).json({
      errors: [
        'User not found',
      ],
    });
    return;
  }

  response.json(user);
});

app.post('/set-server-token', (request, response) => {
  const {token} = request.body;
  db.serverOpts.token = token;

  writeFileSync(DB_PATH, JSON.stringify(db));

  response.json({});
});

app.post('/set-user-jwt', (request, response) => {
  const {id, token} = request.body;
  const user = db.users[id];

  if (!user) {
    response.status(404).json({
      errors: [
        'User not found',
      ],
    });
    return;
  }

  user.token = token;

  writeFileSync(DB_PATH, JSON.stringify(db));

  response.json({});
});

app.get('/init', (request, response) => {
  const user = db.users[request.query.id];

  if (!user) {
    response.status(404).json({
      errors: [
        'User not found',
      ],
    });
    return;
  }

  response.json({
    jwt: user.token,
    serverToken: db.serverOpts.token,
  });
});

const server = process.env.ENV === ENV_PROD
  ? createServer({
    key: readFileSync(process.env.FAKE_SSL_KEY_PATH),
    cert: readFileSync(process.env.FAKE_SSL_CERT_PATH),
  }, app)
  : app;

server.listen(process.env.PORT || 3535);
