const isProd = process.argv.indexOf('-p') > -1;
const {resolve, join} = require('path');
const mode = isProd ? 'production' : 'development';
const {VueLoaderPlugin} = require('vue-loader');
const {DefinePlugin} = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
// const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');

const SHARED_DIR = resolve(join(__dirname, 'shared'));
const SRC_DIR = resolve(join(__dirname, 'client', 'src'));
const DIST_DIR = resolve(join(__dirname, 'client', 'dist'));

module.exports = {
  resolve: {
    extensions: ['.ts', '.js', '.vue'],
    alias: {
      '@': SRC_DIR,
      '@js': join(SRC_DIR, 'js'),
      '@vue': join(SRC_DIR, 'vue'),
      '@shared': SHARED_DIR,
    },
  },
  entry: {
    main: join(SRC_DIR, 'js', 'main.js'),
  },
  output: {
    path: join(DIST_DIR, 'js'),
    chunkFilename: 'modules/[name].module.js',
    filename: '[name].js',
    library: 'Chater',
    libraryTarget: 'umd',
    publicPath: isProd ? '/build/js/dreamext-chater/' : 'https://assets-video.loc/js/',
  },
  mode: mode,
  watch: !isProd,
  devtool: isProd ? false : 'source-map',
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: [SRC_DIR],
        options: {
          loaders: {
            scss: 'css-loader!sass-loader!vue-style-loader',
            sass: 'css-loader!sass-loader?indentedSyntax!vue-style-loader',
          },
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [SRC_DIR],
        exclude: [
          DIST_DIR,
          resolve(join(__dirname, 'server')),
          resolve(join(__dirname, 'node_modules')),
          resolve(join(__dirname, 'shared')),
          resolve(join(__dirname, 'fake')),
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              sourceMap: !isProd,
            },
          },
        ],
      },
      {
        test: /\.sass$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: !isProd,
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: !isProd,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: 'postcss.config.js',
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !isProd,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new DefinePlugin({
      ENV: JSON.stringify(isProd ? 'prod' : 'dev'),
    }),
    new MiniCssExtractPlugin({
      filename: '../css/[name].css',
      chunkFilename: '[id].css',
    }),
    new CopyPlugin([
      {
        from: join(SRC_DIR, 'sounds'),
        to: join(DIST_DIR, 'sounds'),
      },
    ]),
    // new BundleAnalyzerPlugin(),
  ],
};
