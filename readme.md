### Login
- jwt **string**

### ServerManifest
- url: **string**
- ip: **string**
- privateRsaKeyPath: **string**
- publicRsaKeyPath: **string**
- token **string** (UUIDv4)
- title: **string**
- description: **string**
- hooks: **Map<string, string>**

### Hooks
- videoComplete
  - video **binary**
  - createdAt **Date**
  - userId **int**
- getUserData -> (email, nick)
  - id **int**
- setUserJwt
  - id **int**
  - jwt **string**
- setServerToken
  - token **string**
