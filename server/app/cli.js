#!/usr/local/bin/node

const db = require('./services/db');
const {config} = require('dotenv');
config();

const logger = require('./services/loger');
logger.init({
  dirname: process.env.LOG_PATH,
});

db.init({
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
});

const mainMenu = require('./cli/menu/main');
const keysMenu = require('./cli/menu/keys');
const clientServersMenu = require('./cli/menu/client-servers');
const roomsMenu = require('./cli/menu/rooms');

const checkKeysAction = require('./cli/actions/keys/check');
const generateAction = require('./cli/actions/keys/generate');
const encodeAction = require('./cli/actions/keys/encode');
const decodeAction = require('./cli/actions/keys/decode');
const addServerAction = require('./cli/actions/client-servers/add');
const destroyExpiredAction = require('./cli/actions/rooms/destroy-expired');
const destroyEmptyChatsAction = require('./cli/actions/rooms/destroy-empty-chats');
const testAction = require('./cli/actions/test/test');

const actions = new Map([
  [mainMenu.key, mainMenu],
  [keysMenu.key, keysMenu],
  [clientServersMenu.key, clientServersMenu],
  [roomsMenu.key, roomsMenu],
  [checkKeysAction.key, checkKeysAction],
  [generateAction.key, generateAction],
  [addServerAction.key, addServerAction],
  [encodeAction.key, encodeAction],
  [decodeAction.key, decodeAction],
  [destroyExpiredAction.key, destroyExpiredAction],
  [testAction.key, testAction],
  [destroyEmptyChatsAction.key, destroyEmptyChatsAction],
]);

const processForceAction = async (key) => {
  const forceAction = actions.get(key);
  if (!forceAction) {
    console.error('No actions with key ', key);

    return;
  }

  await forceAction.handler();
};

(async () => {
  try {
    const forceIndex = process.argv.indexOf('--force');
    if (forceIndex > -1) {
      const forceActionKey = process.argv[forceIndex + 1];
      await processForceAction(forceActionKey);

      return;
    }

    let action = actions.get(mainMenu.key);

    while (true) {
      action = actions.get(await action.handler());
      if (!action) {
        return;
      }
    }
  } catch (e) {
    logger.error(`CLI error`, e);
  }
})();
