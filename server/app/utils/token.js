const jwt = require('jsonwebtoken');
const userRepo = require('../repositories/user-repository');
const {loadFile} = require('./fs');
const {
  AUTH_PUBLIC_KEY_PATH,
  AUTH_PRIVATE_KEY_PATH,
  AUTH_PASSPHRASE,
} = process.env;

let publicKey = null;
let privateKey = null;

const getPublicKey = async () => {
  if (!publicKey) {
    publicKey = await loadFile(AUTH_PUBLIC_KEY_PATH);
  }

  return publicKey;
};

const getPrivateKey = async () => {
  if (!privateKey) {
    privateKey = await loadFile(AUTH_PRIVATE_KEY_PATH);
  }

  return privateKey;
};

module.exports = {
  async createToken(data) {
    const token = jwt.sign(
        data,
        {
          key: await getPrivateKey(),
          passphrase: AUTH_PASSPHRASE,
        },
        {
          algorithm: 'RS256',
          expiresIn: '24h',
        },
    );

    return token;
  },
  auth(token, server) {
    if (!server || !(token && token.length > 100)) {
      return Promise.resolve(null);
    }

    return new Promise(async (resolve) => {
      jwt.verify(
          token,
          await getPublicKey(),
          {
            algorithms: ['RS256'],
          },
          (err, {id} = {}) => {
            if (err) {
              return resolve(null);
            }

            resolve(userRepo.findOrFail({
              clientId: id,
              ServerId: server.id,
            }));
          });
    });
  },
};
