const {readFile, mkdir, appendFile, access, readdir} = require('fs');
const {resolve, join} = require('path');
const readChunk = require('read-chunk');
const fileType = require('file-type');
const logger = require('../services/loger');

const getBlobMimeData = (blob) => {
  return fileType(blob);
};

const getFileType = (filePath) => {
  const buffer = readChunk.sync(filePath, 0, fileType.minimumBytes);

  return fileType(buffer);
};

const loadFile = (path) => {
  return new Promise((resolve, reject) => {
    readFile(path, null, (err, data) => {
      if (err) {
        logger.error(`Load file: ${path}`, err);
        reject(err);
      } else {
        logger.info(`Load file: ${path}`);
        resolve(data);
      }
    });
  });
};

const loadAllFiles = async (files) => {
  return await Promise.all(files.map(loadFile));
};

const makeDir = (path) => {
  return new Promise((resolve, reject) => {
    mkdir(path, {
      recursive: true,
    }, (err) => {
      if (err) {
        logger.error(`Make dir: ${path}`, err);
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
};

const makeDateDir = async (startPath, dirname = null, customDate = null) => {
  const date = customDate || new Date();
  const datePath = join(
      `${date.getFullYear()}`,
      `${date.getMonth() + 1}`,
      `${date.getDate()}`,
  );
  const destPath = resolve(
    dirname
      ? join(startPath, datePath, dirname)
      : join(startPath, datePath)
  );
  await makeDir(destPath);

  return destPath;
};

const writeToFile = (path, data) => {
  return new Promise((resolve, reject) => {
    appendFile(path, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
};

const checkAccess = (path, constant) => {
  return new Promise((resolve, reject) => {
    access(path, constant, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
};

const glob = (path, regex) => {
  return new Promise((resolve, reject) => {
    readdir(path, (err, files) => {
      if (err) {
        reject(err);

        return;
      }

      resolve(files.filter((file) => {
        return regex.test(file);
      }));
    });
  });
};

module.exports = {
  loadFile,
  loadAllFiles,
  makeDir,
  makeDateDir,
  writeToFile,
  checkAccess,
  glob,
  getFileType,
  getBlobMimeData,
};
