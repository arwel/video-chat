const {Input, Select} = require('enquirer');

module.exports = {
  ask(question, options, type = Input) {
    const prompt = new type({
      name: 'pass',
      message: question,

      ...options
    });
    prompt.clear();

    return prompt.run();
  },
  select(message, choices) {
    const prompt = new Select({
      name: 'dataSource',
      message,
      choices,
    });

    return prompt.run();
  }
};
