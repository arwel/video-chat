const {sha256} = require('./etc');
const uuidV4 = require('uuid/v4');
const {createCipheriv, createDecipheriv} = require('crypto');

const ENCRYPTION_METHOD = 'AES-256-CBC';
const INPUT_ENCODING = 'utf8';
const OUTPUT_ENCODING = 'base64';

module.exports = {
  generateKeys() {
    return {
      secret: `${uuidV4()}`.substring(0, 32),
      iv: sha256(uuidV4()).substring(0, 16),
    };
  },
  encrypt(text, secret, iv) {
    const encryptor = createCipheriv(ENCRYPTION_METHOD, secret, iv);

    return encryptor.update(text, INPUT_ENCODING, OUTPUT_ENCODING) +
      encryptor.final(OUTPUT_ENCODING);
  },
  decrypt(crypted, secret, iv) {
    const decryptor = createDecipheriv(ENCRYPTION_METHOD, secret, iv);

    return decryptor.update(crypted, OUTPUT_ENCODING, INPUT_ENCODING) +
      decryptor.final(INPUT_ENCODING);
  },
};
