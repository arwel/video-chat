const {createHash} = require('crypto');
const logger = require('../services/loger');

module.exports = {
  sha256(text) {
    if (!text || text.length < 1) {
      logger.warn('Error sha256');
      return '';
    }

    const hash = createHash('sha256');
    hash.update(text);

    return hash.digest('hex');
  },
  toMap(obj, key = 'key', value = 'value') {
    const map = new Map();

    if (obj instanceof Array) {
      for (const item of obj) {
        map.set(item[key], item[value]);
      }
    } else {
      for (const objKey in obj) {
        obj.hasOwnProperty(objKey) && map.set(objKey, obj[objKey]);
      }
    }

    return map;
  },
  toObject(obj, key = 'key', value = 'value') {
    let newObj = {};

    if (obj instanceof Array) {
      for (const item of obj) {
        newObj[item[key]] = item[value];
      }
    } else {
      newObj = {... obj};
    }

    return newObj;
  },
};
