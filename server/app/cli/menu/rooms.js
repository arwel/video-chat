const {Select} = require('enquirer');
const destroyExpiredAction = require('../actions/rooms/destroy-expired');

module.exports = {
  title: 'Rooms',
  key: 'rooms-menu',
  async handler() {
    const prompt = new Select({
      name: 'rooms',
      message: 'Rooms',
      choices: [
        {name: destroyExpiredAction.key, message: destroyExpiredAction.title},

        {name: 'main-menu', message: 'Main menu'},
        {name: null, message: 'Exit'},
      ],
    });

    const answer = await prompt.run();
    prompt.clear();

    return answer;
  },
};
