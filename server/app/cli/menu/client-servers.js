const {Select} = require('enquirer');
const addAction = require('../actions/client-servers/add');

module.exports = {
  title: 'Client servers',
  key: 'client-servers',
  async handler() {
    const prompt = new Select({
      name: 'client-servers',
      message: 'Client servers',
      choices: [
        {name: addAction.key, message: addAction.title},

        {name: 'main-menu', message: 'Main menu'},
        {name: null, message: 'Exit'},
      ],
    });

    const answer = await prompt.run();
    prompt.clear();

    return answer;
  }
};
