const {Select} = require('enquirer');
const checkKeysAction = require('../actions/keys/check');
const encodeAction = require('../actions/keys/encode');
const generateAction = require('../actions/keys/generate');
const decodeAction = require('../actions/keys/decode');

module.exports = {
  title: 'Crypto keys',
  key: 'crypto-keys',
  async handler() {
    const prompt = new Select({
      name: 'next-action',
      message: 'Main menu',
      choices: [
        {name: generateAction.key, message: generateAction.title},
        {name: checkKeysAction.key, message: checkKeysAction.title},
        {name: encodeAction.key, message: encodeAction.title},
        {name: decodeAction.key, message: decodeAction.title},

        {name: 'main-menu', message: 'Main menu'},
        {name: null, message: 'Exit'},
      ],
    });

    const answer = await prompt.run();
    prompt.clear();

    return answer;
  },
};
