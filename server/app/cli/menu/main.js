const {Select} = require('enquirer');

const keysAction = require('./keys');
const clientServersAction = require('./client-servers');
const roomsMenu = require('./rooms');

module.exports = {
  title: 'Main menu',
  key: 'main-menu',
  async handler() {
    const prompt = new Select({
      name: 'next-action',
      message: 'Main menu',
      choices: [
        {name: keysAction.key, message: keysAction.title},
        {name: clientServersAction.key, message: clientServersAction.title},
        {name: roomsMenu.key, message: roomsMenu.title},

        {name: null, message: 'Exit'},
      ],
    });

    const answer = await prompt.run();
    prompt.clear();

    return answer;
  },
};
