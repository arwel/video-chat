const {encrypt} = require('../../../utils/crypto');
const {ask} = require('../../../utils/input');

module.exports = {
  title: 'Encode',
  key: 'encode',
  async handler() {
    const text = await ask('Payload');
    const secret = await ask('Secret');
    const iv = await ask('IV');

    console.log(encrypt(text, secret, iv));
  },
};
