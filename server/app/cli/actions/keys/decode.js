const {decrypt} = require('../../../utils/crypto');
const {ask} = require('../../../utils/input');

module.exports = {
  title: 'Decode',
  key: 'keys-decode',
  async handler() {
    const encrypted = await ask('Encrypted text');
    const secret = await ask('Secret');
    const iv = await ask('IV');
    const decrypted = decrypt(encrypted, secret, iv);

    console.log(decrypted);
  },
};
