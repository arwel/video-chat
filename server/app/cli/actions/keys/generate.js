const {generateKeys} = require('../../../utils/crypto');

module.exports = {
  title: 'Generate',
  key: 'keys-generate',
  async handler() {
    const {secret, iv} = generateKeys();

    console.log('secret', secret);
    console.log('iv', iv);
  },
};
