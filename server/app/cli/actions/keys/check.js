const {encrypt, decrypt} = require('../../../utils/crypto');
const {ask} = require('../../../utils/input');

module.exports = {
  title: 'Check',
  key: 'keys-check ',
  async handler() {
    const text = await ask('Test phrase');
    const secret = await ask('Secret');
    const iv = await ask('IV');

    const encrypted = encrypt(text, secret, iv);
    const decrypted = decrypt(encrypted, secret, iv);

    console.log(decrypted === text ? 'Success' : 'Fail');
  },
};
