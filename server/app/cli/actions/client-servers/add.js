const {Server, WebHook} = require('../../../entity/index');
const serverRepo = require('../../../repositories/server-repository');
const {post} = require('../../../utils/request');
const {toMap} = require('../../../utils/etc');
const {loadFile} = require('../../../utils/fs');
const {ask, select} = require('../../../utils/input');
const {generateKeys} = require('../../../utils/crypto');
const {Snippet} = require('enquirer');
const logger = require('../../../services/loger');
const fetch = require('node-fetch');

const getDataFromSource = async () => {
  const FROM_FILE = 'FROM_FILE';
  const FROM_URL = 'FROM_URL';
  const FROM_INPUT = 'FROM_INPUT';

  const sourceType = await select('Select data source', [
    {
      name: FROM_FILE,
      message: 'Load form file',
    },
    {
      name: FROM_URL,
      message: 'Load form URL',
    },
    {
      name: FROM_INPUT,
      message: 'Load form input',
    },
  ]);

  switch (sourceType) {
    case FROM_FILE:
      const path = await ask('Path to json file');

      return JSON.parse(await loadFile(path));
    case FROM_URL:
      const url = await ask('URL for load json data');
      const response = await fetch(url, {
        credentials: 'include',
      });

      return await response.json();
    default:
      return {};
  }
};

const manualCheckLoadedData = async ({
  url,
  ip,
  title,
  description,
  locale,
  hooks,
}) => {
  const prompt = new Snippet({
    name: 'username',
    message: 'Check or fill server data',
    required: true,
    fields: [
      {
        name: 'title',
        message: 'Title',
        initial: title,
      },
      {
        name: 'desc',
        message: 'Description',
        initial: description,
      },
      {
        name: 'url',
        message: 'Url',
        initial: url,
      },
      {
        name: 'ip',
        message: 'IP (v4)',
        initial: ip,

      },
      {
        name: 'locale',
        message: 'Locale',
        initial: locale,

      },

      {
        name: `${Server.VIDEO_COMPLETE}`,
        message: 'Web hook on video completed',
        initial: hooks && hooks[Server.VIDEO_COMPLETE]
          ? hooks[Server.VIDEO_COMPLETE]
          : null,
      },
      {
        name: `${Server.GET_USER_DATA}`,
        message: 'Web hook for get user data',
        initial: hooks && hooks[Server.GET_USER_DATA]
          ? hooks[Server.GET_USER_DATA]
          : null,
      },
      {
        name: `${Server.SET_SERVER_TOKEN}`,
        message: 'Web hook for set server token',
        initial: hooks && hooks[Server.SET_SERVER_TOKEN]
          ? hooks[Server.SET_SERVER_TOKEN]
          : null,
      },
      {
        name: `${Server.SET_USER_JWT}`,
        message: 'Web hook for set user token',
        initial: hooks && hooks[Server.SET_USER_JWT]
          ? hooks[Server.SET_USER_JWT]
          : null,
      },

    ],
    template: `{
  "title": "\${title}",
  "url": "\${url}",
  "locale": "\${locale}",
  "ip": "\${ip}",
  "description": "\${desc}",
  "webHooks": [
  {
    "key": "${Server.VIDEO_COMPLETE}",
    "url": "\${${Server.VIDEO_COMPLETE}}"
  },
  {
    "key": "${Server.GET_USER_DATA}",
    "url": "\${${Server.GET_USER_DATA}}"
  },
  {
    "key": "${Server.SET_SERVER_TOKEN}",
    "url": "\${${Server.SET_SERVER_TOKEN}}"
  },
  {
    "key": "${Server.SET_USER_JWT}",
    "url": "\${${Server.SET_USER_JWT}}"
  }
  ]
}
`,
  });

  prompt.clear();

  return await prompt.run();
};

module.exports = {
  title: 'Add server',
  key: 'client-servers-add',
  async handler() {
    try {
      const defaults = await getDataFromSource();
      const data = await manualCheckLoadedData(defaults);
      const serverDataJson = JSON.parse(data.result);
      const {secret, iv} = generateKeys();
      const {webHooks} = Server;
      delete Server.webHooks;

      const [server] = await serverRepo.findOrCreate({
        where: {
          url: serverDataJson.url,
          ip: serverDataJson.ip,
        },
        defaults: {
          ...serverDataJson,
          iv,
          token: secret,
        },
      });

      serverDataJson.webHooks = serverDataJson.webHooks.map((hook) => {
        return {
          ...hook,
          ServerId: server.id,
        };
      });

      console.log('server id ', server.id);
      console.log('server token ', server.token);
      console.log('server iv ', server.iv);

      await WebHook.bulkCreate(serverDataJson.webHooks);

      const hooks = toMap(serverDataJson.webHooks, 'key', 'url');
      const response = await post(hooks.get(Server.SET_SERVER_TOKEN), {
        token: server.token,
        iv: server.iv,
      });

      console.log(response.status);
    } catch (e) {
      console.log(e);
      logger.error('Add server error', e);
    }
  },
};
