const {post} = require('../../../utils/request');
const {Room, Server} = require('../../../entity');
const {Op: {lt, not}} = require('sequelize');
const {glob} = require('../../../utils/fs');
const {encrypt} = require('../../../utils/crypto');

module.exports = {
  title: 'Destroy expired rooms',
  key: 'destroy-expired-rooms',
  async handler() {
    const roomsIds = [];
    const serversIds = [];
    const servers = {};

    const expiredRooms = await Room.findAll({
      where: {
        expiredAt: {
          [lt]: new Date(),
        },
        videoRecordSupport: true,
        recordsPath: {
          [not]: null,
        },
      },
    });

    for (const room of expiredRooms) {
      roomsIds.push(room.id);
      serversIds.push(room.ServerId);
    }

    let serversArray = await Server.findAll({where: {id: serversIds}});
    for (const {token, iv, id} of serversArray) {
      servers[id] = {
        token,
        iv,
      };
    }
    serversArray = null;

    for (const room of expiredRooms) {
      const recordsPath = `${room.recordsPath}`.trim();
      if (!room.videoRecordSupport || recordsPath.length < 1) {
        continue;
      }

      const videosNames = await glob(recordsPath, /\.webm$/i);
      if (videosNames.length < 1) {
        continue;
      }

      const {token, iv} = servers[room.ServerId];

      const response = await post(room.videoReturnUrl, {
        video: encrypt(
            JSON.stringify(videosNames.map((name) => {
              return name.trim().replace(/\.webm$/i, '');
            })),
            token,
            iv,
        ),
      });

      console.log('response code', response.status);
    }

    await Room.update({
      videoRecordSupport: false,
    }, {
      where: {
        id: roomsIds,
      },
    });
  },
};
