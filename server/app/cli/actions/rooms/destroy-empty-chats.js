const {
  STATUS_DELETED,
} = require('../../../../../shared/const/status');
const {Room, Message} = require('../../../entity');

module.exports = {
  title: 'Destroy empty chats',
  key: 'destroy-empty-rooms',
  async handler() {
    const dialogs = await Room.findAll({
      where: {
        status: STATUS_DELETED,
      },
    });

    if (dialogs.length < 1) {
      return;
    }

    const dialogsIds = dialogs.map((dialog) => {
      return dialog.id;
    });

    await Message.destroy({
      where: {
        RoomId: dialogsIds,
      },
    });

    await Room.destroy({
      where: {
        id: dialogsIds,
      },
    });
  },
};
