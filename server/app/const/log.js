module.exports = {
  LOG_DEBUG: 'debug',
  LOG_INFO: 'info',
  LOG_WARN: 'warn',
  LOG_ERROR: 'error',
};
