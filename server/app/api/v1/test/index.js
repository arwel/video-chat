const {Router} = require('express');
const router = Router();

router.get('/ping', (req, res) => {
  res.status(200).json({
    ping: 'pong',
  });
});

router.post('/', (req, res) => {
  res.json({
    request: req.body,
    test: 'OK',
  });
});

module.exports = router;
