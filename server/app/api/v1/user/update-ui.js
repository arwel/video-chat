const socketPull = require('../../../services/socket-pull');
const userRepo = require('../../../repositories/user-repository');
const {NOT_FOUND} = require('http-status-codes');

module.exports = async (req, res) => {
  const {clientId, events} = req.body;
  let user = null;

  try {
    user = await userRepo.findOrFail({
      clientId,
      ServerId: req.server.id,
    });
  } catch (e) {
    return res.status(NOT_FOUND).json({
      errors: [`${e}`],
    });
  }

  for (const {type, payload} of events) {
    if (!type || !payload) {
      continue;
    }

    socketPull.emit(user.id, 'updateUi', {
      type,
      payload,
    });
  }

  res.json({
    result: 'OK',
  });
};
