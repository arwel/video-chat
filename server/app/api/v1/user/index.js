const updateData = require('./update-data');
const bulkUpdate = require('./bulk-update-data');
const register = require('./register');
const updateUi = require('./update-ui');
const userAuth = require('../../middleware/user-auth');
const {Router} = require('express');
const router = Router();

router.post('/register', register);
router.post('/update-ui', updateUi);
router.post('/bulk-update-data', bulkUpdate);
router.post('/update-data', userAuth, updateData);

module.exports = router;
