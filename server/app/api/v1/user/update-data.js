const {BAD_REQUEST, getStatusText} = require('http-status-codes');
const logger = require('../../../services/loger');

module.exports = async (req, res) => {
  try {
    const {email, nick, locale, avatar, position} = req.body;
    const user = req.user;

    user.nick = nick;
    user.email = email;
    user.avatar = avatar;
    user.position = position;
    user.locale = locale;

    await user.save();

    res.json({
      user,
    });
  } catch (e) {
    logger.error('User update data error', e);

    res.json({
      errors: [
        getStatusText(BAD_REQUEST),
      ],
    }, BAD_REQUEST);
  }
};
