const userRepo = require('../../../repositories/user-repository');
const {BAD_REQUEST, getStatusText} = require('http-status-codes');
const logger = require('../../../services/loger');

module.exports = async (req, res) => {
  try {
    const {users} = req.body;

    for (const {email, nick, locale, avatar, position, clientId} of users) {
      const user = await userRepo.findOneBy({
        ServerId: req.server.id,
        clientId,
      });

      if (!user) {
        continue;
      }

      user.nick = nick;
      user.email = email;
      user.avatar = avatar;
      user.position = position;
      user.locale = locale;

      await user.save();
    }

    res.json({
      users,
    });
  } catch (e) {
    logger.error('User update data error', e);

    res.json({
      errors: [
        getStatusText(BAD_REQUEST),
      ],
    }, BAD_REQUEST);
  }
};
