const {BAD_REQUEST, getStatusText} = require('http-status-codes');
const {createToken} = require('../../../utils/token');
const {Server} = require('../../../entity');
const userRepo = require('../../../repositories/user-repository');
const logger = require('../../../services/loger');

module.exports = async (req, res) => {
  try {
    const serverToken = `${req.header(Server.AUTH_HEADER)}`;
    const {email, nick, locale, avatar, position, clientId} = req.body;
    const userData = {
      clientId,
      ServerId: req.server.id,
    };

    const [user] = await userRepo.findOrCreateUnsafe({
      where: userData,
      defaults: {
        ...userData,
        nick,
        email,
        avatar,
        position,
        locale,
      },
    });

    const token = await createToken({
      serverToken,
      email,
      id: user.clientId,
    });

    res.json({
      token,
      user,
    });
  } catch (e) {
    logger.error('User register error', e);

    res.json({
      errors: [
        getStatusText(BAD_REQUEST),
      ],
    }, BAD_REQUEST);
  }
};
