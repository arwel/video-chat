const userRoomRepo = require('../../../repositories/user-room-repository');
const messageRepo = require('../../../repositories/message-repository');
const {NOT_FOUND} = require('http-status-codes');

module.exports = async (req, res) => {
  const {roomId, message} = req.body;

  try {
    await userRoomRepo.findOrFail({
      UserId: req.user.id,
      RoomId: roomId,
    });
  } catch (e) {
    return res.status(NOT_FOUND).json({
      errors: [`${e}`],
    });
  }

  await messageRepo.createWithHistory({
    content: message,
    UserId: req.user.id,
    RoomId: roomId,
    ReplyId: null,
  });

  res.json({
    result: true,
  });
};
