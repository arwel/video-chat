const send = require('./send');
const userAuth = require('../../middleware/user-auth');
const {Router} = require('express');
const router = Router();

router.use(userAuth);

router.post('/send', send);

module.exports = router;
