const serverAuth = require('../middleware/server-auth');
const aesDecodeRequest = require('../middleware/aes-decode-request');
const {Router} = require('express');
const router = Router();
const roomRoutes = require('./room');
const testRoutes = require('./test');
const userRoutes = require('./user');
const messageRoutes = require('./message');

router.use('/test', testRoutes);

router.use(serverAuth);
router.use(aesDecodeRequest);
router.use('/room', roomRoutes);
router.use('/user', userRoutes);
router.use('/message', messageRoutes);

module.exports = router;
