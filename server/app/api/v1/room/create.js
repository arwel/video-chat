const {TYPE_CALL} = require('../../../../../shared/const/room-types');
const roomRepo = require('../../../repositories/room-repository');
const {
  STATUS_OFFLINE,
} = require('../../../../../shared/const/status');

module.exports = async (req, res) => {
  const {
    title,
    slug,
    type = TYPE_CALL,
    expireAt,
    videoRecordSupport = true,
    videoReturnUrl,
  } = req.body;

  const roomData = {
    title,
    slug,
    ServerId: req.server.id,
  };

  const [room] = await roomRepo.findOrCreate({
    where: roomData,
    defaults: {
      ...roomData,
      type,
      UserId: req.user.id,
      expiredAt: new Date(expireAt),
      videoRecordSupport,
      videoReturnUrl,
    },
  });

  if (room.status !== STATUS_OFFLINE) {
    room.status = STATUS_OFFLINE;
    await room.save();
  }

  res.json({
    room,
  });
};
