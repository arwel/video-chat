const socketPull = require('../../../services/socket-pull');
const userRepo = require('../../../repositories/user-repository');
const roomRepo = require('../../../repositories/room-repository');
const messageRepo = require('../../../repositories/message-repository');
const userRoomRepo = require('../../../repositories/user-room-repository');
const {
  STATUS_OFFLINE,
} = require('../../../../../shared/const/status');

const getUserByClientId = (clientId, serverId) => {
  return userRepo.findOneBy({
    ServerId: serverId,
    clientId,
  });
};

module.exports = async (req, res) => {
  const responseData = [];

  for (const room of req.body) {
    if (!room.members || room.members.length < 1) {
      continue;
    }

    const roomData = {
      slug: room.slug,
      ServerId: req.server.id,
    };

    const [roomEntity] = await roomRepo.findOrCreate({
      where: roomData,
      defaults: {
        ...roomData,
        title: room.title,
        type: room.type,
        UserId: req.user.id,
        expiredAt: new Date(room.expireAt),
        videoRecordSupport: room.videoRecordSupport,
        videoReturnUrl: room.videoReturnUrl,
      },
    });

    if (roomEntity.status !== STATUS_OFFLINE) {
      roomEntity.status = STATUS_OFFLINE;
      await roomEntity.save();
    }

    const users = await Promise.all(room.members.map((memberId) => {
      return getUserByClientId(memberId, req.server.id);
    }));

    await Promise.all(users.map((user) => {
      const data = {
        UserId: user.id,
        RoomId: roomEntity.id,
      };

      return userRoomRepo.findOrCreate({
        where: data,
        defaults: data,
      });
    }));

    const roomPublicData = {
      ...(roomEntity.getPublicData()),
      ...(await messageRepo.getDialogStats(req.user.id, roomEntity.id)),
    };
    responseData.push(roomPublicData);
    for (const member of users) {
      socketPull.emit(member.id, 'dialogsAddNew', {
        chat: roomPublicData,
      });
    }
  }

  res.json({
    result: 'OK',
    rooms: responseData,
  });
};
