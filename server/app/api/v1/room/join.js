const {UserRoom, Room} = require('../../../entity');
const {NOT_FOUND} = require('http-status-codes');

module.exports = async (req, res) => {
  let room = null;

  try {
    room = await Room.findOrFail({
      slug: req.body.slug,
      ServerId: req.server.id,
    });
  } catch (e) {
    return res.status(NOT_FOUND).json({
      errors: [`${e}`],
    });
  }

  await UserRoom.create({
    RoomId: room.id,
    UserId: req.user.id,
  });

  res.json({
    result: true,
  });
};
