const roomRepo = require('../../../repositories/room-repository');
const {glob} = require('../../../utils/fs');
const {NOT_FOUND} = require('http-status-codes');

module.exports = async (req, res) => {
  const {slug} = req.body;
  let room = null;

  try {
    room = await roomRepo.findOrFail({slug});
  } catch (e) {
    return res.status(NOT_FOUND).json({
      errors: [`${e}`],
    });
  }

  const recordsPath = `${room.recordsPath}`.trim();
  if (!room.videoRecordSupport || recordsPath.length < 1) {
    return res.json({
      records: [],
    });
  }

  const videosNames = await glob(recordsPath, /\.webm$/i);
  if (videosNames.length < 1) {
    return res.json({
      records: [],
    });
  }

  return res.json({
    records: videosNames.map((name) => {
      return name.trim().replace(/\.webm$/i, '');
    }),
  });
};
