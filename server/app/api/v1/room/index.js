const create = require('./create');
const getRoomRecords = require('./get-room-records');
const createWithUsers = require('./create-with-users');
const join = require('./join');
const userAuth = require('../../middleware/user-auth');
const {Router} = require('express');
const router = Router();

// public
router.post('/get-room-records', getRoomRecords);

// private
router.post('/create', userAuth, create);
router.post('/join', userAuth, join);
router.post('/create-with-users', userAuth, createWithUsers);

module.exports = router;
