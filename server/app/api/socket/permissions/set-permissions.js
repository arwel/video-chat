const {
  DEVICE_VIDEO_INPUT,
  DEVICE_AUDIO_INPUT,
} = require('../../../../../shared/const/device-kinds');

module.exports = async (socket, user, server, data) => {
  user.hasMicrophone = !!data[DEVICE_AUDIO_INPUT];
  user.hasCamera = !!data[DEVICE_VIDEO_INPUT];

  await user.save();
};
