const userRoomRepo = require('../../../repositories/user-room-repository');
const messageRepo = require('../../../repositories/message-repository');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {ids, roomId}) => {
  const membersIds = await userRoomRepo.getMemberIds(roomId);
  const memberIndex = membersIds.indexOf(user.id);
  if (memberIndex < 0) {
    return;
  }

  await messageRepo.setSeen(ids);

  for (const memberId of membersIds) {
    socketPull.emit(memberId, 'chatsSetSeenMessages', {
      ids,
    });
  }

  const {count, messages} = await messageRepo.getUserNewMessages(user.id);

  socket.emit('newMessagesSetData', {
    count,
    messages,
  });
};
