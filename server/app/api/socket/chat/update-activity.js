const userRoomRepo = require('../../../repositories/user-room-repository');
const socketPull = require('../../../services/socket-pull');
const activityTypes = Object.values(
    require('../../../../../shared/const/activities-types')
);

module.exports = async (socket, user, server, {roomId, type, state}) => {
  if (activityTypes.indexOf(type) < 0) {
    return;
  }

  const membersIds = await userRoomRepo.getMemberIds(roomId);
  if (membersIds.indexOf(user.id) < 0) {
    return;
  }

  const activityData = {
    type,
    state,
    name: user.nick,
  };
  for (const memberId of membersIds) {
    if (memberId === user.id) {
      continue;
    }

    socketPull.emit(memberId, 'chatsUpdateActivity', activityData);
  }
};
