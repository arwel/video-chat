module.exports = {
  sendMessage: require('./send-message'),
  sendEditMessage: require('./send-edit-message'),
  deleteMessage: require('./delete-message'),
  getMessages: require('./get-messages'),
  updateActivity: require('./update-activity'),
  setSeen: require('./set-seen'),
  getNewMessages: require('./get-new-messages'),
  translateMessages: require('./translate-messages'),
};
