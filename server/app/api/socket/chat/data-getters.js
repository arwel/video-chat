const {Op:{ne}} = require('sequelize');
const {Message, UserRoom} = require('../../../entity');

const getNewMessagesCount = (user, roomsIds) => Message.count({
  where: {
    RoomId: roomsIds,
    seen: false,
    UserId: {
      [ne]: user.id,
    },
  },
});

const getLastNewMessages = async (user, roomsIds) => {
  const messages = await Message.findAll({
    where: {
      RoomId: roomsIds,
      seen: false,
      UserId: {
        [ne]: user.id,
      },
    },
    limit: 3,
    order: [
      ['id', 'DESC'],
    ],
  });

  return Promise.all(messages.map((message) => message.getPublicData(false)));
};

const getUserNewMessages = async (user) => {
  const roomsIds = await UserRoom.findAll({
    where: {
      UserId: user.id,
    },
  }).map((userRoom) => userRoom.RoomId);

  const [count, messages] = await Promise.all([
    getNewMessagesCount(user, roomsIds),
    getLastNewMessages(user, roomsIds),
  ]);

  return {
    count,
    messages,
  };
};

module.exports = {
  getNewMessagesCount,
  getLastNewMessages,
  getUserNewMessages,
};
