const messagesRepo = require('../../../repositories/message-repository');

module.exports = async (socket, user) => {
  const {count, messages} = await messagesRepo.getUserNewMessages(user.id);

  socket.emit('newMessagesSetData', {
    count,
    messages,
  });
};
