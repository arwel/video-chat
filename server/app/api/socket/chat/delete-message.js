const userRoomRepo = require('../../../repositories/user-room-repository');
const messageRepo = require('../../../repositories/message-repository');
const socketPull = require('../../../services/socket-pull');


module.exports = async (socket, user, server, {id}) => {
  const message = await messageRepo.findOrFail({
    id,
    UserId: user.id,
  });

  await messageRepo.softDeleteWithHistory(message);

  const membersIds = await userRoomRepo.getMemberIds(message.RoomId);
  for (const memberId of membersIds) {
    socketPull.emit(memberId, 'chatsDeleteMessage', {
      id,
    });
  }
};
