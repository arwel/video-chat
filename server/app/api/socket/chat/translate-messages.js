const messageTranslationRepo = require('../../../repositories/message-translation-repository');
const messageRepo = require('../../../repositories/message-repository');
const translate = require('@vitalets/google-translate-api');

const getTranslation = async (id, locale) => {
  try {
    let translation = await messageTranslationRepo.findOneBy({
      MessageId: id,
      locale,
    });

    if (!translation) {
      const msg = await messageRepo.findOrFail({id});
      const {text} = await translate(msg.content, {to: locale});
      translation = await messageTranslationRepo.create({
        locale,
        content: text,
        MessageId: msg.id,
      });
    }

    return translation.getPublicData();
  } catch (e) {
    return null;
  }
};

module.exports = async (socket, user, server, {ids, locale}) => {
  const translations = await Promise.all(
      ids.map((id) => getTranslation(id, locale))
  );

  socket.emit('chatsSetTranslations', {
    translations: translations.filter((translation) => !!translation),
  });
};
