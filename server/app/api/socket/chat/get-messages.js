const userRoomRepo = require('../../../repositories/user-room-repository');
const roomRepo = require('../../../repositories/room-repository');
const messageRepo = require('../../../repositories/message-repository');
const {TYPE_ANONYMOUS_CHAT} = require('../../../../../shared/const/room-types');
const {ITEMS_PER_PAGE} = require('../../../../../shared/const/threasholds');

module.exports = async (socket, user, server, {chatId, offset}) => {
  await userRoomRepo.findOrFail({
    RoomId: chatId,
    UserId: user.id,
  });

  const room = await roomRepo.find(chatId);
  const messagesData = await messageRepo.getMessagesData(
      chatId,
      +offset,
      ITEMS_PER_PAGE,
      room.type === TYPE_ANONYMOUS_CHAT
  );

  socket.emit('chatsAddMessages', {
    messages: messagesData.reverse(),
  });
};
