const messageTranslationRepo = require('../../../repositories/message-translation-repository');
const messageRepo = require('../../../repositories/message-repository');
const userRoomRepo = require('../../../repositories/user-room-repository');
const {
  UserRoom,
} = require('../../../entity');
const socketPull = require('../../../services/socket-pull');
const {TYPE_ANONYMOUS_CHAT} = require('../../../../../shared/const/room-types');
const translate = require('@vitalets/google-translate-api');

const updateTranslations = async (msgId, newContent) => {
  const translations = await messageTranslationRepo.getMessageTranslations(msgId);

  await Promise.all(translations.map(async (translation) => {
    const {text} = await translate(newContent, {
      to: translation.locale,
    });

    translation.content = text;

    return translation.save();
  }));
};

module.exports = async (socket, user, server, {message, id}) => {
  const msg = await messageRepo.findOrFail({
    id,
    userId: user.id,
  });

  await messageRepo.updateWithHistory(msg, {
    content: message,
  });

  await updateTranslations(msg.id, message);

  const room = await msg.getRoom();
  const messageData = await msg.getPublicData(
      true,
      room.type === TYPE_ANONYMOUS_CHAT
  );
  const membersIds = await userRoomRepo.getMemberIds(msg.RoomId);
  for (const memberId of membersIds) {
    socketPull.emit(memberId, 'chatsUpdateMessage', {
      message: messageData,
    });
  }
};
