const {join} = require('path');
const {statSync} = require('fs');
const userRoomRepo = require('../../../repositories/user-room-repository');
const messageRepo = require('../../../repositories/message-repository');
const fileRepo = require('../../../repositories/file-repository');
const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');
const {TYPE_ANONYMOUS_CHAT} = require('../../../../../shared/const/room-types');
const {filterToPlainText} = require('../../../../../shared/utils/filters/text');
const {sha256} = require('../../../utils/etc');
const {PATH_TO_SERVER_UPLOADS} = require('../../../../config/path');
const {
  ALLOWED_UPLOAD_FILES_COUNT,
} = require('../../../../../shared/const/threasholds');
const {
  validateFiles,
  size,
  mimeType,
} = require('../../../../../shared/utils/validation/files');
const {
  makeDateDir,
  writeToFile,
  getBlobMimeData,
} = require('../../../utils/fs');

const uploadFile = async ({file, absolutePath, relativePath, message}) => {
  const {mime, ext} = getBlobMimeData(file.content);
  const newFileName = `${sha256(file.name)}.${ext}`;
  const destFilePath = join(absolutePath, newFileName);

  await writeToFile(destFilePath, file.content);

  const {size} = statSync(destFilePath);

  return fileRepo.create({
    name: file.name,
    path: join(relativePath, newFileName),
    mimeType: mime,
    size,
    MessageId: message.id,
  });
};

const processUploadedFiles = async ({files, dir, message}) => {
  if (files.length < 1) {
    return;
  }

  const absolutePath = await makeDateDir(PATH_TO_SERVER_UPLOADS, `${dir}`);
  const relativePath = absolutePath.replace(`${PATH_TO_SERVER_UPLOADS}/`, '');

  const uploadedFiles = await Promise.all(files.map((file) => {
    return uploadFile({
      file,
      absolutePath,
      relativePath,
      message,
    });
  }));

  return uploadedFiles.reduce((uploadedCount, file) => {
    return !!file ? ++uploadedCount : uploadedCount;
  });
};

module.exports = async (
  socket,
  user,
  server,
  {message, roomId, replyId, files = []},
) => {
  files = files.slice(0, ALLOWED_UPLOAD_FILES_COUNT);
  message = filterToPlainText(message);

  const fileValidationErrors = validateFiles(files, [
    size,
    mimeType,
  ]);
  if (fileValidationErrors.length > 0) {
    return;
  }

  const membersIds = await userRoomRepo.getMemberIds(roomId);
  const memberIndex = membersIds.indexOf(user.id);
  if (memberIndex < 0) {
    return;
  }

  let reply = null;
  if (replyId) {
    const replyMsg = await messageRepo.find(replyId);
    reply = replyMsg ? replyMsg.id : null;
  }

  const msg = await messageRepo.createWithHistory({
    content: message,
    UserId: user.id,
    RoomId: roomId,
    ReplyId: reply,
  });

  await processUploadedFiles({
    files,
    dir: roomId,
    message: msg,
  });

  const room = await roomRepo.find(roomId);
  const messageData = await msg.getPublicData(
      true,
      room.type === TYPE_ANONYMOUS_CHAT,
  );
  await roomRepo.updateDate(room);

  const usersNewMessagesData = await Promise.all(
      membersIds.map((memberId) => messageRepo.getUserNewMessages(memberId))
  );

  for (const key of Object.keys(membersIds)) {
    const memberId = membersIds[key];
    const {count, messages} = usersNewMessagesData[key] || {
      count: 0,
      messages: [],
    };

    socketPull.emit(memberId, 'chatsNewMessage', messageData);
    socketPull.emit(memberId, 'newMessagesSetData', {
      count,
      messages,
    });
  }
};
