const fileRepo = require('../../../repositories/file-repository');
const userRoomRepo = require('../../../repositories/user-room-repository');
const {loadFile} = require('../../../utils/fs');
const {PATH_TO_SERVER_UPLOADS} = require('../../../../config/path');
const {join} = require('path');

module.exports = async (socket, user, server, {id}) => {
  const file = await fileRepo.findOrFail({
    id,
  });

  const message = await file.getMessage();
  if (message.UserId !== user.id) {
    await userRoomRepo.findOrFail({
      UserId: user.id,
      RoomId: message.RoomId,
    });
  }

  const path = join(PATH_TO_SERVER_UPLOADS, file.path);
  const buffer = Buffer.from(await loadFile(path));

  socket.emit('filesDownloadContent', {
    id,
    content: Uint8Array.from(buffer).buffer,
    mimeType: file.mimeType,
  });
};
