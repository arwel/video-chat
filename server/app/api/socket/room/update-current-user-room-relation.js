const userRoomRepo = require('../../../repositories/user-room-repository');

module.exports = async (socket, user, server, {roomId, status}) => {
  const relation = await userRoomRepo.findOrFail({
    UserId: user.id,
    RoomId: roomId,
  });

  if (status === relation.status) {
    return;
  }

  relation.status = status;
  await relation.save();

  const relations = await userRoomRepo.findAll({
    where: {
      UserId: user.id,
    },
  });

  socket.emit('userSetRoomRelations', {
    relations,
  });
};
