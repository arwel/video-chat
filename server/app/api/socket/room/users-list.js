const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');
const {
  STATUS_ONLINE,
  STATUS_OFFLINE,
} = require('../../../../../shared/const/status');

module.exports = async (socket, user, server, {slug}) => {
  const room = await roomRepo.findOrFail({
    slug,
    ServerId: server.id,
  });

  const users = await room.getUsers();
  const clients = [];
  for (const currentUser of users) {
    if (currentUser.id === user.id) {
      continue;
    }

    const status = socketPull.has(currentUser.id)
      ? STATUS_ONLINE
      : STATUS_OFFLINE;
    clients.push(currentUser.getPublicData(status));
  }

  socket.emit('roomUsersList', clients);
};
