const {STATUS_OFFLINE} = require('../../../../../shared/const/status');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user) => {
  const rooms = await user.getRooms();

  socketPull.closeAll(user.id);

  for (const room of rooms) {
    const users = await room.getUsers();
    for (const {id} of users) {
      if (id === user.id || !socketPull.has(id)) {
        continue;
      }

      socketPull.emit(
          id,
          'updateRoomUser',
          user.getPublicData(STATUS_OFFLINE)
      );
    }
  }
};
