const {Op: {like, or}} = require('sequelize');
const userRepo = require('../../../repositories/user-repository');

module.exports = async (socket, user, server, {query}) => {
  console.log('query', query);

  const users = await userRepo.findAll({
    where: {
      [or]: [
        {
          nick: {
            [like]: `%${query}%`,
          },
          email: {
            [like]: `%${query}%`,
          },
        },
      ],
    },
    limit: 5,
  });

  const usersData = await Promise.all(users.map((user) => user.getPublicData()));

  socket.emit('dialogsSetUsersToInvite', {
    users: usersData,
  });
};
