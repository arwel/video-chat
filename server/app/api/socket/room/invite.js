const roomRepo = require('../../../repositories/room-repository');
const userRoomRepo = require('../../../repositories/user-room-repository');
const userRepo = require('../../../repositories/user-repository');
const socketPull = require('../../../services/socket-pull');
const {
  TYPE_GROUP_CHAT,
  TYPE_ANONYMOUS_CHAT,
} = require('../../../../../shared/const/room-types');

module.exports = async (socket, user, server, {roomId, id}) => {
  await userRoomRepo.findOrFail( {
    RoomId: roomId,
    UserId: user.id,
  });

  const room = await roomRepo.findOneBy({
    id: roomId,
    type: [TYPE_GROUP_CHAT, TYPE_ANONYMOUS_CHAT],
  });

  if (!room) {
    return;
  }

  const userToInvite = await userRepo.findOrFail({
    ServerId: server.id,
    id,
  });

  const inviteData = {
    RoomId: roomId,
    UserId: userToInvite.id,
  };

  await userRoomRepo.findOrCreate({
    where: inviteData,
    defaults: inviteData,
  });

  socketPull.emit(userToInvite.id, 'dialogsAddNew', {
    chat: room.getPublicData(),
  });

  const usersData = await userRoomRepo.getUsersOfRoom(roomId);
  for (const member of usersData) {
    socketPull.emit(member.id, 'dialogsSetActiveDialogMembers', {
      members: usersData,
    });
  }
};
