module.exports = {
  entry: require('./entry'),
  leave: require('./leave'),
  usersList: require('./users-list'),
  getRooms: require('./get-rooms'),
  removeMe: require('./remove-me'),
  invite: require('./invite'),
  usersToInvite: require('./users-to-invite'),
  setActiveChatBySlug: require('./set-active-chat-by-slug'),
  getCurrentUserRoomRelations: require('./get-current-user-room-relations'),
  updateCurrentUserRoomRelation: require('./update-current-user-room-relation'),
};
