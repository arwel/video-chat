const roomRepo = require('../../../repositories/room-repository');

module.exports = async (socket, user, server, {slug}) => {
  const room = await roomRepo.findOrFail({
    slug,
    ServerId: server.id,
  });

  socket.emit('dialogsRemoteSetActiveChat', {
    dialog: room.getPublicData(),
  });
};
