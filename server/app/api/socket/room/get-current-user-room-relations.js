const userRoomRepo = require('../../../repositories/user-room-repository');

module.exports = async (socket, user) => {
  const relations = await userRoomRepo.findAll({
    where: {
      UserId: user.id,
    },
  });

  socket.emit('userSetRoomRelations', {
    relations,
  });
};
