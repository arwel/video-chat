const roomRepo = require('../../../repositories/room-repository');
const {STATUS_ONLINE} = require('../../../../../shared/const/status');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {slug}) => {
  const room = await roomRepo.findOrFail({
    slug,
    ServerId: server.id,
  });

  const users = await room.getUsers();
  for (const {id} of users) {
    if (id === user.id || !socketPull.has(id)) {
      continue;
    }

    socketPull.emit(id, 'updateRoomUser', user.getPublicData(STATUS_ONLINE));
  }
};
