const userRoomRepo = require('../../../repositories/user-room-repository');
const {ITEMS_PER_PAGE} = require('../../../../../shared/const/threasholds');

module.exports = async (socket, user, _, {type, offset = 0}) => {
  const rooms = await userRoomRepo.getRoomsOfUser(
      user.id,
      type,
      ITEMS_PER_PAGE,
      offset
  );

  socket.emit('roomSetRoomList', {
    rooms: rooms.map((room) => room.getPublicData()),
  });
};
