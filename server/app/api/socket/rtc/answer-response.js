const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {answer, ice, masterId}) => {
  socketPull.emit(masterId, 'rtcAnswerResponse', {
    slaveId: user.id,
    answer,
    ice,
  });
};
