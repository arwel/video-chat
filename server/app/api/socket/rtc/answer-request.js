const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {offer, ice, slaveId, withSlaveMedia = true}) => {
  socketPull.emit(slaveId, 'rtcAnswerRequest', {
    masterId: user.id,
    offer,
    ice,
    slaveId,
    withSlaveMedia,
  });
};
