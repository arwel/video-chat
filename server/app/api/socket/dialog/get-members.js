const userRoomRepo = require('../../../repositories/user-room-repository');

module.exports = async (socket, user, server, {id}) => {
  const usersData = await userRoomRepo.getUsersOfRoom(id, null);

  socket.emit('dialogsSetActiveDialogMembers', {
    members: usersData,
  });
};
