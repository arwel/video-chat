const roomRepo = require('../../../repositories/room-repository');
const userRoomRepo = require('../../../repositories/user-room-repository');
const userRepo = require('../../../repositories/user-repository');
const socketPull = require('../../../services/socket-pull');
const {
  TYPE_GROUP_CHAT,
  TYPE_ANONYMOUS_CHAT,
} = require('../../../../../shared/const/room-types');

module.exports = async (socket, user, server, {roomId, email}) => {
  await userRoomRepo.findOrFail({
    UserId: user.id,
    RoomId: roomId,
  });

  const room = await roomRepo.findOneBy({
    id: roomId,
    type: [TYPE_GROUP_CHAT, TYPE_ANONYMOUS_CHAT],
  });

  if (!room) {
    return;
  }

  const userToInvite = await userRepo.findOrFail({
    ServerId: server.id,
    email,
  });

  const inviteData = {
    RoomId: roomId,
    UserId: userToInvite.id,
  };

  await userRoomRepo.findOrCreate({
    where: inviteData,
    defaults: inviteData,
  });

  socketPull.emit(userToInvite.id, 'dialogsAddNew', {
    chat: room.getPublicData(),
  });
};
