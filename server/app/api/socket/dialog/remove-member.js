const userRoomRepo = require('../../../repositories/user-room-repository');
const roomRepo = require('../../../repositories/room-repository');

module.exports = async (socket, user, server, {id, roomId}) => {
  const isCurrentUser = user.id === id;
  if (!isCurrentUser) {
    await roomRepo.findOrFail({
      id: roomId,
      UserId: user.id,
    });
  }

  await userRoomRepo.destroy({
    where: {
      RoomId: roomId,
      UserId: id,
    },
  });

  if (isCurrentUser) {
    return socket.emit('dialogsRemove', {
      id: roomId,
    });
  }

  const usersData = await userRoomRepo.getUsersOfRoom(roomId);
  socket.emit('dialogsSetActiveDialogMembers', {
    members: usersData,
  });
};
