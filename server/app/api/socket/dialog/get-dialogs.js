const userRoomRepo = require('../../../repositories/user-room-repository');
const messageRepo = require('../../../repositories/message-repository');
const {ITEMS_PER_PAGE} = require('../../../../../shared/const/threasholds');
const {
  TYPE_CHAT,
  TYPE_ANONYMOUS_CHAT,
  TYPE_GROUP_CHAT,
} = require('../../../../../shared/const/room-types');


module.exports = async (socket, user, _, {offset = 0}) => {
  const dialogs = await userRoomRepo.getRoomsOfUser(
      user.id,
      [
        TYPE_CHAT,
        TYPE_ANONYMOUS_CHAT,
        TYPE_GROUP_CHAT,
      ],
      ITEMS_PER_PAGE,
      offset
  );

  const dialogsData = await Promise.all(dialogs.map(async (room) => {
    return {
      ...room.getPublicData(),
      ...(await messageRepo.getDialogStats(user.id, room.id)),
    };
  }));

  socket.emit('dialogsSetDialogList', {
    dialogs: dialogsData,
  });
};
