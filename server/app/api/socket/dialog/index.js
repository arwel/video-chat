module.exports = {
  leave: require('./leave'),
  getDialogs: require('./get-dialogs'),
  removeMe: require('./remove-me'),
  invite: require('./invite'),
  getMembers: require('./get-members'),
  removeMember: require('./remove-member'),
  setActiveChatBySlug: require('./set-active-chat-by-slug'),
};
