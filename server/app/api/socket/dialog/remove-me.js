const userRoomRepo = require('../../../repositories/user-room-repository');
const roomRepo = require('../../../repositories/room-repository');

module.exports = async (socket, user, server, {roomId}) => {
  const removed = await userRoomRepo.destroy({
    where: {
      UserId: user.id,
      RoomId: roomId,
    },
  });

  if (removed < 1) {
    return;
  }

  const usersInRoomCount = await userRoomRepo.count({
    where: {
      RoomId: roomId,
    },
  });

  if (usersInRoomCount < 1) {
    await roomRepo.softDelete(roomId);
  }

  socket.emit('dialogsRemove', {
    id: roomId,
  });
};
