module.exports = async (socket, user) => {
  socket.emit('userUpdateCurrent', await user.getPublicData());
};
