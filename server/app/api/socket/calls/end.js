const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {id}) => {
  socketPull.emit(id, 'callEnd', {
    id: user.id,
  });
};
