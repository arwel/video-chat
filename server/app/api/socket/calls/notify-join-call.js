const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {ids}) => {
  for (const id of ids) {
    socketPull.emit(id, 'onlineUsersForCall', (await user.getPublicData()));
  }
};
