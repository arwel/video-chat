const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {roomSlug}) => {
  const room = await roomRepo.findOrFail({
    slug: roomSlug,
  });

  socketPull.emit(user.id, 'callStatusChanged', {
    status: room.status,
  });
};
