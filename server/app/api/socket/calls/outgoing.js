const userRepo = require('../../../repositories/user-repository');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {receiverId, isForce}) => {
  const receiver = await userRepo.findOrFail({
    id: receiverId,
  });

  if (receiver.hasCamera || receiver.hasMicrophone) {
    socketPull.emit(receiverId, 'incomingCall', {
      id: user.id,
      peer: `${user.id}`,
      isForce,
    });
  }
};
