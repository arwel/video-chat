const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {id}) => {
  socketPull.emit(id, 'accept', {
    id: user.id,
    peer: `${user.id}`,
  });
};
