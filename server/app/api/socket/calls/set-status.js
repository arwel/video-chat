const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');

module.exports = async (socket, user, server, {roomSlug, status}) => {
  const room = await roomRepo.findOrFail({
    slug: roomSlug,
  });

  const users = await room.getUsers();
  const isMember = users.some(({id}) => id === user.id);
  if (!isMember) {
    return;
  }

  room.status = status;
  await room.save();

  for (const {id} of users) {
    socketPull.emit(id, 'callStatusChanged', {
      status: room.status,
    });
  }
};
