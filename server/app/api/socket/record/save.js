const recordManager = require('../../../services/record-manager');

module.exports = async (socket, user, server, {slug, type, data}) => {
  await recordManager.save({
  	fileName: user.clientId,
    slug,
    data,
    type,
  });
};
