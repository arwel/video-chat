const {dirname} = require('path');
const roomRepo = require('../../../repositories/room-repository');
const recordManager = require('../../../services/record-manager');

module.exports = async (socket, user, server, {slug, type}) => {
  const path = recordManager.close({
    slug,
    type,
    fileName: user.clientId,
  });

  if (!path) {
    return;
  }

  await roomRepo.update({
    recordsPath: dirname(path),
  }, {
    where: {
      slug,
    },
  });
};
