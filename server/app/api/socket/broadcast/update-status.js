const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');
const {
  STATUS_OFFLINE,
  STATUS_ONLINE,
  STATUS_PENDING,
} = require('../../../../../shared/const/status');

const properStatuses = [STATUS_ONLINE, STATUS_PENDING, STATUS_OFFLINE];

module.exports = async (socket, {id}, server, {roomSlug, status}) => {
  if (properStatuses.indexOf(status) < 0) {
    return;
  }

  const room = await roomRepo.findOrFail({slug: roomSlug});
  const owner = await room.getUser();

  if (status === room.status || owner.id !== id) {
    return;
  }

  room.status = status;
  await room.save();

  for (const client of await room.getUsers()) {
    if (client.id !== id) {
      socketPull.emit(client.id, 'broadcastSetStatus', status);
    }
  }
};
