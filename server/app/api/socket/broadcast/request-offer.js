const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');
const {STATUS_ONLINE} = require('../../../../../shared/const/status');

module.exports = async (socket, {id}, server, {roomSlug}) => {
  const room = await roomRepo.findOrFail({slug: roomSlug});
  const owner = await room.getUser();

  if (id === owner.id && room.status === STATUS_ONLINE) {
    return;
  }

  socketPull.emit(owner.id, 'rtcRequestOffer', {
    slaveId: id,
  });
};
