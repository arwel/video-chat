const roomRepo = require('../../../repositories/room-repository');
const socketPull = require('../../../services/socket-pull');
const {TYPE_BROADCAST} = require('../../../../../shared/const/room-types');

module.exports = async (socket, {id}, server, {roomSlug}) => {
  const room = await roomRepo.findOrFail({slug: roomSlug});
  if (room.type !== TYPE_BROADCAST) {
    return;
  }

  const broadcaster = await room.getUser();
  const isBroadcaster = broadcaster.id === id;
  const broadcasterData = broadcaster.getPublicData();

  socketPull.emit(id, 'broadcastSetIsBroadcaster', isBroadcaster);
  socketPull.emit(id, 'broadcastSetStatus', room.status);
  if (!isBroadcaster) {
    socketPull.emit(id, 'broadcastSetBroadcaster', broadcasterData);

    return;
  }

  for (const client of await room.getUsers()) {
    if (client.id !== id) {
      socketPull.emit(client.id, 'broadcastSetBroadcaster', broadcasterData);
    }
  }
};
