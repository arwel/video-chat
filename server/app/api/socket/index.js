module.exports = {
  room: require('./room'),
  calls: require('./calls'),
  rtc: require('./rtc'),
  broadcast: require('./broadcast'),
  user: require('./user'),
  record: require('./record'),
  permissions: require('./permissions'),
  chat: require('./chat'),
  dialog: require('./dialog'),
  uploadedFiles: require('./uploaded-files'),
};
