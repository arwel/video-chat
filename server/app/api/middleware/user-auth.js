const {auth} = require('../../utils/token');
const {User} = require('../../entity');
const {UNAUTHORIZED, getStatusText} = require('http-status-codes');
const logger = require('../../services/loger');

module.exports = async (req, res, next) => {
  const [_, token] = `${req.header(User.AUTH_HEADER)}`.split(' ');

  try {
    req.user = await auth(token, req.server);

    if (!req.user) {
      throw new Error('user auth failed');
    }

    next();
  } catch (e) {
    logger.error(e.message, e);

    res.status(UNAUTHORIZED).json({
      errors: [
        getStatusText(UNAUTHORIZED),
      ],
    });
  }
};
