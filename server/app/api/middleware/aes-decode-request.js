const {BAD_REQUEST, getStatusText} = require('http-status-codes');
const logger = require('../../services/loger');
const {Server} = require('../../entity');
const {decrypt} = require('../../utils/crypto');

module.exports = async (req, res, next) => {
  try {
    const token = `${req.header(Server.AUTH_HEADER)}`;
    const iv = `${req.server.iv}`;
    const payload = decrypt(req.body.payload, token, iv);

    delete req.body.payload;

    req.body = JSON.parse(payload);

    next();
  } catch (e) {
    logger.error(e.message, e);

    res.json({
      errors: [
        getStatusText(BAD_REQUEST),
      ],
    }, BAD_REQUEST);
  }
};
