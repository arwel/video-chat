const serverRepo = require('../../repositories/server-repository');
const {Server} = require('../../entity');
const {UNAUTHORIZED, getStatusText} = require('http-status-codes');
const logger = require('../../services/loger');

module.exports = async (req, res, next) => {
  const token = `${req.header(Server.AUTH_HEADER)}`;

  try {
    const server = await serverRepo.findOrFail({token});
    if (!server) {
      throw new Error(`Unauthorized by token ${token}`);
    }

    req.server = server;
    next();
  } catch (e) {
    logger.error(e.message, e);

    res.status(UNAUTHORIZED).json({
      errors: [
        getStatusText(UNAUTHORIZED),
      ],
    });
  }
};
