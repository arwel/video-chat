const {PATH_TO_SERVER_KEYS} = require('../../../config/path');
const {BAD_REQUEST, getStatusText} = require('http-status-codes');
const logger = require('../../services/loger');
const {join} = require('path');
const {loadFile} = require('../../utils/fs');
const {Server} = require('../../entity');
const {decrypt} = require('../../utils/crypto');

module.exports = async (req, res, next) => {
  try {
    const privateKeyPath = join(PATH_TO_SERVER_KEYS, req.server.privateKeyPath);
    const privateKey = await loadFile(privateKeyPath);
    const token = `${req.header(Server.AUTH_HEADER)}`;
    const payload = decrypt(req.body.payload, token, privateKey);

    delete req.body.payload;

    req.body = JSON.parse(payload);

    next();
  } catch (e) {
    logger.error(e.message, e);

    res.json({
      errors: [
        getStatusText(BAD_REQUEST),
      ],
    }, BAD_REQUEST);
  }
};
