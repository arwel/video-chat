const {Model, INTEGER, DATE, TEXT, STRING} = require('sequelize');
const db = require('../services/db');
const {
  CHANGE_TYPE_CREATED,
  CHANGE_TYPE_UPDATED,
  CHANGE_TYPE_DELETED,
} = require('../../../shared/const/message-change-type');

class MessageHistory extends Model {
}

MessageHistory.init({
    id: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    content: {
      type: TEXT,
      allowNull: true,
    },
    changeType: {
      type: STRING,
      field: 'change_type',
      allowNull: false,
      validate: {
        notEmpty: true,
        isIn: {
          args: [
            [
              CHANGE_TYPE_CREATED,
              CHANGE_TYPE_UPDATED,
              CHANGE_TYPE_DELETED,
            ],
          ],
        },
      },
    },
    MessageId: {
      type: INTEGER(11),
      allowNull: false,
      references: {
        model: 'message',
        key: 'id',
      },
      field: 'message_id',
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'MessageHistory',
    tableName: 'message_history',
    hooks: true,
  });

module.exports = MessageHistory;


