const {Model, INTEGER, DATE, BIGINT, STRING} = require('sequelize');
const db = require('../services/db');

class File extends Model {
  getPublicData() {
    return {
      name: this.name,
      mimeType: this.mimeType,
      size: this.size,
      id: this.id,
    };
  }
}

File.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: INTEGER(11),
    },
    name: {
      type: STRING,
      allowNull: false,
    },
    path: {
      type: STRING,
      allowNull: false,
    },
    mimeType: {
      type: STRING,
      field: 'mime_type',
      allowNull: false,
    },
    size: {
      type: BIGINT,
      allowNull: false,
    },
    MessageId: {
      type: INTEGER(11),
      allowNull: true,
      references: {
        model: 'message',
        key: 'id',
      },
      field: 'message_id',
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'File',
    tableName: 'file',
  },
);

module.exports = File;


