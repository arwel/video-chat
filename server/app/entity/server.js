const {Model, INTEGER, STRING, DATE} = require('sequelize');
const db = require('../services/db');
const {toObject} = require("../utils/etc");
const {ENV_PROD} = require('../../../shared/const/env');
const {ENV} = process.env;

const isProd = ENV_PROD === ENV;

class Server extends Model {
  static GET_USER_DATA = 'GET_USER_DATA';
  static SET_USER_JWT = 'SET_USER_JWT';
  static VIDEO_COMPLETE = 'VIDEO_COMPLETE';
  static SET_SERVER_TOKEN = 'SET_SERVER_TOKEN';
  static AUTH_HEADER = 'X-Server-Token';

  async getHooksAsObject() {
    return toObject(await this.getWebHooks(), 'key', 'url');
  }
}

Server.init(
  {
    id: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: STRING,
      allowNull: false,
      validate: {
        min: 5,
        max: 255,
        notEmpty: true
      },
    },
    description: {
      type: STRING,
      allowNull: false,
      validate: {
        min: 10,
        max: 1024,
        notEmpty: true
      }
    },
    url: {
      type: STRING,
      allowNull: false,
      validate: isProd ? {
        isUrl: {
          msg: 'url must be a URL with https protocol.',
          args: [{
            protocols: ['https'],
            require_valid_protocol: true,
            require_protocol: true
          }]
        },
        min: 5,
      } : {},
      unique: true
    },
    ip: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isIP: true
      },
      unique: true
    },
    token: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
      unique: true
    },
    iv: {
      type: STRING,
      allowNull: false,
      field: 'iv',
      validate: {
        // min: 3,
        // max: 300,
        notEmpty: true
      }
    },
    locale: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'Server',
    tableName: 'server',
  }
);

module.exports = Server;
