const {Model, INTEGER, STRING, DATE} = require('sequelize');
const db = require('../services/db');
const {ENV_PROD} = require('../../../shared/const/env');


class WebHook extends Model {
}

WebHook.init(
    {
      id: {
        type: INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      key: {
        type: STRING,
        validate: {
          min: 5,
          max: 255,
          notEmpty: true,
        },
      },
      url: {
        type: STRING,
        allowNull: false,
        validate: process.env.ENV === ENV_PROD ? {
          isUrl: {
            msg: 'url must be a URL with https protocol.',
            args: [{
              protocols: ['https'],
              require_valid_protocol: true,
              require_protocol: true,
            }],
          },
          min: 5,
        } : {},
      },
      ServerId: {
        type: INTEGER(11),
        allowNull: true,
        references: {
          model: 'Server',
          key: 'id',
        },
        field: 'server_id',
      },
      createdAt: {
        allowNull: false,
        type: DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: DATE,
        field: 'updated_at',
      },
    },
    {
      sequelize: db.getConnection(),
      modelName: 'WebHook',
      tableName: 'web_hook',
    }
);

module.exports = WebHook;
