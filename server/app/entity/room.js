const {
  TYPE_CALL,
  TYPE_BROADCAST,
  TYPE_CHAT,
  TYPE_ANONYMOUS_CHAT,
  TYPE_GROUP_CHAT,
} = require('../../../shared/const/room-types');

const {Model, INTEGER, STRING, DATE, BOOLEAN} = require('sequelize');
const db = require('../services/db');
const {dateToStr} = require('../../../shared/utils/normalize');
const {
  STATUS_OFFLINE,
  STATUS_ONLINE,
  STATUS_PENDING,
  STATUS_DELETED,
} = require("../../../shared/const/status");

class Room extends Model {

  getPublicData() {
    return {
      id: this.id,
      title: this.title,
      type: this.type,
      status: this.status,
      slug: this.slug,
      userId: this.UserId,
      updatedAt: dateToStr(this.updatedAt),
      createdAt: dateToStr(this.createdAt),
    };
  }
}

Room.init({
    id: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    type: {
      type: STRING,
      allowNull: false,
      defaultValue: TYPE_CALL,
      validate: {
        notEmpty: true,
        isIn: {
          args: [
            [
              TYPE_CALL,
              TYPE_BROADCAST,
              TYPE_CHAT,
              TYPE_ANONYMOUS_CHAT,
              TYPE_GROUP_CHAT,
            ]
          ],
        },
      },
    },
    videoReturnUrl: {
      type: STRING,
      allowNull: true,
      field: 'video_return_url',
    },
    videoRecordSupport: {
      type: BOOLEAN,
      field: 'video_record_support',
      defaultValue: true,
    },
    status: {
      type: STRING,
      allowNull: false,
      defaultValue: STATUS_OFFLINE,
      validate: {
        notEmpty: true,
        isIn: {
          args: [
            [
              STATUS_ONLINE,
              STATUS_OFFLINE,
              STATUS_PENDING,
              STATUS_DELETED,
            ]
          ]
        },
      }
    },
    slug: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
    recordsPath: {
      type: STRING,
      allowNull: true,
      field: 'records_path',
    },
    ServerId: {
      type: INTEGER(11),
      allowNull: true,
      references: {
        model: 'Server',
        key: 'id'
      },
      field: 'server_id'
    },
    UserId: {
      type: INTEGER(11),
      allowNull: true,
      references: {
        model: 'User',
        key: 'id'
      },
      field: 'user_id'
    },
    expiredAt: {
      type: DATE,
      allowNull: false,
      field: 'expired_at',
      validate: {
        isDate: true
      },
      defaultValue: new Date()
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'Room',
    tableName: 'room',
    hooks: true,
  }
);

module.exports = Room;


