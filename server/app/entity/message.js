const {Model, INTEGER, DATE, TEXT, BOOLEAN, STRING} = require('sequelize');
const db = require('../services/db');
const {STATUS_ACTIVE} = require('../../../shared/const/status');

class Message extends Model {
  async getPublicData(loadExtended = true, anonymous = false) {
    const reply = loadExtended && this.ReplyId
      ? await (await this.getMessage()).getPublicData(false, anonymous)
      : null;

    return {
      id: this.id,
      message: this.content,
      user: (await this.getUser()).getPublicData(
        null,
        anonymous,
        this.RoomId
      ),
      roomId: this.RoomId,
      date: this.createdAt,
      reply,
      seen: this.seen,
      files: loadExtended
        ? (await this.getFiles())
          .map((file) => file.getPublicData())
        : [],
    };
  }
}

Message.init({
  id: {
    type: INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  content: {
    type: TEXT,
    allowNull: true,
  },
  status: {
    type: STRING,
    defaultValue: STATUS_ACTIVE,
  },
  seen: {
    type: BOOLEAN,
    defaultValue: false,
  },
  ReplyId: {
    type: INTEGER(11),
    allowNull: true,
    references: {
      model: 'message',
      key: 'id',
    },
    field: 'message_id',
  },
  UserId: {
    type: INTEGER(11),
    allowNull: false,
    references: {
      model: 'user',
      key: 'id',
    },
    field: 'user_id',
  },
  RoomId: {
    type: INTEGER(11),
    allowNull: false,
    references: {
      model: 'room',
      key: 'id',
    },
    field: 'room_id',
  },
  createdAt: {
    allowNull: false,
    type: DATE,
    field: 'created_at',
  },
  updatedAt: {
    allowNull: false,
    type: DATE,
    field: 'updated_at',
  },
},
{
  sequelize: db.getConnection(),
  modelName: 'Message',
  tableName: 'message',
  hooks: true,
});

module.exports = Message;


