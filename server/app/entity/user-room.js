const {Model, INTEGER, DATE, STRING} = require('sequelize');
const db = require('../services/db');
const {
  UR_STATUS_ACTIVE,
  UR_STATUS_MUTED,
} = require('../../../shared/const/user-room-status');

class UserRoom extends Model {
}

UserRoom.init(
    {
      id: {
        type: INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      UserId: {
        type: INTEGER(11),
        allowNull: true,
        references: {
          model: 'User',
          key: 'id',
        },
        field: 'user_id',
      },
      RoomId: {
        type: INTEGER(11),
        allowNull: true,
        references: {
          model: 'Room',
          key: 'id',
        },
        field: 'room_id',
      },
      status: {
        type: STRING,
        allowNull: false,
        defaultValue: UR_STATUS_ACTIVE,
        validate: {
          notEmpty: true,
          isIn: {
            args: [
              [
                UR_STATUS_ACTIVE,
                UR_STATUS_MUTED,
              ],
            ],
          },
        },
      },
      startStatusAt: {
        type: DATE,
        allowNull: true,
      },
      stopStatusAt: {
        type: DATE,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: DATE,
        field: 'updated_at',
      },
    },
    {
      sequelize: db.getConnection(),
      modelName: 'UserRoom',
      tableName: 'user_room',
    }
);

module.exports = UserRoom;


