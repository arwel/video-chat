const Server = require('./server');
const WebHook = require('./web-hook');
const User = require('./user');
const Room = require('./room');
const UserRoom = require('./user-room');
const Message = require('./message');
const File = require('./file');
const MessageHistory = require('./message-history');
const MessageTranslation = require('./message-translation');

Server.webHooks = Server.hasMany(WebHook, {
  as: 'webHooks',
  foreignKey: 'ServerId',
  sourceKey: 'id',
});
WebHook.server = WebHook.belongsTo(Server, {
  as: 'server',
  foreignKey: 'ServerId',
  targetKey: 'id',
});
User.server = User.belongsTo(Server, {
  as: 'server',
  foreignKey: 'ServerId',
  targetKey: 'id',
});
Room.server = Room.belongsTo(Server, {
  as: 'server',
  foreignKey: 'ServerId',
  targetKey: 'id',
});
Room.user = Room.belongsTo(User, {
  as: 'user',
  foreignKey: 'UserId',
  targetKey: 'id',
});
Message.user = Message.belongsTo(User, {
  as: 'user',
  foreignKey: 'UserId',
  targetKey: 'id',
});
Message.room = Message.belongsTo(Room, {
  as: 'room',
  foreignKey: 'RoomId',
  targetKey: 'id',
});
File.message = File.belongsTo(Message, {
  as: 'message',
  foreignKey: 'MessageId',
  targetKey: 'id',
});
Message.files = Message.hasMany(File, {
  as: 'files',
  foreignKey: 'MessageId',
  sourceKey: 'id',
  onDelete: 'cascade',
  hooks: true,
});
Message.reply = Message.belongsTo(Message, {
  as: 'message',
  foreignKey: 'ReplyId',
  targetKey: 'id',
  onDelete: 'set null',
  hooks: true,
});
User.rooms = User.belongsToMany(Room, {
  through: 'UserRoom',
  foreignKey: 'UserId',
  otherKey: 'RoomId',
});
User.messages = User.hasMany(Message, {
  as: 'messages',
  foreignKey: 'UserId',
  sourceKey: 'id',
  onDelete: 'cascade',
  hooks: true,
});
Room.users = Room.belongsToMany(User, {
  through: 'UserRoom',
  foreignKey: 'RoomId',
  otherKey: 'UserId',
});
Message.history = Message.hasMany(MessageHistory, {
  as: 'history',
  foreignKey: 'MessageId',
  sourceKey: 'id',
  onDelete: 'cascade',
  hooks: true,
});
MessageHistory.message = MessageHistory.belongsTo(Message, {
  as: 'message',
  foreignKey: 'MessageId',
  targetKey: 'id',
  onDelete: 'cascade',
  hooks: true,
});
MessageTranslation.message = MessageTranslation.belongsTo(Message, {
  as: 'message',
  foreignKey: 'MessageId',
  targetKey: 'id',
  onDelete: 'cascade',
  hooks: true,
});

module.exports = {
  Server,
  WebHook,
  User,
  Room,
  UserRoom,
  Message,
  MessageHistory,
  File,
  MessageTranslation,
};
