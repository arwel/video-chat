const {STATUS_OFFLINE, STATUS_ONLINE} = require('../../../shared/const/status');
const {Model, INTEGER, STRING, DATE, BOOLEAN} = require('sequelize');
const db = require('../services/db');
const socketPull = require('../services/socket-pull');

class User extends Model {
  static AUTH_HEADER = 'Authorization';

  getPublicData(status, anonimous, roomId) {
    const defaultStatus = socketPull.has(this.id)
      ? STATUS_ONLINE
      : STATUS_OFFLINE;

    return {
      nick: anonimous ?  `User ${this.id * roomId}` : this.nick,
      id: this.id,
      status: status ? status : defaultStatus,
      hasPeer: socketPull.hasPeer(this.id),
      peer: this.id,
      avatar: anonimous ? null : this.avatar,
      hasCamera: this.hasCamera,
      hasMicrophone: this.hasMicrophone,
      position: this.position,
    };
  }
}

User.init(
  {
    id: {
      type: INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    clientId: {
      type: INTEGER(11),
      allowNull: false,
      field: 'client_id',
    },
    nick: {
      type: STRING,
      allowNull: false,
      validate: {
        min: 5,
        max: 255,
        notEmpty: true,
      },
    },
    avatar: {
      type: STRING,
      allowNull: true,
    },
    position: {
      type: STRING,
      allowNull: true,
    },
    locale: {
      type: STRING,
      allowNull: true,
    },
    email: {
      type: STRING,
      allowNull: false,
      validate: {
        min: 5,
        max: 255,
        isEmail: true,
        notEmpty: true,
      },
    },
    hasCamera: {
      type: BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    hasMicrophone: {
      type: BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    ServerId: {
      type: INTEGER(11),
      allowNull: true,
      references: {
        model: 'Server',
        key: 'id',
      },
      field: 'server_id',
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'User',
    tableName: 'user',
  },
);

module.exports = User;
