const {Model, INTEGER, DATE, TEXT, STRING} = require('sequelize');
const db = require('../services/db');

class MessageTranslation extends Model {
  getPublicData() {
    return {
      id: this.id,
      translation: this.content,
      locale: this.locale,
      messageId: this.MessageId,
    };
  }
}

MessageTranslation.init({
    id: {
      type: INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    content: {
      type: TEXT,
      allowNull: true,
    },
    locale: {
      type: STRING,
      allowNull: false,
    },
    MessageId: {
      type: INTEGER(11),
      allowNull: false,
      references: {
        model: 'message',
        key: 'id',
      },
      field: 'message_id',
    },
    createdAt: {
      allowNull: false,
      type: DATE,
      field: 'created_at',
    },
    updatedAt: {
      allowNull: false,
      type: DATE,
      field: 'updated_at',
    },
  },
  {
    sequelize: db.getConnection(),
    modelName: 'MessageTranslation',
    tableName: 'message_translation',
    hooks: true,
  });

module.exports = MessageTranslation;


