const BaseRepository = require('./base-repository');
const {MessageTranslation} = require('../entity');

class MessageTranslationRepository extends BaseRepository {
  constructor() {
    super(MessageTranslation);
  }

  getMessageTranslations(messageId) {
    return MessageTranslation.findAll({
      where: {
        MessageId: messageId,
      },
    });
  }
}

module.exports = new MessageTranslationRepository();
