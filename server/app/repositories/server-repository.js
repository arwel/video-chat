const BaseRepository = require('./base-repository');
const {Server} = require('../entity');

class ServerRepository extends BaseRepository {
  constructor() {
    super(Server);
  }
}

module.exports = new ServerRepository();
