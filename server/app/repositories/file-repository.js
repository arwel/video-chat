const BaseRepository = require('./base-repository');
const {File} = require('../entity');

class FileRepository extends BaseRepository {
  constructor() {
    super(File);
  }
}

module.exports = new FileRepository();
