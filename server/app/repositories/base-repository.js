const logger = require('../services/loger');

module.exports = class BaseRepository {
  modelClass;

  constructor(Model) {
    this.modelClass = Model;
  }

  async findOrFail(where) {
    const model = await this.modelClass.findOne({where});
    if (!model) {
      logger.error('Model not found with', this.modelClass);
      throw new Error('Model not found with');
    }

    return model;
  }

  async findOrCreateUnsafe(params) {
    const entity = await this.modelClass.findOne({
      where: params.where,
    });

    if (entity) {
      return [entity];
    }

    return this.modelClass.bulkCreate([params.defaults]);
  }

  findOrCreate(params) {
    return this.modelClass.findOrCreate(params);
  }

  find(id) {
    return this.modelClass.findByPk(id);
  }

  findOneBy(where, order = []) {
    return this.modelClass.findOne({where, order});
  }

  findAll(params) {
    return this.modelClass.findAll(params);
  }

  create(params) {
    return this.modelClass.create(params)
  }

  destroy(params) {
    return this.modelClass.destroy(params);
  }

  count(params) {
    return this.modelClass.count(params);
  }

  update(values, options) {
    return this.modelClass.update(values, options);
  }
};
