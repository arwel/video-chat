const BaseRepository = require('./base-repository');
const {User} = require('../entity');

class UserRepository extends BaseRepository {
  constructor() {
    super(User);
  }
}

module.exports = new UserRepository();
