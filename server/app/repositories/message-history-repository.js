const BaseRepository = require('./base-repository');
const {MessageHistory} = require('../entity');

class MessageHistoryRepository extends BaseRepository {
  constructor() {
    super(MessageHistory);
  }

  create(data) {
    return MessageHistory.create(data);
  }
}

module.exports = new MessageHistoryRepository();
