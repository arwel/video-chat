const {Op: {ne}} = require('sequelize');
const BaseRepository = require('./base-repository');
const messageHistoryRepo = require('./message-history-repository');
const userRoomRepo = require('./user-room-repository');
const {STATUS_ACTIVE} = require('../../../shared/const/status');
const {Message} = require('../entity');
const {STATUS_DELETED} = require('../../../shared/const/status');
const {
  CHANGE_TYPE_DELETED,
  CHANGE_TYPE_CREATED,
  CHANGE_TYPE_UPDATED,
} = require('../../../shared/const/message-change-type');

class MessageRepository extends BaseRepository {
  constructor() {
    super(Message);
  }

  async createWithHistory(data) {
    const msg = await Message.create(data);
    await messageHistoryRepo.create({
      content: msg.content,
      changeType: CHANGE_TYPE_CREATED,
      MessageId: msg.id,
    });

    return msg;
  }

  async getUserNewMessages(userId) {
    const roomsIds = await userRoomRepo.getRoomsIdsByUser(userId);

    const [count, messages] = await Promise.all([
      this.getNewMessagesCount(userId, roomsIds),
      this.getLastNewMessages(userId, roomsIds),
    ]);

    return {
      count,
      messages,
    };
  }

  getNewMessagesCount(userId, roomsIds) {
    return Message.count({
      where: {
        RoomId: roomsIds,
        seen: false,
        UserId: {
          [ne]: userId,
        },
      },
    });
  }

  async getLastNewMessages(userId, roomsIds) {
    const messages = await Message.findAll({
      where: {
        RoomId: roomsIds,
        seen: false,
        UserId: {
          [ne]: userId,
        },
      },
      limit: 3,
      order: [
        ['id', 'DESC'],
      ],
    });

    return Promise.all(messages.map((message) => message.getPublicData(false)));
  }

  async softDeleteWithHistory(message) {
    message.status = STATUS_DELETED;
    await message.save();

    await messageHistoryRepo.create({
      content: message.content,
      changeType: CHANGE_TYPE_DELETED,
      MessageId: message.id,
    });
  }

  async getMessagesData(roomId, offset, limit, anonymous = false) {
    const messages = await Message.findAll({
      where: {
        RoomId: roomId,
        status: STATUS_ACTIVE,
      },
      limit,
      offset: +offset,
      order: [
        ['created_at', 'DESC'],
        ['id', 'DESC'],
      ],
    });

    return Promise.all([...messages].map((msg) => {
      return msg.getPublicData(true, anonymous);
    }));
  }

  async updateWithHistory(message, params) {
    for (const prop of Object.keys(params)) {
      message[prop] = params[prop];
    }

    await message.save();

    await messageHistoryRepo.create({
      content: message.content,
      changeType: CHANGE_TYPE_UPDATED,
      MessageId: message.id,
    });
  }

  setSeen(ids) {
    return Message.update({
      seen: true,
    }, {
      where: {
        id: ids,
      },
    });
  }

  async getLastRoomMessage(roomId) {
    const message = await this.findOneBy({
      RoomId: roomId,
    }, [
      ['id', 'DESC'],
    ]);

    return message ? await message.getPublicData(false) : null;
  }

  async getDialogStats(userId, roomId) {
    await userRoomRepo.findOrFail({
      RoomId: roomId,
      UserId: userId,
    });

    const [members, notReadCount, lastMessage] = await Promise.all([
      userRoomRepo.getMembersData(roomId, userId),
      this.getNewMessagesCount(userId, roomId),
      this.getLastRoomMessage(roomId),
    ]);

    return {
      roomId,
      stats: {
        members,
        notReadCount,
        lastMessage,
      },
    };
  }
}

module.exports = new MessageRepository();
