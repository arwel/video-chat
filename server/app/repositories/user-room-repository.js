const BaseRepository = require('./base-repository');
const userRepo = require('./user-repository');
const roomRepo = require('./room-repository');
const {UserRoom} = require('../entity');
const {TYPE_ANONYMOUS_CHAT} = require('../../../shared/const/room-types');

class UserRoomRepository extends BaseRepository {
  constructor() {
    super(UserRoom);
  }

  async getMemberIds(roomId) {
    const members = await UserRoom.findAll({
      where: {
        RoomId: roomId,
      },
    });

    return members.map(({UserId}) => UserId);
  }

  async getRoomsIdsByUser(userId) {
    const userRooms = await UserRoom.findAll({
      where: {
        UserId: userId,
      },
    });

    return userRooms.map(({RoomId}) => RoomId);
  }

  async getUsersOfRoom(roomId, limit = 3, offset = 0) {
    const lastUsersInRoom = await UserRoom.findAll({
      where: {
        RoomId: roomId,
      },
      limit,
      offset,
      order: [
        ['id', 'DESC'],
      ],
    });

    const room = await roomRepo.find(roomId);
    const isAnonymous = room.type === TYPE_ANONYMOUS_CHAT;

    const users = await userRepo.findAll({
      where: {
        id: lastUsersInRoom.map(({UserId}) => UserId),
      },
    });

    return users.map((user) => user.getPublicData(null, isAnonymous, roomId));
  }

  async getRoomsOfUser(userId, type, limit, offset = 0) {
    const userRooms = await UserRoom.findAll({
      where: {
        UserId: userId,
      },
    });

    return roomRepo.findAll({
      where: {
        id: userRooms.map(({RoomId}) => RoomId),
        type,
      },
      limit,
      offset,
      order: [
        ['updated_at', 'DESC'],
      ],
    });
  }

  async getMembersData(roomId, currentUserId) {
    const lastUsersInRoom = await this.getUsersOfRoom(roomId);

    return lastUsersInRoom.filter(
        ({id} = {}) => id && id !== currentUserId
    );
  }
}

module.exports = new UserRoomRepository();
