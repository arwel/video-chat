const BaseRepository = require('./base-repository');
const {STATUS_DELETED} = require('../../../shared/const/status');
const {Room} = require('../entity');

class RoomRepository extends BaseRepository {
  constructor() {
    super(Room);
  }

  updateDate(room, date = new Date()) {
    room.updatedAt = date;
    room.changed('updatedAt', true);

    return room.save();
  }

  softDelete(ids) {
    return Room.update(
        {
          status: STATUS_DELETED,
        },
        {
          where: {
            id: ids,
          },
        },
    );
  }
}

module.exports = new RoomRepository();
