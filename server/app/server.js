const {config} = require('dotenv');
config();
const {
  LOG_PATH,
  DB_NAME,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_DIALECT,
  SSL_KEY_PATH,
  SSL_CERT_PATH,
  PORT,
} = process.env;

const logger = require('./services/loger');
logger.init({
  dirname: LOG_PATH,
});

const db = require('./services/db');
db.init({
  database: DB_NAME,
  username: DB_USER,
  password: DB_PASSWORD,
  host: DB_HOST,
  dialect: DB_DIALECT,
});

const Server = require('./services/server');

const server = new Server();
server.run(
    SSL_KEY_PATH,
    SSL_CERT_PATH,
    PORT
);

