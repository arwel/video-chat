const {existsSync, createReadStream} = require('fs');
const {join} = require('path');
const {createServer} = require('https');

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const socketIo = require('socket.io');
const {ExpressPeerServer} = require('peer');

const {loadAllFiles} = require('../utils/fs');
const {post} = require('../utils/request');
const {createToken} = require('../utils/token');
const {Server} = require('../entity');
const Router = require('./router');
const {auth} = require('../utils/token');
const socketPull = require('./socket-pull');
const routes = require('../api/socket');
const logger = require('./loger');
const apiV1Routes = require('../api/v1');
const serverRepo = require('../repositories/server-repository');
const roomRepo = require('../repositories/room-repository');
const userRepo = require('../repositories/user-repository');

class SocketServer {
  #router = null;

  run = async (keyPath, certPath, port) => {
    const [key, cert] = await loadAllFiles([
      keyPath,
      certPath,
    ]);

    this.#router = new Router(routes);

    const expressApp = express();
    expressApp.use(bodyParser.json());
    expressApp.use(cors());
    expressApp.use('/api/v1', apiV1Routes);

    expressApp.get('/record/:token/:room/:record', this.expressGetRecord);

    const server = createServer({
      key,
      cert,
    }, expressApp);

    const peerServer = ExpressPeerServer(server, {
      debug: true,
      allow_discovery: true,
    });
    expressApp.use(peerServer);
    peerServer.on('connection', this.peerConnectedHandler);

    const io = socketIo(server, {
      origins: '*:*',
    });
    server.listen(port);

    io.on('connection', this.socketConnectionHandler.bind(this, peerServer));
  };

  peerConnectedHandler = (peerId) => {
    socketPull.setPeerId(peerId, peerId);
  };

  expressGetRecord = async (req, res) => {
    const {token, room, record} = req.params;

    try {
      const server = await serverRepo.findOrFail({token});
      const roomModel = await roomRepo.findOrFail({
        slug: room,
        ServerId: server.id,
      });

      const videoPath = join(roomModel.recordsPath, `${record}.webm`);
      if (!existsSync(videoPath)) {
        throw new Error('Video not found for room ' + roomModel.id + ' ' + videoPath);
      }

      const rs = createReadStream(videoPath);
      rs.on('open', () => {
        rs.pipe(res);
      });
    } catch (e) {
      logger.error('Socket connection error', e);
      res.status(404).send('');
    }
  };

  socketConnectionHandler = async (peerServer, socket) => {
    try {
      const {serverToken, token} = socket.handshake.query;
      const server = await serverRepo.findOrFail({token: serverToken});
      const user = await auth(token, server);

      socket.on(
        'updateToken',
        this.socketUpdateTokenHandler.bind(this, server)
      );

      socket.on(
        'forceLogout',
        this.socketForceLogoutHandle.bind(this, peerServer)
      );

      if (user) {
        this.#handleAuthenticatedUser(user, socket, server, peerServer);
      } else {
        socket.emit('socketOpened');
      }
    } catch (e) {
      logger.error('Socket connection error', e);
    }
  };

  socketForceLogoutHandle = (peerServer, {id, peer}) => {
    peerServer.emit('close', peer);
    peerServer.emit('disconnect', peer);
    socketPull.closeAll(id);
    socketPull.removeAll(id);
  };

  #handleAuthenticatedUser = async (user, socket, server) => {
    // if (socketPull.has(user.id)) {
    //   socketPull.emit(user.id, 'authFromAnotherDevice', {});
    //   socketPull.closeAll(user.id);
    // }

    socketPull.add(user.id, socket);
    await socket.on('action',
      this.socketActionHandler.bind(this, socket, user, server)
    );
    socket.emit('socketOpened');
  };

  socketActionHandler = async (socket, user, server, {route, data}) => {
    const handler = this.#router.getHandler(route);
    try {
      await handler(socket, user, server, data);
    } catch (e) {
      logger.error('Socket action error', e);
    }
  };

  socketUpdateTokenHandler = async (server, {id, serverToken}) => {
    try {
      const webHooks = await server.getHooksAsObject();
      const user = await userRepo.findOrFail({
        clientId: id,
        ServerId: server.id,
      });

      const token = await createToken({
        serverToken,
        email: user.email,
        id: user.clientId,
      });

      await post(webHooks[Server.SET_USER_JWT], {
        id,
        token,
      });
    } catch (e) {
      console.log(e);
      logger.error('Socket update token error', e);
    }
  };
}

module.exports = SocketServer;
