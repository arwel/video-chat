const {PATH_TO_SERVER_RECORDS} = require('../../config/path');
const {makeDateDir} = require('../utils/fs');
const {join} = require('path');
const {createWriteStream} = require('fs');

class RecordManager {
  #writeStreams;

  constructor() {
    this.#writeStreams = new Map();
  }

  #getKey = ({type, slug, fileName}) => {
    return `${type}-${slug}-${fileName}`;
  };

  save = async ({type, slug, data, fileName}) => {
    const key = this.#getKey({
      type,
      slug,
      fileName,
    });
    let {stream} = this.#writeStreams.get(key) || {};

    if (!stream) {
      const dirPath = await makeDateDir(join(PATH_TO_SERVER_RECORDS, type), slug);
      const filePath = join(dirPath, `${fileName}.${Date.now()}.webm`);

      stream = createWriteStream(filePath);
      this.#writeStreams.set(key, {
        filePath,
        stream,
      });
    }

    console.log('save chunck ', type, slug);

    stream.write(data);
  };

  close = ({type, slug, fileName}) => {
    const key = this.#getKey({
      type,
      slug,
      fileName
    });
    const {stream, filePath} = this.#writeStreams.get(key) || {};
    if (stream && filePath) {
      stream.close();
      this.#writeStreams.delete(key);
    }

    console.log('try to close stream ', type, slug);

    return filePath;
  };

  getFilePath = ({type, slug, fileName}) => {
    const key = this.#getKey({
      type,
      slug,
      fileName
    });
    const {filePath} = this.#writeStreams.get(key) || {};

    return filePath;
  };
}

module.exports = new RecordManager();
