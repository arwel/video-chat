class SocketPull {
  #sockets;
  #peerIds;

  constructor() {
    this.#sockets = new Map();
    this.#peerIds = new Map();
  }

  has = (key) => {
    return this.#sockets.has(key);
  };

  hasPeer = (key) => {
    return this.#peerIds.has(`${key}`);
  };

  setPeerId = (key, id) => {
    this.#peerIds.set(key, id);
  };

  add = (key, socket) => {
    let pull = this.#sockets.get(key);
    if (!pull) {
      pull = new Map();
      this.#sockets.set(key, pull);
    }

    pull.set(socket.id, socket);
    socket.on('disconnect', () => {
      this.remove(key, socket);
    });
  };

  close = (key, {id}) => {
    this.#peerIds.delete(key);
    const pull = this.#sockets.get(key);
    if (!pull) {
      return null;
    }

    const socket = pull.get(id);
    console.log('get socket for id ', id);
    if (socket) {
      console.log('try to close socket ', key);
      socket.close && socket.close();
      return socket.disconnect();
    }

    return null;
  };

  closeAll = (key) => {
    this.#peerIds.delete(key);
    const pull = this.#sockets.get(key);
    if (!pull) {
      return;
    }

    pull.forEach((socket) => {
      socket.disconnect();
      socket.close && socket.close();
    });
  };

  remove = (key, socket) => {
    this.#peerIds.delete(key);
    const pull = this.#sockets.get(key);
    if (!pull) {
      return;
    }

    pull.delete(socket.id);

    if (pull.size < 1) {
      this.removeAll(key);
    }
  };

  removeAll = (key) => {
    this.#peerIds.delete(key);
    this.#sockets.delete(key);
  };

  emit = (key, event, payload) => {
    const pull = this.#sockets.get(key);
    if (!pull) {
      return;
    }

    pull.forEach((socket) => {
      socket.emit(event, payload);
    });
  };
}

module.exports = new SocketPull();
