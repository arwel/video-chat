module.exports = class Router {
  #routes = {};

  constructor(routes) {
    this.#routes = routes;
  }

  getHandler = (route) => {
    let routes = this.#routes;
    let pathArr = route.split('/');

    for (const pathItem of pathArr) {
      const route = routes[pathItem];
      if (!route) {
        break;
      }

      routes = route;
    }

    return routes;
  };
};
