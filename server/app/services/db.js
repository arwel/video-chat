const Sequelize = require('sequelize');
const logger = require('./loger');

class Db {
  #connection = null;

  init = ({database, username, password, host, dialect}) => {
    if (!this.#connection) {
      this.#connection = new Sequelize(database, username, password, {
        host,
        dialect
      });
    }
  };

  getConnection = () => {
    if (!this.#connection) {
      logger.error(`ERROR DB connection`);
      throw new Error('Db not configured');
    }

    return this.#connection;
  };
}

module.exports = new Db();
