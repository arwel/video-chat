const winston = require('winston');
const {LOG_INFO, LOG_DEBUG, LOG_ERROR, LOG_WARN} = require("../const/log");
const {ENV_DEV} = require("../const/env");
const { combine, timestamp, json } = winston.format;


class Logger {
  #logger = null;

  init = ({dirname}) => {
    if (!this.#logger) {
      const logLevels = [
        LOG_INFO,
        LOG_DEBUG,
        LOG_ERROR,
        LOG_WARN
      ];

      this.#logger = winston.createLogger({
        level: process.env.ENV === ENV_DEV ? LOG_INFO : LOG_WARN,
        format: combine(
          timestamp(),
          json()
        ),
        handleExceptions: true,
        json: true,
        transports:
          logLevels.map((logLevel) => {
            return new winston.transports.File({
              dirname,
              level: logLevel,
              filename: `${logLevel}.log`
            });
        }),
      });
    }
  };

  #checkLogger = () => {
    if (!this.#logger) {
      throw new Error('Logger not configured :(');
    }
  };

  debug = (message, metadata) => {
    this.#checkLogger();
    this.#logger.debug({ level: LOG_DEBUG, message: message, metadata });
    console.log(message, metadata);
  };

  info = (message, metadata) => {
    this.#checkLogger();
    this.#logger.info({ level: LOG_INFO, message: message, metadata });
    console.log(message, metadata);
  };

  warn = (message, metadata) => {
    this.#checkLogger();
    this.#logger.warn({ level: LOG_WARN, message: message, metadata });
    console.log(message, metadata);
  };

  error = (message, metadata) => {
    this.#checkLogger();
    this.#logger.error({ level: LOG_ERROR, message: message, metadata });
    console.log(message, metadata);
  };
}

module.exports = new Logger();
