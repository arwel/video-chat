'use strict';

module.exports = {
  up: async (queryInterface) => {
    const secret = 'ac964609-54fa-41b0-ac6a-995aec0e';
    const iv = 'e37ce923a8f8f4cd';
    const date = new Date();

    return queryInterface.bulkInsert('server', [{
      title: 'Title',
      description: 'Description',
      locale: 'en',
      url: 'https://moi.health',
      ip: '127.0.0.1',
      token: secret,
      iv,
      created_at: date,
      updated_at: date,
    }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('server', null, {});
  },
};
