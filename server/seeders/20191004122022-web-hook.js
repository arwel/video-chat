'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const server_id = '1';
    return queryInterface.bulkInsert('web_hook', [{
      key: 'SET_USER_JWT',
      url: 'http://localhost:3535/set-user-jwt',
      server_id,
    }, {
      key: 'SET_SERVER_TOKEN',
      url: 'http://localhost:3535/set-server-token',
      server_id,
    }, {
      key: 'GET_USER_DATA',
      url: 'http://localhost:3535/get-user-data',
      server_id,
    }, {
      key: 'VIDEO_COMPLETE',
      url: 'http://localhost:3535/video-complete',
      server_id,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('web_hook', null, {});
  }
};
