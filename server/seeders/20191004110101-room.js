'use strict';

const {
  TYPE_CHAT,
  TYPE_ANONYMOUS_CHAT,
  TYPE_GROUP_CHAT,
} = require('../../shared/const/room-types');


module.exports = {
  up: (queryInterface) => {
    const server_id = 1;
    const date = new Date();
    date.setFullYear(2020);
    return queryInterface.bulkInsert('room', [
      {
        title: 'John Doe',
        slug: 'room-slug',
        expired_at: date,
        server_id,
        user_id: 1,
        created_at: date,
        updated_at: date,
      },
      {
        title: 'Chat 1',
        slug: 'room-chat-1',
        expired_at: date,
        type: TYPE_ANONYMOUS_CHAT,
        server_id,
        user_id: 1,
        created_at: date,
        updated_at: date,
      },
      {
        title: 'Chat 2',
        slug: 'room-chat-2',
        expired_at: date,
        type: TYPE_GROUP_CHAT,
        server_id,
        user_id: 1,
        created_at: date,
        updated_at: date,
      },
      {
        title: 'Chat 3',
        slug: 'room-chat-3',
        expired_at: date,
        type: TYPE_CHAT,
        server_id,
        user_id: 1,
        created_at: date,
        updated_at: date,
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('room', null, {});
  },
};
