'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const room_id = 1;
    const date = new Date();
    return queryInterface.bulkInsert('user_room', [
      {
        user_id: 1,
        room_id,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 2,
        room_id,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 3,
        room_id,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 4,
        room_id,
        created_at: date,
        updated_at: date,
      },

      // Chat rooms
      {
        user_id: 1,
        room_id: 2,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 1,
        room_id: 3,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 1,
        room_id: 4,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 2,
        room_id: 2,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 2,
        room_id: 3,
        created_at: date,
        updated_at: date,
      },
      {
        user_id: 2,
        room_id: 4,
        created_at: date,
        updated_at: date,
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_room', null, {});
  },
};
