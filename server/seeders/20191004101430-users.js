'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const server_id = 1;
    const date = new Date();
    return queryInterface.bulkInsert('user', [{
      nick: 'First User',
      email: 'firstuser@gmail.com',
      avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/c3/c3d3d8492fa7893b65cdd26e0a8a2587c56a938d_full.jpg',
      locale: 'en',
      server_id,
      created_at: date,
      updated_at: date,
      client_id: 1,
    }, {
      nick: 'Second User',
      email: 'seconduser@gmail.com',
      avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/16/164f1597b3b0a9ffa3bb8f64e9ccccdd5d47f515_full.jpg',
      locale: 'en',
      server_id,
      created_at: date,
      updated_at: date,
      client_id: 2,
    }, {
      nick: 'Third User',
      email: 'thirduser@gmail.com',
      avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/f0/f0b3527b025fdcfdf6ef26acb333c6f6eaad677b_full.jpg',
      locale: 'en',
      server_id,
      created_at: date,
      updated_at: date,
      client_id: 3,
    }, {
      nick: 'Fourth User',
      email: 'fourthuser@gmail.com',
      avatar: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/ba/baef8f1208329ca7290f2cd376ce80cd9d67becc_full.jpg',
      locale: 'en',
      server_id,
      created_at: date,
      updated_at: date,
      client_id: 4,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  },
};
