'use strict';

const {
  CHANGE_TYPE_CREATED,
  CHANGE_TYPE_UPDATED,
  CHANGE_TYPE_DELETED,
} = require('../../shared/const/message-change-type');

const TABLE_NAME = 'message_history';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(TABLE_NAME, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11),
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      changeType: {
        type: Sequelize.STRING,
        field: 'change_type',
        allowNull: false,
        validate: {
          notEmpty: true,
          isIn: {
            args: [
              [
                CHANGE_TYPE_CREATED,
                CHANGE_TYPE_UPDATED,
                CHANGE_TYPE_DELETED,
              ],
            ],
          },
        },
      },
      MessageId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'message',
          key: 'id',
        },
        field: 'message_id',
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable(TABLE_NAME);
  },
};
