'use strict';

const {STATUS_ACTIVE} = require('../../shared/const/status');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('message', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11),
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: STATUS_ACTIVE,
      },
      Reply: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'message',
          key: 'id',
        },
        field: 'message_id',
      },
      UserId: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        references: {
          model: 'user',
          key: 'id',
        },
        field: 'user_id',
      },
      RoomId: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        references: {
          model: 'room',
          key: 'id',
        },
        field: 'room_id',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
      seen: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('message');
  },
};
