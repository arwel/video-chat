'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_room', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11),
      },
      UserId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'user',
          key: 'id',
        },
        field: 'user_id',
        onDelete: 'CASCADE',
      },
      RoomId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'room',
          key: 'id',
        },
        field: 'room_id',
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('user_room');
  },
};
