'use strict';

const {
  MESSAGE_TYPE_AUDIO,
  MESSAGE_TYPE_TEXT,
} = require('../../shared/const/message-types');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('message', 'type', {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: MESSAGE_TYPE_TEXT,
      validate: {
        notEmpty: true,
        isIn: {
          args: [
            [
              MESSAGE_TYPE_TEXT,
              MESSAGE_TYPE_AUDIO,
            ],
          ],
        },
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('message', 'type', {});
  },
};
