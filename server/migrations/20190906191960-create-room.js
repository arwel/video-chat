'use strict';
const {
  TYPE_CALL,
  TYPE_BROADCAST,
  TYPE_GROUP_CHAT,
  TYPE_ANONYMOUS_CHAT,
  TYPE_CHAT,
} = require('../../shared/const/room-types');
const {
  STATUS_ONLINE,
  STATUS_PENDING,
  STATUS_OFFLINE,
  STATUS_DELETED,
} = require('../../shared/const/status');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('room', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11),
      },
      title: {
        type: Sequelize.STRING,
      },
      slug: {
        type: Sequelize.STRING,
      },
      videoReturnUrl: {
        type: Sequelize.STRING,
        allowNull: true,
        field: 'video_return_url',
      },
      videoRecordSupport: {
        type: Sequelize.BOOLEAN,
        field: 'video_record_support',
        defaultValue: true,
      },
      recordsPath: {
        type: Sequelize.STRING,
        allowNull: true,
        field: 'records_path',
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: STATUS_OFFLINE,
        validate: {
          notEmpty: true,
          isIn: {
            args: [
              [
                STATUS_ONLINE,
                STATUS_OFFLINE,
                STATUS_PENDING,
                STATUS_DELETED,
              ],
            ],
          },
        },
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: TYPE_CALL,
        validate: {
          notEmpty: true,
          isIn: {
            args: [
              [
                TYPE_CALL,
                TYPE_BROADCAST,
                TYPE_GROUP_CHAT,
                TYPE_ANONYMOUS_CHAT,
                TYPE_CHAT,
              ],
            ],
          },
        },
      },
      UserId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'user',
          key: 'id',
        },
        field: 'user_id',
      },
      ServerId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: 'server',
          key: 'id',
        },
        field: 'server_id',
      },
      expiredAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'expired_at',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('room');
  },
};
