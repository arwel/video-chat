'use strict';

const {
  UR_STATUS_ACTIVE,
  UR_STATUS_MUTED,
} = require('../../shared/const/user-room-status');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('user_room', 'status', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: UR_STATUS_ACTIVE,
        validate: {
          notEmpty: true,
          isIn: {
            args: [
              [
                UR_STATUS_ACTIVE,
                UR_STATUS_MUTED,
              ],
            ],
          },
        },
      }),
      queryInterface.addColumn('user_room', 'startStatusAt', {
        type: Sequelize.DATE,
        allowNull: true,
      }),
      queryInterface.addColumn('user_room', 'stopStatusAt', {
        type: Sequelize.DATE,
        allowNull: true,
      }),
    ]);
  },

  down: (queryInterface) => {
    return Promise.all([
      queryInterface.removeColumn('user_room', 'status', {}),
      queryInterface.removeColumn('user_room', 'startStatusAt', {}),
      queryInterface.removeColumn('user_room', 'stopStatusAt', {}),
    ]);
  },
};
