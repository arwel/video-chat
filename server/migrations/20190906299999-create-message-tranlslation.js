'use strict';

const TABLE_NAME = 'message_translation';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(TABLE_NAME, {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      locale: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      MessageId: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        references: {
          model: 'message',
          key: 'id',
        },
        field: 'message_id',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable(TABLE_NAME);
  },
};
