const {resolve, join} = require('path');

const CWD = process.cwd();
const VAR_PATH = resolve(join(CWD, 'var'));

module.exports = {
  PATH_TO_SERVER_KEYS: join(VAR_PATH, 'keys'),
  PATH_TO_SERVER_LOGS: join(VAR_PATH, 'logs'),
  PATH_TO_SERVER_RECORDS: join(VAR_PATH, 'records'),
  PATH_TO_SERVER_UPLOADS: join(VAR_PATH, 'uploads'),
};
