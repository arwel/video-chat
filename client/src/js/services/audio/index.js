import notification from '../notification/index'
import {INCOMING_SONG, OUTGOING_SONG} from '../../config/audio'

const audioNotify = () => {
  notification.show({
    title: 'Audio error',
    body: 'Please give audio access'
  });
};

class AudioService {
  host;
  incomingUrl;
  outgoingUrl;
  isPlaying;

  constructor() {
    this.host = '';
    this.incomingUrl = '';
    this.outgoingUrl = '';
    this.isPlaying = false;
  }

  init = ({host, incomingUrl, outgoingUrl}) => {
    return new Promise((resolve) => {
      this.host = host;
      this.incomingUrl = incomingUrl && incomingUrl.length > 0 ? incomingUrl : INCOMING_SONG;
      this.outgoingUrl = outgoingUrl && outgoingUrl.length > 0 ? outgoingUrl : OUTGOING_SONG;

      resolve(this);
    });
  };

  play = (url) => {
    if (this.isPlaying) {
      return () => {};
    }

    this.isPlaying = true;
    const audio = new Audio();
    audio.src = url;
    audio.loop = true;

    const changePlayingStatus = () => {
      this.isPlaying = false;
    };

    try {
      audio.addEventListener('pause', changePlayingStatus);
      audio.addEventListener('cancel', changePlayingStatus);

      audio.play().catch(audioNotify);
    } catch (e) {
      audioNotify();
    }

    return audio.pause.bind(audio);
  };

}

export default new AudioService();
