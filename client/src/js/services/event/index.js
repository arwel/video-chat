class EventBus {
  subscribers;

  constructor() {
    this.subscribers = {};
  }

  subscribe = (type, callback) => {
    if (typeof callback !== 'function') {
      throw new Error(`Wrong type of callback to subscribe on event ${type}`);
    }

    let callbacks = this.subscribers[type];
    if (!callbacks) {
      callbacks = [];
      this.subscribers[type] = callbacks;
    }
    callbacks.push(callback);
  };

  dispatch = (type, data) => {
    const callbacks = this.subscribers[type];
    if (!callbacks) {
      return;
    }

    for (const callback of callbacks) {
      callback(data);
    }
  };
}

export default new EventBus();
