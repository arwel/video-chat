import Vue from 'vue';
import eventBus from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.eventBus && !$options.parent.$eventBus) {
      return;
    }

    this.$eventBus = $options.eventBus || $options.parent.$eventBus;
  },
});

export default eventBus;
