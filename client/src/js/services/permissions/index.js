import {PERMISSION_DENIED} from '@shared/const/permissions';
import DEVICE_PERM_MAP from '@shared/const/device-perm-relations';
import {PERM_NAME_CAMERA, PERM_NAME_MICROPHONE} from '@shared/const/permission-names';
import {getUserMedia} from '../../util/etc';

const enumerateDevices = window.navigator.mediaDevices.enumerateDevices.bind(window.navigator.mediaDevices) ||
  window.navigator.enumerateDevices.bind(window.navigator);

class PermissionsService {
  micPermission;
  camPermission;

  constructor() {
    this.camPermission = false;
    this.micPermission = false;
  }

  checkPermissionFallback = async (permission) => {
    try {
      let constraints = {
        video: false,
        audio: false,
      };

      switch (permission) {
        case PERM_NAME_CAMERA:
          constraints.video = true;
        break;
        case PERM_NAME_MICROPHONE:
          constraints.audio = true;
        break;
      }

      const stream = await getUserMedia(constraints);
      for (const track of stream.getTracks()) {
        track.stop();
      }

      return true;
    } catch (e) {
      return false;
    }
  };

  checkPermission = async (permission, onPermissionStatusChanged = null) => {
    try {
      const permissionStatus = await window.navigator.permissions.query({
        name: permission,
      });

      if (onPermissionStatusChanged) {
        permissionStatus.addEventListener('change', onPermissionStatusChanged);
      }

      return permissionStatus.state !== PERMISSION_DENIED;
    } catch (e) {
      return this.checkPermissionFallback(permission);
    }
  };

  enumerateDevice = async (deviceKind) => {
    const devices = await enumerateDevices();
    for (const {kind} of devices) {
      if (kind === deviceKind) {
        return true;
      }
    }

    return false;
  };

  canUse = async (device, onPermissionStatusChanged = null) => {
    const devicePresent = await this.enumerateDevice(device);

    if (devicePresent) {
      return await this.checkPermission(DEVICE_PERM_MAP[device], onPermissionStatusChanged);
    }

    return false;
  };
}

export default new PermissionsService();
