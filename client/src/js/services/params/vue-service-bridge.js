import Vue from 'vue';
import paramsService from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.params && !$options.parent.$params) {
      return;
    }

    this.$params = $options.params || $options.parent.$params;
  },
});

export default paramsService;
