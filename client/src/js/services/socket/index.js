import io from "socket.io-client";

class SocketService {
  _socket;
  socketInitHandlers;
  _serverToken;
  _host;
  _port;
  _p2pPort;
  _isInitialized;

  constructor() {
    this.socketInitHandlers = [];
    this._host = '';
    this._serverToken = '';
    this._port = 8080;
    this._p2pPort = 8081;
    this._isInitialized = false;
  }

  isConfigured() {
    return !!this._socket;
  }

  get socket() {
    if (!this._socket) {
      throw new Error('Socket not configured');
    }

    return this._socket;
  }

  get credentials() {
    return {
      token: this._serverToken,
      host: this._host,
      port: this._port,
      p2pPort: this._p2pPort,
    };
  }

  addInitHandler = (handler) => {
    if (this._isInitialized) {
      handler(this);
    }

    this.socketInitHandlers.push(handler);
  };

  init({host, port, serverToken, token, p2pPort}) {
    this._serverToken = serverToken;
    this._host = host;
    this._port = port;
    this._p2pPort = p2pPort;

    return new Promise((resolve) => {
      this._socket = io(`${host}:${port}`, {
        query: `serverToken=${serverToken}&token=${token}`,
      });
      this._socket.on('socketOpened', () => {
        for (const handler of this.socketInitHandlers) {
          handler(this);
        }

        this.socketInitHandlers = [];
        this._isInitialized = true;

        resolve(this);
      });
    });
  }

  dispatch(route, data) {
    this.socket.emit('action', {
      route,
      data
    });
  }
}

export default new SocketService();
