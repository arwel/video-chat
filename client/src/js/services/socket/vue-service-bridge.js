import Vue from 'vue';
import socketService from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.socket && !$options.parent.$socket) {
      return;
    }

    this.$socket = $options.socket || $options.parent.$socket;
  },
});

export default socketService;
