import {STATE_INACTIVE, STATE_RECORDING} from '@js/const/state';
import RECORD_CONFIG from '@js/config/record';
import mediaService from '@js/services/media/index';

const DummyRecorder = {
  start() {},
  stop() {},
  addEventListener() {},
  state: '',
};

class Recorder {
  mediaRecorder;
  handlers;

  async getRecorder(config = {}) {
    if (!this.mediaRecorder) {
      this.mediaRecorder = !window.MediaRecorder ? DummyRecorder : new MediaRecorder(await mediaService.getStream(config), {
        audioBitsPerSecond: RECORD_CONFIG.audioBitsPerSecond,
        videoBitsPerSecond: RECORD_CONFIG.videoBitsPerSecond,
        mimeType: config.mimeType ? config.mimeType : RECORD_CONFIG.mimeType,
      });
      this.mediaRecorder.addEventListener('dataavailable', this.dataAvailableHandler);
    }

    return this.mediaRecorder;
  }

  get state() {
    return this.mediaRecorder ? `${this.mediaRecorder.state}` : STATE_INACTIVE;
  }

  constructor() {
    this.mediaRecorder = null;
    this.handlers = new Map();
  }

  start = async (config) => {
    const recorder = await this.getRecorder(config);
    if (recorder.state !== STATE_RECORDING) {
      recorder.start(RECORD_CONFIG.timeout);
    }
  };

  stop = async () => {
    const recorder = await this.getRecorder();
    if (recorder.state === STATE_RECORDING) {
      recorder.stop();
      mediaService.release();
      this.mediaRecorder = null;
    }
  };

  addHandler = (key, handler) => {
    this.handlers.set(key, handler);
  };

  removeHandler = (key) => {
    this.handlers.delete(key);
  };

  dataAvailableHandler = (event) => {
    this.handlers.forEach((handler) => {
      handler(event);
    });
  };
}

export default new Recorder();
