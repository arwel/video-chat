import localeEn from '../../locales/en';
import localeUk from '../../locales/uk';
import localeRu from '../../locales/ru';

class TranslationService {
  translations;

  constructor() {
    this.translations = {};
  }

  init = (locale) => {
    return new Promise((resolve) => {
      switch (locale) {
        case 'en':
          this.translations = localeEn;
          break;
        case 'uk':
          this.translations = localeUk;
          break;
        case 'ru':
          this.translations = localeRu;
          break;
        default:
          this.translations = localeEn;
          break;
      }

      resolve(this);
    });
  };

  get = (key, _default = '') => {
    return this.translations[key] ? this.translations[key] : _default;
  }
}

export default new TranslationService();
