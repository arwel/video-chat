import Vue from 'vue';
import translationService from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.t && !$options.parent.$t) {
      return;
    }

    this.$t = $options.t || $options.parent.$t;
  },
});

export default translationService.get.bind(translationService);
