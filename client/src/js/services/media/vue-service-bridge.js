import Vue from 'vue';
import mediaService from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.userMedia && !$options.parent.$userMedia) {
      return;
    }

    this.$userMedia = $options.userMedia || $options.parent.$userMedia;
  },
});

export default mediaService;
