import notificationService from '../notification/index';
import permissionService from '../permissions/index';
import USER_MEDIA_CONSTRAINTS from '../../config/stream';
import {DEVICE_AUDIO_INPUT, DEVICE_VIDEO_INPUT} from '@shared/const/device-kinds'
let {audio, video} = USER_MEDIA_CONSTRAINTS;

import {getUserMedia} from '../../util/etc';

class MediaService {
  stream;
  pointersCount;

  constructor() {
    this.pointersCount = 0;
    this.pointersCount = 0;
  }

  getStream = async ({captureVideo = true, captureAudio = true} = {}) => {
    if (!this.stream) {
      const checkPermsRequests = [
        captureVideo ? permissionService.canUse(DEVICE_VIDEO_INPUT) : Promise.resolve(false),
        captureAudio ? permissionService.canUse(DEVICE_AUDIO_INPUT) : Promise.resolve(false),
      ];


      const [canCaptureVideo, canCaptureAudio] = await Promise.all(checkPermsRequests);

      const videoConstraint = canCaptureVideo ? video : false;
      const audioConstraint = canCaptureAudio ? audio : false;

      if (!videoConstraint && !audioConstraint) {
        notificationService.show({
          title: 'Error capturing user media',
          body: 'You have disabled mic and camera',
        });

        return null;
      }

      let stream = await getUserMedia({
        video: videoConstraint,
        audio: audioConstraint,
      });

      // double check, because async shared state
      if (this.stream) {
        this.closeStream(stream);
      } else {
        this.stream = stream;
      }
    }

    this.pointersCount++;

    return this.stream;
  };

  setTracksEnabled = (tracks, enabled) => {
    for (const track of tracks) {
      track.enabled = enabled;
    }
  };

  setEnabledCam = (enabled) => {
    this.stream && this.setTracksEnabled(this.stream.getVideoTracks(), enabled);
  };

  setEnabledMic = (enabled) => {
    this.stream && this.setTracksEnabled(this.stream.getAudioTracks(), enabled);
  };

  isEnabledTrack = (tracks) => {
    for (const track of tracks) {
      if (!track.enabled) {
        return false;
      }
    }

    return true;
  };

  isEnabledCam = () => {
    return !!(this.stream && this.isEnabledTrack(this.stream.getVideoTracks()));
  };

  isEnabledMic = () => {
    return !!(this.stream && this.isEnabledTrack(this.stream.getAudioTracks()));
  };

  release = () => {
    this.pointersCount > 0 && this.pointersCount--;

    if (this.pointersCount < 1 && this.stream && this.stream.active) {
      this.closeForce();
    }
  };

  closeForce = () => {
    this.stream && this.closeStream(this.stream);
    this.stream = null;
  };

  closeStream = (stream) => {
    for (const track of stream.getTracks()) {
      track.stop();
    }
  };
}

export default new MediaService();
