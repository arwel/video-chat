const CLASS_NAME = 'chater-notification';
const TIMEOUT = 8000;

class HtmlNotificationAdapter {
  _notificationWrapper;

  get wrapper() {
    const className = `${CLASS_NAME}--wrapper`;

    if (!this._notificationWrapper) {
      this._notificationWrapper = document.querySelector(`.${className}`);
    }

    if (!this._notificationWrapper) {
      this._notificationWrapper = this.createWrapper();

      requestAnimationFrame(() => {
        document.body.appendChild(this._notificationWrapper);
      });
    }

    return this._notificationWrapper;
  }

  createWrapper = () => {
    const wrapper = document.createElement('div');
    wrapper.className = `${CLASS_NAME}--wrapper`;

    return wrapper;
  };

  add = ({icon, title, body, type} = {icon: '', title: '', body: '', type: ''}) => {
    const notification = document.createElement('div');
    notification.className = `${CLASS_NAME} ${type}`;

    if (icon) {
      const iconElem = document.createElement('div');
      iconElem.className = `${CLASS_NAME}--icon`;
      iconElem.innerHTML = icon;
      notification.appendChild(iconElem);
      notification.classList.add('has-icon');
    }

    if (title) {
      const titleElem = document.createElement('b');
      titleElem.className = `${CLASS_NAME}--title`;
      titleElem.innerHTML = title;
      notification.appendChild(titleElem);
    }

    if (body) {
      const bodyElem = document.createElement('div');
      bodyElem.className = `${CLASS_NAME}--body`;
      bodyElem.innerHTML = body;
      notification.appendChild(bodyElem);
    }

    requestAnimationFrame(() => {
      setTimeout(this.remove(notification), TIMEOUT);
      notification.addEventListener('click', this.remove(notification));
      this.wrapper.appendChild(notification);
    });
  };

  remove = (notification) => {
    return () => {
      if (notification.parentNode === this.wrapper) {
        requestAnimationFrame(() => {
          this.wrapper.removeChild(notification);
        })
      }
    };
  };
}

export default new HtmlNotificationAdapter();
