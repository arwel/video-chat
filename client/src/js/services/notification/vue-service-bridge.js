import Vue from 'vue';
import notificationService from './index';

Vue.mixin({
  beforeCreate() {
    const {$options} = this;

    if (!$options.notify && !$options.parent.$notify) {
      return;
    }

    this.$notify = $options.notify || $options.parent.$notify;
  },
});

export default notificationService;
