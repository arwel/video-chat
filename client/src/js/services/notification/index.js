import {
  PERMISSION_GRANTED,
  PERMISSION_DENIED,
  PERMISSION_DEFAULT
} from '../../../../../shared/const/permissions';
import htmlNotificationAdapter from './html-notification-adapter';

class NotificationService {
  _isInit;
  notificationAddedHandlers;

  get isInit () {
    return this._isInit;
  }

  constructor() {
    this._isInit = false;
    this.notificationAddedHandlers = [];
  }

  addNotificationAddedHandler = (handler) => {
    this.notificationAddedHandlers.push(handler);
  };

  init = () => {
    return new Promise(async (resolve) => {
      if (Notification.permission === PERMISSION_DEFAULT) {
        await this.initManager();
      }

      this._isInit = Notification.permission === PERMISSION_GRANTED;

      resolve(this);
    });
  };

  permissionUpdateHandler = async (status) => {
    if (status === PERMISSION_GRANTED && !this._isInit) {
      await this.initManager();
      this._isInit = true;
    } else if (status === PERMISSION_DENIED && this._isInit) {
      this._isInit = false;
    }
  };

  initManager = async () => {
    await Notification.requestPermission(this.permissionUpdateHandler);
  };

  showHtmlNotification = (data) => {
    for (const handler of this.notificationAddedHandlers) {
      handler(data, this);
    }

    htmlNotificationAdapter.add(data);
  };

  showSuccess = (data) => {
    data.type = 'success';
    data.icon = '<i class="material-icons-outlined">notifications</i>';

    this.showHtmlNotification(data);
  };

  showDanger = (data) => {
    data.type = 'danger';
    data.icon = '<i class="material-icons-outlined">notifications</i>';

    this.showHtmlNotification(data);
  };

  show = (data) => {
    if (!this._isInit) {
      console.log('Fallback to html notification');
      this.showHtmlNotification(data);

      return null;
    }

    try {
      const notification = new Notification(data.title, {
        body: data.body || '',
        icon: data.icon || 'favicon.ico',
      });

      notification.addEventListener('click', this.notificationClickHandler);

      return notification;
    } catch (e) {
      this._isInit = false;

      return this.show(data);
    }
  };

  notificationClickHandler = (event) => {
    window.focus();

    event.target.close();
  };
}

export default new NotificationService();
