export const OPERA = 'opera';
export const FIREFOX = 'firefox';
export const SAFARI = 'safari';
export const IE = 'ie';
export const EDGE = 'edge';
export const CHROME = 'chrome';
export const BLINK = 'blink';
export const GENERIC = 'generic';
