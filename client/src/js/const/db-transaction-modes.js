export const DB_TRANSACTION_MODE_READ = 'read';
export const DB_TRANSACTION_MODE_WRITE = 'write';
export const DB_TRANSACTION_MODE_READ_WRITE = 'readwrite';
