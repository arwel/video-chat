export const STATE_INACTIVE = 'inactive';
export const STATE_RECORDING = 'recording';
export const STATE_PAUSED = 'paused';
