export const readFile = (file) => {
  const reader = new FileReader();

  return new Promise((resolve) => {
    reader.addEventListener('load', ({target: {result}}) => {
      resolve({
        name: file.name,
        content: result,
      });
    });

    reader.readAsArrayBuffer(file);
  });
};

export const bulkReadFiles = (files) => {
  return Promise.all(files.map(readFile));
};
