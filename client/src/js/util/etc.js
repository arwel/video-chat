import {
  BLINK,
  CHROME,
  EDGE,
  FIREFOX,
  GENERIC,
  IE,
  OPERA,
  SAFARI,
} from '@js/const/browsers';

export const isSubObject = (subObj, obj) => {
  for (const key in subObj) {
    if (!subObj.hasOwnProperty(key)) {
      continue;
    }

    if (obj[key] === undefined) {
      return false;
    }
  }

  return true;
};

export const filterObject = (obj, callback) => {
  if (typeof callback !== 'function') {
    return {};
  }

  const filtered = {};
  for (const key in obj) {
    if (!obj.hasOwnProperty(key) || !callback(obj[key])) {
      continue;
    }

    filtered[key] = obj[key];
  }

  return filtered;
};

export const getObjectRem = (obj, keys) => {
  for (const key of keys) {
    delete obj[key];
  }

  return obj;
};

export const objectLength = (obj) => {
  return Object.keys(obj).length;
};

export const getSubObject = (obj, keys) => {
  const subObject = {};

  for (const key of keys) {
    if (obj[key] !== undefined) {
      subObject[key] = obj[key];
    }
  }

  return subObject;
};

export const get = (obj, path, _default = null) => {
  try {
    return path.split('.').reduce((newObj, key) => {
      return newObj[key];
    }, obj);
  } catch (e) {
    return _default;
  }
};

export const getUserMedia = window.navigator.mediaDevices.getUserMedia.bind(
    window.navigator.mediaDevices) ||
  window.navigator.getUserMedia.bind(window.navigator.getUserMedia) ||

  window.navigator.webkitGetUserMedia.bind(window.navigator) ||

  window.navigator.mozGetUserMedia.bind(window.navigator) ||

  window.navigator.msGetUserMedia.bind(window.navigator);

export const getBrowser = () => {
  const isIE = !!document.documentMode;
  const isChrome = !!window.chrome &&
    (!!window.chrome.webstore || !!window.chrome.runtime);
  const isOpera = (!!window.opr && !!opr.addons) || !!window.opera ||
    navigator.userAgent.indexOf(' OPR/') >= 0;

  if (isOpera) {
    return OPERA;
  } else if (typeof InstallTrigger !== 'undefined') {
    return FIREFOX;
  } else if (/constructor/i.test(window.HTMLElement) || (function(p) {
    return p.toString() === '[object SafariRemoteNotification]';
  })(!window['safari'] ||
    (typeof safari !== 'undefined' && safari.pushNotification))) {
    return SAFARI;
  } else if (isIE) {
    return IE;
  } else if (!isIE && !!window.StyleMedia) {
    return EDGE;
  } else if (isChrome) {
    return CHROME;
  } else if ((isChrome || isOpera) && !!window.CSS) {
    return BLINK;
  } else {
    return GENERIC;
  }
};

export const getCssValue = (elem, property) => {
  return document.defaultView.getComputedStyle(elem, null).
      getPropertyValue(property);
};
