export const emit = (socket, route, data) => {
  socket.emit('action', {
    route,
    data,
  });
};
