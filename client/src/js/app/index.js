import notificationService from '@js/services/notification';
import socketService from '@js/services/socket';
import audioService from '@js/services/audio';
import paramsService from '@js/services/params';
import translationService from '@js/services/translation';
import {get} from '@js/util/etc';
import {KEY_LAST_UPDATE_TOKEN} from '@js/const/local-storage-keys';
import {JWT_LIFETIME} from '@shared/const/threasholds';
import initVideoChatPanel from '@vue/components/panels/video-chat-panel';
import initReadMessagesPanel from '@vue/components/panels/read-messages-panel';
import initChatPanel from '@vue/components/panels/chat-panel';
import initBroadcastPanel from '@vue/components/panels/broadcast-panel';
import initDialogListPanel from '@vue/components/panels/dialog-list-panel';
import initNewMessagesWidget from '@vue/components/widgets/new-messages/bridge';

export default class App {
  userId;
  roomSlug;
  chatSlug;
  serverToken;
  locale;
  loadedStyles;

  constructor() {
    this.userId = 0;
    this.roomSlug = '';
    this.chatSlug = '';
    this.serverToken = '';
    this.locale = '';
    this.loadedStyles = {};
  }

  init = async (config) => {
    this.userId = config.userId;
    this.serverToken = config.serverToken;
    this.locale = config.locale;

    const incomingUrl = get(config, 'static.audio.incoming.url');
    const outgoingUrl = get(config, 'static.audio.outgoing.url');

    await Promise.all([
      translationService.init(config.locale),
      audioService.init({
        host: config.host,
        incomingUrl,
        outgoingUrl,
      }),
      socketService.init({
        host: config.host,
        port: config.port,
        p2pPort: config.p2pPort || config.port,
        serverToken: config.serverToken,
        token: config.jwt,
      }),
      notificationService
      .init(),
    ]).catch(console.log);

    socketService.socket.on('updateUi', this.socketUpdateUiHandler);

    window.addEventListener('beforeunload', this.windowBeforeUnloadHandler);

    window.dispatchEvent(new CustomEvent('dreamext.chater.init', {
      detail: {
        chater: this,
      },
    }));

    return this;
  };

  setParam = (key, value) => {
    paramsService.set(key, value);

    return this;
  };

  windowBeforeUnloadHandler = () => {
    socketService.dispatch('room/leave');
  };

  socketUpdateUiHandler = ({type, payload}) => {
    window.dispatchEvent(new CustomEvent(`dreamext.chater.socket.${type}`, {
      detail: payload,
    }));
  };

  renderNewMessagesWidget = (elem) => {
    initNewMessagesWidget(elem, {
      roomSlug: this.roomSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  renderVideoChatPanel = (elem) => {
    initVideoChatPanel(elem, {
      roomSlug: this.roomSlug,
      chatSlug: this.chatSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  renderBroadcastPanel = (elem) => {
    initBroadcastPanel(elem, {
      roomSlug: this.roomSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  renderChatPanel = (elem) => {
    initChatPanel(elem, {
      roomSlug: this.roomSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  renderReadMessagesPanel = (elem) => {
    initReadMessagesPanel(elem, {
      roomSlug: this.roomSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  renderDialogListPanel = (elem) => {
    initDialogListPanel(elem, {
      roomSlug: this.roomSlug,
      locale: this.locale,
      serverToken: this.serverToken,
    });
  };

  updateToken = () => {
    socketService.socket.emit('updateToken', {
      id: this.userId,
      serverToken: this.serverToken,
    });

    return this;
  };

  entryToRoom = ({slug, chatSlug}) => {
    this.roomSlug = slug;
    this.chatSlug = chatSlug;

    slug && socketService.dispatch('room/entry', {
      slug,
    });
    chatSlug && socketService.dispatch('dialog/setActiveChatBySlug', {
      slug: chatSlug,
    });

    return this;
  };

  setTheme = (theme) => {
    const root = document.documentElement;

    for (const prop in theme) {
      if (!theme.hasOwnProperty(prop)) {
        continue;
      }

      root.style.setProperty(`--${prop}`, theme[prop]);
    }

    return this;
  };

  setCover = (url) => {
    document.documentElement.style.setProperty('--home-image', url);

    return this;
  };

  loadStyles = (url) => {
    if (!this.loadedStyles[url]) {
      this.loadedStyles[url] = 1;

      const link = document.createElement('link');
      link.rel = 'stylesheet';
      link.href = url;

      requestAnimationFrame(() => {
        document.head.appendChild(link);
      });
    }

    return this;
  };

  lazyUpdateToken = () => {
    const now = Math.round(Date.now() / 1000);
    const lastUpdatedTimestamp = localStorage.getItem(KEY_LAST_UPDATE_TOKEN) || 0;
    if (now - lastUpdatedTimestamp <= JWT_LIFETIME) {
      return this;
    }

    this.updateToken();
    localStorage.setItem(KEY_LAST_UPDATE_TOKEN, `${now}`);
  };
}
