export default {
  timeout: 1000,
  audioBitsPerSecond: 64000,
  // audioBitsPerSecond: 32000,
  videoBitsPerSecond: 128000,
  // videoBitsPerSecond: 64000,
  mimeType: 'video/webm; codecs=vp8',
  audioMimeType: 'audio/webm;codecs=opus',
};
