import {getBrowser} from '@js/util/etc';
import {BLINK, FIREFOX} from '@js/const/browsers';

const getConfig = () => {
  const AUDIO = true;
  const MIN_WIDTH = 320;
  const MIN_HEIGHT = 240;
  const IDEAL_WIDTH = 640;
  const IDEAL_HEIGHT = 480;
  const MAX_WIDTH = 640;
  const MAX_HEIGHT = 480;
  const IDEAL_FRAME_RATE = 24;
  const MAX_FRAME_RATE = 25;
  const RATIO = 1.3333333333333333;
  const FACING_MODE = 'face';

  switch (getBrowser()) {
    case FIREFOX:
      return {
        audio: AUDIO,
        video: {
          width: {
            min: MIN_WIDTH,
            ideal: IDEAL_WIDTH,
            max: MAX_WIDTH,
          },
          height: {
            min: MIN_HEIGHT,
            ideal: IDEAL_HEIGHT,
            max: MAX_HEIGHT,
          },
          frameRate: {
            ideal: IDEAL_FRAME_RATE,
          },
        },
      };
    case BLINK:
      return {
        audio: AUDIO,
        video: {
          width: {
            min: MIN_WIDTH,
            ideal: IDEAL_WIDTH,
            max: MAX_WIDTH,
          },
          height: {
            min: MIN_HEIGHT,
            ideal: IDEAL_HEIGHT,
            max: MAX_HEIGHT,
          },
          aspectRatio: RATIO,
          frameRate: {
            ideal: IDEAL_FRAME_RATE,
            max: MAX_FRAME_RATE,
          },
          facingMode: {
            exact: FACING_MODE,
          },
        },
      };
    default:
      return {
        audio: AUDIO,
        video: true,
      };
  }
};

export default getConfig();
