export default {
  'ru': 'Russian',
  'uk': 'Ukrainian',
  'en': 'English',
  'pl': 'Polish',
  'es': 'Spanish',
  'fr': 'French',
  'de': 'German',
  'it': 'Italian',
  'cs': 'Czech',
  'be': 'Belarusian',
};
