import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';
import mediaService from '@js/services/media';
import {STATUS_OFFLINE, STATUS_ONLINE} from '@js/const/status';

export default (socketService, context) => {
  const {getters, commit, dispatch} = context;
  const {socket} = socketService;

  context.watch(
      () => context.getters['calls/activeCalls'],
      (calls, oldCalls) => {
        if (calls.length < 1 && oldCalls.length > 0) {
          mediaService.closeForce();
        }

        const newStatus = calls.length > 0 ? STATUS_ONLINE : STATUS_OFFLINE;
        dispatch('calls/setStatus', {status: newStatus});

        const {id} = context.getters['app/masterFrame'] || {};
        if (!id) {
          return;
        }

        const index = calls.findIndex((call) => {
          return call.id === id;
        });

        if (calls.length < 1) {
          dispatch('app/setMasterFrame', null);
        } else if (index < 0) {
          const firstCall = context.getters['roomUsersList/usersObject'][calls[0]];
          const streams = context.getters['rtc/streams'];

          dispatch('app/setMasterFrame', {
            id: firstCall.id,
            nick: firstCall.nick,
            stream: streams[firstCall.id],
            canMute: true,
          });
        }
      },
  );

  socket.on('incomingCall', ({id, isForce}) => {
    const callStatus = getters['calls/status'];

    if (callStatus === STATUS_ONLINE) {
      dispatch('calls/moveIncomingToActive', id);

      return;
    }

    const {nick} = getters['roomUsersList/usersObject'][id] || {};

    notificationService.show({
      title: translationService.get('incomingCall'),
      body: `${translationService.get('youHaveIncomingCall')}${nick}`,
    });

    if (isForce) {
      commit('calls/addForceCall', id);
    }

    commit('calls/addIncomingCall', id);
  });

  socket.on('decline', ({id}) => {
    dispatch('calls/removeIncomingCall', id);
    dispatch('calls/removeOutgoingCall', id);
  });

  socket.on('accept', ({id}) => {
    dispatch('calls/addActiveCall', id);
    dispatch('calls/removeIncomingCall', id);
    dispatch('calls/removeOutgoingCall', id);
    dispatch('rtc/createOffer', {id, withMedia: true});
  });

  socket.on('callEnd', ({id}) => {
    dispatch('calls/removeOutgoingCall', id);
    dispatch('calls/removeIncomingCall', id);
    commit('calls/removeActiveCall', id);

    dispatch('rtc/closeStream', id);
    dispatch('rtc/closeConnection', id);
  });

  socket.on('onlineUsersForCall', ({nick}) => {
    notificationService.show({
      title: translationService.get('inviteToCall'),
      body: `${translationService.get('user')} ${nick} ${translationService.get('waitingForYou')} ${translationService.get('goToServicePageForCall')}`,
    });
  });
};
