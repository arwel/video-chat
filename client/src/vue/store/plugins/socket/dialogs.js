import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';
import paramService from '@js/services/params';
import {fixEntityDate} from '@shared/utils/normalize';

export default (socketService, {dispatch, commit, getters}) => {
  const activeDefaultDialog = (dialogs) => {
    const defaultDialogSlug = `${paramService.get('panels.dialogs.defaultDialogSlug')}`.trim();
    if (defaultDialogSlug.length < 1) {
      return;
    }

    for (const dialog of dialogs) {
      if (dialog.slug === defaultDialogSlug) {
        dispatch('dialogs/setActiveChat', dialog);
        dispatch('app/setShowMobileChat', true);
        paramService.set('panels.dialogs.defaultDialogSlug', '');

        return;
      }
    }
  };

  socketService.socket.on('dialogsSetDialogList', ({dialogs}) => {
    dispatch('dialogs/setLastLoadedCount', dialogs.length);
    dispatch('dialogs/addDialogs', dialogs.map((room) => fixEntityDate(room)));
    dispatch('dialogs/setLoading', false);

    activeDefaultDialog(dialogs);
  });

  socketService.socket.on('dialogsRemove', ({id}) => {
    const activeChat = getters['dialogs/activeChat'];
    if (activeChat.id === id) {
      dispatch('dialogs/setActiveChat', null);
    }

    commit('dialogs/remove', {id});
  });

  socketService.socket.on('dialogsAddNew', ({chat}) => {
    const dialogs = getters['dialogs/dialogs'];
    if (dialogs.find(({id}) => id === chat.id)) {
      return;
    }

    chat = fixEntityDate(chat);
    dispatch('dialogs/addDialogs', [chat]);
    activeDefaultDialog([chat]);

    notificationService.show({
      title: translationService.get('newInvite'),
      body: translationService.get('invitedToRoom') + ` "${chat.title}"`,
    });
  });

  socketService.socket.on('dialogsRemoteSetActiveChat', ({dialog}) => {
    dispatch('dialogs/addDialogs', [dialog]);
    dispatch('dialogs/setActiveChat', dialog);
  });

  socketService.socket.on('dialogsSetUsersToInvite', ({users}) => {
    dispatch('dialogs/setUsersToInvite', users);
  });

  socketService.socket.on('dialogsSetActiveDialogMembers', ({members}) => {
    dispatch('dialogs/setActiveDialogMembers', members);
  });
};
