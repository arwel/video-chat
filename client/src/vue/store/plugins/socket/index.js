import roomUsers from './room-users';
import calls from './calls';
import rtc from './rtc';
import broadcast from './broadcast';
import user from './user';
import permissions from './permissions';
import dialogs from './dialogs';
import chats from './chats';
import files from './files';
import widgetNewMessages from './widgets/new-messages';

const runSocketSupport = (socket, store) => {
  chats(socket, store);
  roomUsers(socket, store);
  calls(socket, store);
  rtc(socket, store);
  broadcast(socket, store);
  user(socket, store);
  permissions(socket, store);
  dialogs(socket, store);
  files(socket, store);
  widgetNewMessages(socket, store);
};

export default (socketService) => {
  return (store) => {
    socketService.addInitHandler((service) => {
      runSocketSupport(service, store);
    });
  };
};
