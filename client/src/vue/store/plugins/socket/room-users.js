import {STATUS_OFFLINE} from '@js/const/status';

export default (socketService, {commit, dispatch}) => {
  socketService.socket.on('roomUsersList', (usersList) => {
    const usersObj = {};
    for (const user of usersList) {
      usersObj[user.id] = user;
    }

    commit('roomUsersList/setUsers', usersObj);
  });

  socketService.socket.on('updateRoomUser', (user) => {
    if (user.status === STATUS_OFFLINE) {
      dispatch('calls/removeActiveCall', user.id);
      dispatch('calls/removeIncomingCall', user.id);
      dispatch('calls/removeForceCall', user.id);
      dispatch('calls/removeOutgoingCall', user.id);
    }

    commit('roomUsersList/updateUser', user);
  });
};
