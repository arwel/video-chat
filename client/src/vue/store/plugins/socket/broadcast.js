export default (socketService, {commit}) => {
  const on = socketService.socket.on.bind(socketService.socket);

  on('broadcastSetIsBroadcaster', (isBroadcaster) => {
    commit('broadcast/setIsBroadcaster', isBroadcaster);
  });

  on('broadcastSetBroadcaster', ({id, nick, status, avatar}) => {
    commit('broadcast/setBroadcaster', {
      id,
      nick,
      status,
      avatar,
    });
  });

  on('broadcastSetStatus', (status) => {
    commit('broadcast/setStatus', status);
  });
};
