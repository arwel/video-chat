import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';

const showDisabledMediaNotification = () => {
  notificationService.show({
    title: translationService.get('userMediaError'),
    body: translationService.get('userMediaAttention'),
  });
};

export default (socketService, context) => {
  socketService.socket.on('error', () => {
    socketService.socket.disconnect();
    socketService.socket.close();
    context.dispatch('permissions/setOnline', false);
  });

  socketService.socket.on('disconnect', () => {
    // @TODO: let`s try
    // if (context.getters['permissions/reconnects'] > 0 && context.getters['permissions/tryReconnect']) {
    if (context.getters['permissions/tryReconnect']) {
      socketService.socket.connect();
      console.log('reconnect');
      context.dispatch('permissions/decreaseReconnects');
    } else {
      context.dispatch('permissions/setOnline', !socketService.socket.disconnected);
      context.dispatch('rtc/disconnect');
    }
  });

  context.watch(
      () => context.getters['permissions/hasCamera'],
      (newPerm) => {
        const hasAudio = context.getters['permissions/hasMic'];

        if (!newPerm && !hasAudio) {
          showDisabledMediaNotification();
        }
      },
  );

  context.watch(
      () => context.getters['permissions/hasMic'],
      (newPerm) => {
        const hasVideo = context.getters['permissions/hasCamera'];

        if (!newPerm && !hasVideo) {
          showDisabledMediaNotification();
        }
      },
  );
};
