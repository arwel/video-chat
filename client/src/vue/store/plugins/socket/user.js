import translationService from '@js/services/translation';
import notificationService from '@js/services/notification';

export default (socketService, context) => {
  const {commit, dispatch} = context;

  socketService.dispatch('user/getCurrent', {});

  socketService.socket.on('authFromAnotherDevice', () => {
    dispatch('permissions/setTryReconnect', false).then(() => {
      socketService.socket.close();
    });
  });

  socketService.socket.on('userUpdateCurrent', (user) => {
    commit('user/updateCurrentUser', user);
  });

  socketService.socket.on('errorMessage', ({title, body}) => {
    notificationService.showDanger({
      title: translationService.get(title, title),
      body: translationService.get(body, body),
    });
  });

  socketService.socket.on('userSetRoomRelations', ({relations}) => {
    dispatch('user/setRooms', relations);
  });

  context.watch(
      () => context.getters['user/currentUser'],
      (newUser, oldUser) => {
        if (!newUser) {
          dispatch('user/setRooms', {});

          return;
        }

        if (newUser.id && newUser.id !== oldUser.id) {
          dispatch('user/loadCurrentUserRoomRelations', null);
        }
      },
  );
};
