export default ({socket}, context) => {
  const {dispatch, getters} = context;

  // set master frame
  context.watch(
      () => context.getters['rtc/streams'],
      (streams) => {
        const masterFrame = getters['app/masterFrame'];
        const streamsKeys = Object.keys(streams);
        if (!masterFrame && streamsKeys.length > 0) {
          const firstStreamKey = streamsKeys[0];
          const firstStream = streams[firstStreamKey];
          const {id, nick} = getters['roomUsersList/usersObject'][firstStreamKey] || {};

          dispatch('app/setMasterFrame', {
            id,
            nick,
            stream: firstStream,
          });
        }
      }
  );

  // make call
  socket.on('rtcRequestOffer', ({slaveId}) => {
    dispatch('rtc/createOffer', {
      id: slaveId,
      withMedia: true,
      withSlaveMedia: false,
    });
  });
};
