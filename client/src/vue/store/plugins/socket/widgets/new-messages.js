import {normalizeChatMessage} from '@shared/utils/normalize';

export default (socketService, {commit}) => {
  socketService.socket.on('newMessagesSetData', ({count, messages}) => {
    commit('widgets/newMessages/setCount', +count);
    commit('widgets/newMessages/setMessages', messages.map(normalizeChatMessage));
  });
};
