export default (socketService, {commit, getters}) => {
  socketService.socket.on('filesDownloadContent', ({id, content, mimeType}) => {
    if (!getters['files/loading'][id]) {
      return;
    }

    commit('files/removeLoading', {id});
    commit('files/setLoaded', {
      id,
      content,
      mimeType,
    });
  });
};
