import notificationService from '@js/services/notification';

export default (socketService, {dispatch}) => {
  if (!notificationService.isInit) {
    notificationService.addNotificationAddedHandler((notify) => {
      dispatch('notifications/add', notify);
    });
  }
};
