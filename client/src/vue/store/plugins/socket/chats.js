import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';
import {normalizeChatMessage} from '@shared/utils/normalize';
import {STATE_ACTIVE} from '@shared/const/state';
import {ACTIVITY_FILES_UPLOADING} from '@shared/const/activities-types';
import {STATE_INACTIVE} from '@shared/const/state';
import {UR_STATUS_ACTIVE} from '@shared/const/user-room-status';

export default (socketService, context) => {
  const {getters, commit, dispatch} = context;

  socketService.socket.on('chatsNewMessage', (msg) => {
    const {id} = getters['user/currentUser'] || {};
    const {status} = getters['user/room'](msg.roomId);
    if (id !== msg.user.id && status === UR_STATUS_ACTIVE) {
      notificationService.show({
        title: `${translationService.get('newMessageFrom')} ${msg.user.nick}`,
        body: msg.message,
      });

      dispatch('chats/setSeen', {
        ids: [msg.id],
      });
    } else {
      dispatch('chats/setUploading', false);
      dispatch('chats/sendMyActivity', {
        roomId: getters['dialogs/activeChat'].id,
        state: STATE_INACTIVE,
        type: ACTIVITY_FILES_UPLOADING,
      });
    }

    dispatch('chats/addMessage', msg);
    dispatch('chats/setNeedScroll', true);
  });

  socketService.socket.on('chatsAddMessages', ({messages}) => {
    dispatch('chats/setLastLoadedCount', messages.length);

    const {id} = getters['dialogs/activeChat'];
    const message = messages[0];
    if (message && message.roomId !== id) {
      return;
    }

    const normalizedMessages = messages.map(normalizeChatMessage);
    const oldMessages = getters['chats/messages'];

    commit('chats/setMessages', [
      ...normalizedMessages,
      ...oldMessages,
    ]);
    dispatch('chats/setLoading', false);

    if (oldMessages.length === 0) {
      dispatch('chats/setNeedScroll', true);
    }

    const currentUser = getters['user/currentUser'];
    const notSeenMsgsIds = messages.filter((message) => {
      return !message.seen && message.user.id !== currentUser.id;
    }).map((message) => {
      return message.id;
    });

    if (notSeenMsgsIds.length < 1) {
      return;
    }

    dispatch('chats/setSeen', {
      ids: notSeenMsgsIds,
    });
  });

  socketService.socket.on('chatsUpdateMessage', ({message}) => {
    commit('chats/updateMessage', message);

    const locale = getters['chats/translateActive'];
    socketService.dispatch('chat/translateMessages', {
      ids: [message.id],
      locale,
    });
  });

  socketService.socket.on('chatsDeleteMessage', ({id}) => {
    commit('chats/deleteMessage', id);
    dispatch('chats/setNeedScroll', true);
  });

  socketService.socket.on('chatsUpdateActivity', ({type, state, name}) => {
    const action = state === STATE_ACTIVE
      ? 'chats/addActivity'
      : 'chats/removeActivity';

    dispatch(action, {
      type,
      name,
    });
  });

  socketService.socket.on('chatsSetSeenMessages', ({ids}) => {
    commit('chats/setSeen', ids);
  });

  socketService.socket.on('chatsSetTranslations', ({translations}) => {
    dispatch('chats/addTranslations', translations);
  });

  context.watch(
      () => context.getters['chats/translateActive'],
      (locale) => {
        if (!locale) {
          dispatch('chats/setTranslations', {});

          return;
        }

        const messagesIds = [...getters['chats/messages']].map((message) => {
          return message.id;
        });

        dispatch('chats/loadTranslations', {
          ids: messagesIds,
          locale,
        });
      }
  );
};
