import Vue from 'vue';
import Vuex from 'vuex';
import socketService from '@js/services/socket';

import roomUsersList from './modules/room-users-list';
import calls from './modules/calls';
import rtc from './modules/rtc';
import broadcast from './modules/broadcast';
import user from './modules/user';
import record from './modules/record';
import sounds from './modules/sounds';
import permissions from './modules/permissions';
import app from './modules/app';
import dialogs from './modules/dialogs';
import chats from './modules/chats';
import chatAudio from './modules/chat-audio';
import files from './modules/files';
import widgets from './modules/widgets/index';

import socketPlugin from './plugins/socket';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    roomUsersList,
    calls,
    rtc,
    broadcast,
    user,
    record,
    sounds,
    permissions,
    app,
    dialogs,
    chats,
    chatAudio,
    files,
    widgets,
  },
  plugins: [
    socketPlugin(socketService),
  ],
});
