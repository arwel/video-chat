import socketService from '@js/services/socket';
import {
  UR_STATUS_ACTIVE,
} from '@shared/const/user-room-status';

const state = {
  currentUser: {},
  rooms: {},
};

const getters = {
  room: ({rooms}) => (id) => (rooms[id] || {
    status: UR_STATUS_ACTIVE,
    startStatusAt: null,
    stopStatusAt: null,
  }),
  rooms: ({rooms}) => rooms,
  currentUser({currentUser}) {
    return currentUser;
  },
};

const actions = {
  updateRoomRelation(_, {roomId, status}) {
    socketService.dispatch('room/updateCurrentUserRoomRelation', {
      roomId,
      status,
    });
  },
  loadCurrentUserRoomRelations({getters}) {
    const {id} = getters['currentUser'] || {};

    socketService.dispatch('room/getCurrentUserRoomRelations', {
      id,
    });
  },
  setRooms({commit}, rooms) {
    if (rooms instanceof Array) {
      const roomHashTab = {};
      for (const room of rooms) {
        roomHashTab[room.RoomId] = room;
      }
      commit('setRooms', roomHashTab);
    } else if (typeof rooms === 'object') {
      commit('setRooms', rooms);
    }
  },
  updateCurrentUser({commit}, payload) {
    commit('updateCurrentUser', payload);
  },
};
const mutations = {
  setRooms(state, rooms) {
    state.rooms = rooms;
  },
  updateCurrentUser(state, userData) {
    state.currentUser = {
      ...state.currentUser,
      ...userData,
    };
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
