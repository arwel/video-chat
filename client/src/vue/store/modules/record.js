import socketService from '@js/services/socket';
import recordService from '@js/services/record';
import {STATE_RECORDING} from '@js/const/state';

const state = {};

const getters = {};

const actions = {
  start(_, {slug, type}) {
    if (recordService.state === STATE_RECORDING) {
      return null;
    }

    recordService.addHandler(type, ({data}) => {
      socketService.dispatch('record/save', {
        slug,
        type,
        data,
      });
    });

    return recordService.start();
  },
  stop(_, {slug, type}) {
    if (recordService.state !== STATE_RECORDING) {
      return null;
    }

    recordService.removeHandler(type);

    return recordService.stop().then(() => {
      socketService.dispatch('record/close', {
        slug,
        type,
      });
    });
  },
};

const mutations = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
