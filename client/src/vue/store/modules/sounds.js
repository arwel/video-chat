const state = {
  incoming: null,
  outgoing: null
};

const getters = {
};

const actions = {
};

const mutations = {
  setIncoming(state, incoming) {
    state.incoming = incoming;
  },
  setOutgoing(state, outgoing) {
    state.outgoing = outgoing;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
