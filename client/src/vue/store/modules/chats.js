import socketService from '@js/services/socket';
import {
  TYPE_ANONYMOUS_CHAT,
  TYPE_CHAT,
  TYPE_GROUP_CHAT,
} from '@shared/const/room-types';
import {
  CONTEXT_MENU_HEADER,
  CONTEXT_MENU_IN_MESSAGE,
} from '@js/const/content-menu-types';
import eventBus from '@js/services/event';

const state = {
  messages: [],
  reply: null,
  edit: null,
  loading: false,
  lastLoadedCount: -1,
  needScroll: false,
  uploading: false,
  activities: {},
  activeMessage: null,
  translations: {},
  translateActive: false,
  contextMenuType: CONTEXT_MENU_HEADER,
};

const getters = {
  contextMenuType: ({contextMenuType}) => contextMenuType,
  translateActive: ({translateActive}) => translateActive,
  translation: ({translations}) => (id) => translations[id],
  translations: ({translations}) => translations,
  activeMessage({activeMessage}) {
    return activeMessage;
  },
  activities({activities}) {
    return activities;
  },
  messages({messages}) {
    return messages;
  },
  reply({reply}) {
    return reply;
  },
  edit({edit}) {
    return edit;
  },
  loading({loading}) {
    return loading;
  },
  lastLoadedCount({lastLoadedCount}) {
    return lastLoadedCount;
  },
  needScroll({needScroll}) {
    return needScroll;
  },
  uploading({uploading}) {
    return uploading;
  },
};

const actions = {
  setContextMenuType({commit}, type) {
    if ([
      CONTEXT_MENU_HEADER,
      CONTEXT_MENU_IN_MESSAGE,
    ].indexOf(type) < 0) {
      type = CONTEXT_MENU_HEADER;
    }

    commit('setContextMenuType', type);
  },
  loadTranslations(_, {ids, locale}) {
    socketService.dispatch('chat/translateMessages', {
      ids,
      locale,
    });
  },
  setTranslateActive({commit}, isActive) {
    commit('setTranslateActive', isActive);
  },
  addTranslations({commit}, translations) {
    let payload = {};
    if (translations instanceof Array) {
      for (const translation of translations) {
        payload[translation.messageId] = translation;
      }
    } else if (typeof translations === 'object') {
      payload = translations;
    }

    commit('addTranslations', payload);
  },
  setTranslations({commit}, translations) {
    if (typeof translations === 'object') {
      commit('setTranslations', translations);
    }
  },
  setActiveMessage({commit}, message) {
    commit('setActiveMessage', message);
  },
  setSeen({rootGetters}, {ids}) {
    const activeChat = rootGetters['dialogs/activeChat'];
    activeChat && socketService.dispatch('chat/setSeen', {
      ids,
      roomId: activeChat.id,
    });
  },
  sendMyActivity(_, {state, type, roomId}) {
    socketService.dispatch('chat/updateActivity', {
      state,
      type,
      roomId,
    });
  },
  addActivity({commit}, {type, name}) {
    commit('addActivity', {
      type,
      name,
    });
  },
  removeActivity({commit}, {type, name}) {
    commit('removeActivity', {
      type,
      name,
    });
  },
  setUploading({commit}, uploading) {
    commit('setUploading', !!uploading);
  },
  resetProps({commit}) {
    commit('resetProps');
  },
  setNeedScroll({commit}, flag) {
    commit('setNeedScroll', !!flag);
  },
  setLastLoadedCount({commit}, count) {
    commit('setLastLoadedCount', +count);
  },
  setLoading({commit}, payload) {
    commit('setLoading', !!payload);
  },
  setEdit({commit}, message) {
    commit('setEdit', message);
  },
  setReply({commit}, message) {
    commit('setReply', message);
  },
  getDialogs() {
    socketService.dispatch('room/getRooms', {
      type: [
        TYPE_CHAT,
        TYPE_ANONYMOUS_CHAT,
        TYPE_GROUP_CHAT,
      ],
    });
  },
  setActiveChat({commit, dispatch}, chat) {
    commit('setMessages', []);
    commit('setActiveChat', chat);
  },
  getChatMessages(_, {chatId, offset = 0}) {
    socketService.dispatch('chat/getMessages', {
      chatId,
      offset,
    });
  },
  send({getters, dispatch}, {message = '', roomId, files = []}) {
    dispatch('setUploading', true);

    const reply = getters['reply'] || {id: null};

    socketService.dispatch('chat/sendMessage', {
      message,
      roomId,
      files,
      replyId: reply.id,
    });
  },
  remove(_, {id}) {
    socketService.dispatch('chat/deleteMessage', {
      id,
    });
  },
  sendEdit(_, {message, id}) {
    socketService.dispatch('chat/sendEditMessage', {
      id,
      message,
    });
  },
  addMessage({commit, rootGetters, dispatch}, message) {
    const {stats} = rootGetters['dialogs/dialog'](message.roomId) || {};
    const {id} = rootGetters['dialogs/activeChat'] || {};
    const isCurrentChat = message.roomId === id;
    message.date = new Date(message.date);

    if (stats) {
      stats.lastMessage = message;
      !isCurrentChat && stats.notReadCount++;
      dispatch('dialogs/updateDialog', {
        id: message.roomId,
        updatedAt: message.date,
        stats,
      }, {root: true});
    }

    if (!isCurrentChat) {
      const newMessagesCount = rootGetters['widgets/newMessages/count'];
      dispatch('widgets/newMessages/setCount', newMessagesCount + 1, {root: true});
      dispatch('widgets/newMessages/addMessage', message, {root: true});
    }

    eventBus.dispatch('chats/addMessage', {
      isCurrentChat,
      message,
    });

    isCurrentChat && commit('addMessage', message);
  },
  setMessages({commit}, messages) {
    commit('setMessages', messages);
  },
};

const mutations = {
  setContextMenuType(state, type) {
    state.contextMenuType = type;
  },
  setTranslateActive(state, isActive) {
    state.translateActive = isActive;
  },
  addTranslations(state, translations) {
    state.translations = {
      ...state.translations,
      ...translations,
    };
  },
  setTranslations(state, translations) {
    state.translations = translations;
  },
  setActiveMessage(state, message) {
    state.activeMessage = message;
  },
  setSeen(state, ids) {
    state.messages = state.messages.map((message) => {
      if (ids.indexOf(message.id) > -1) {
        message.seen = true;
      }

      return message;
    });
  },
  removeActivity(state, {type, name}) {
    const activityUsers = state.activities[type];
    if (!activityUsers) {
      return;
    }

    activityUsers.delete(name);
    if (activityUsers.size < 1) {
      delete state.activities[type];
    }

    state.activities = {...state.activities};
  },
  addActivity(state, {type, name}) {
    if (!state.activities[type]) {
      state.activities[type] = new Set();
    }
    state.activities[type].add(name);

    state.activities = {...state.activities};
  },
  setUploading(state, uploading) {
    state.uploading = uploading;
  },
  resetProps(state) {
    state.messages = [];
    state.reply = null;
    state.edit = null;
    state.loading = false;
    state.lastLoadedCount = -1;
    state.needScroll = false;
    state.uploading = false;
    state.activities = {};
  },
  setNeedScroll(state, flag) {
    state.needScroll = flag;
  },
  setLastLoadedCount(state, count) {
    state.lastLoadedCount = count;
  },
  setLoading(state, payload) {
    state.loading = payload;
  },
  deleteMessage(state, id) {
    const index = state.messages.findIndex((msg) => {
      return id === msg.id;
    });
    if (index < 0) {
      return;
    }

    state.messages.splice(index, 1);
    state.messages = [...state.messages];
  },
  updateMessage(state, message) {
    const index = state.messages.findIndex((msg) => {
      return message.id === msg.id;
    });
    if (index < 0) {
      return;
    }

    state.messages[index].message = message.message;
    state.messages = [...state.messages];
  },
  setEdit(state, message) {
    state.edit = message;
  },
  setReply(state, message) {
    state.reply = message;
  },
  setDialogs(state, payload) {
    state.dialogs = payload;
  },
  setActiveChat(state, chat) {
    state.activeChat = chat;
  },
  setMessages(state, messages) {
    state.messages = messages;
  },
  addMessage(state, message) {
    state.messages.push(message);

    state.messages = [...state.messages];
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
