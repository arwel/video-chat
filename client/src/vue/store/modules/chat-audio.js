import recordService from '@js/services/record';
import {STATE_RECORDING} from '@js/const/state';
import RECORD_CONFIG from '@js/config/record';
import {STATE_ACTIVE, STATE_INACTIVE} from '@shared/const/state';
import {
  ACTIVITY_TYPING,
  ACTIVITY_VOICE_RECORDING,
} from '@shared/const/activities-types';

const RECORDING_TYPE = 'audio-chat';

const state = {
  recorded: [],
};

const getters = {
  recorded({recorded}) {
    return recorded;
  },
  mergedRecords({recorded}) {
    return new Blob(recorded, {
      type: RECORD_CONFIG.audioMimeType,
    });
  },
  recordingState() {
    return recordService.state;
  },
};

const actions = {
  start({commit, dispatch, rootGetters}) {
    if (recordService.state === STATE_RECORDING) {
      return null;
    }

    commit('setRecordedData', []);

    dispatch('chats/sendMyActivity', {
      roomId: rootGetters['dialogs/activeChat'].id,
      state: STATE_ACTIVE,
      type: ACTIVITY_VOICE_RECORDING,
    }, {root: true});

    recordService.addHandler(RECORDING_TYPE, ({data}) => {
      dispatch('addRecordedData', data);
    });

    return recordService.start({
      mimeType: RECORD_CONFIG.audioMimeType,
      captureVideo: false,
    });
  },
  stop({getters, dispatch, rootGetters}) {
    if (recordService.state !== STATE_RECORDING) {
      return null;
    }

    recordService.removeHandler(RECORDING_TYPE);

    dispatch('chats/sendMyActivity', {
      roomId: rootGetters['dialogs/activeChat'].id,
      state: STATE_INACTIVE,
      type: ACTIVITY_VOICE_RECORDING,
    }, {root: true});

    return recordService.stop().then(() => {
      const {id} = rootGetters['dialogs/activeChat'];

      dispatch('chats/send', {
        roomId: id,
        files: [
          {
            name: `record-message-${Date.now()}`,
            content: getters['mergedRecords'],
          },
        ],
      }, {root: true});
    });
  },
  addRecordedData({commit, getters}, data) {
    commit('setRecordedData', [
      ...getters['recorded'],
      data,
    ]);
  },
};

const mutations = {
  setRecordedData(state, data) {
    state.recorded = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
