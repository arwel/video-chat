import newMessages from '@vue/store/modules/widgets/new-messages';

export default {
  namespaced: true,
  modules: {
    newMessages,
  },
};
