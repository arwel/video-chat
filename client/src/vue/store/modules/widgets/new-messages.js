import socketService from '@js/services/socket';

const state = {
  count: 0,
  messages: [],
};

const getters = {
  count: ({count}) => count,
  messages: ({messages}) => messages,
};

const actions = {
  requestData() {
    socketService.dispatch('chat/getNewMessages');
  },
  setCount({commit}, count) {
    commit('setCount', count);
  },
  addMessage({getters, commit}, message) {
    commit('setMessages', [
      ...getters['messages'],
      message,
    ]);
  },
};

const mutations = {
  setCount(state, count) {
    state.count = +count;
  },
  setMessages(state, messages) {
    state.messages = messages;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
