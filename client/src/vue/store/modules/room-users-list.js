import socketService from '@js/services/socket';
import {STATUS_ONLINE} from '@js/const/status';

const state = {
  users: {},
  lastInviteNotificationTime: 0,
};
const getters = {
  lastInviteNotificationTime: ({lastInviteNotificationTime}) => lastInviteNotificationTime,
  users({users}) {
    return Object.values(users);
  },
  usersObject({users}) {
    return users;
  },
  onlineUsers({users}) {
    return Object.values(users).filter(({status}) => {
      return status === STATUS_ONLINE;
    });
  },
  onlineUsersObject({users}, {onlineUsers}) {
    const onlineUsersObj = {};
    for (const user of Object.values(onlineUsers)) {
      onlineUsersObj[user.id] = user;
    }

    return onlineUsersObj;
  },
  onlineUsersWithoutPeer(_, {onlineUsers}) {
    return onlineUsers.filter(({hasPeer}) => {
      return !hasPeer;
    });
  },
};
const actions = {
  setLastInviteNotificationTime({commit}, timestamp) {
    commit('setLastInviteNotificationTime', timestamp);
  },
  loadUsers({commit}, {roomSlug}) {
    socketService.dispatch('room/usersList', {
      slug: roomSlug,
    });
  },
  sendInviteNotification({getters}) {
    const ids = getters['onlineUsersWithoutPeer'].map(({id}) => id);
    socketService.dispatch('calls/notifyJoinCall', {ids});
  },
};
const mutations = {
  setLastInviteNotificationTime(state, timestamp) {
    state.lastInviteNotificationTime = +timestamp;
  },
  setUsers(state, users) {
    state.users = users;
  },
  updateUser(state, user) {
    state.users[user.id] = user;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
