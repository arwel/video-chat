const state = {
  activeUserList: false,
  fullScreen: false,
  masterFrame: null,
  serverToken: null,
  showMobileChat: false,
  chatRect: {
    top: 0,
    left: 0,
    height: 0,
  },
  readOnly: false,
};

const getters = {
  readOnly: ({readOnly}) => readOnly,
  activeUserList: ({activeUserList}) => activeUserList,
  fullScreen: ({fullScreen}) => fullScreen,
  masterFrame: ({masterFrame}) => masterFrame,
  serverToken: ({serverToken}) => serverToken,
  showMobileChat: ({showMobileChat}) => showMobileChat,
  chatRect: ({chatRect}) => chatRect,
};

const actions = {
  setReadOnly({commit}, payload) {
    commit('setReadOnly', payload);
  },
  setShowMobileChat({commit}, payload) {
    commit('setShowMobileChat', !!payload);
  },
  setFullScreen({commit}, payload) {
    commit('setFullScreen', !!payload);
  },
  setMasterFrame({commit}, payload) {
    commit('setMasterFrame', payload);
  },
  setServerToken({commit}, token) {
    commit('setServerToken', token);
  },
  setChatRect({commit}, height) {
    commit('setChatRect', height);
  },
};

const mutations = {
  setReadOnly(state, payload) {
    state.readOnly = payload;
  },
  setShowMobileChat(state, payload) {
    state.showMobileChat = payload;
  },
  setFullScreen(state, payload) {
    state.fullScreen = payload;
  },
  setMasterFrame(state, payload) {
    state.masterFrame = payload;
  },
  setServerToken(state, payload) {
    state.serverToken = payload;
  },
  setChatRect(state, height) {
    state.chatRect = height;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
