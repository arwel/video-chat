import {
  DEVICE_AUDIO_INPUT,
  DEVICE_VIDEO_INPUT,
} from '@shared/const/device-kinds';
import permissionsService from '@js/services/permissions';
import {
  PERMISSION_DENIED,
} from '@shared/const/permissions';
import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';
import socketService from '@js/services/socket';

const showDisabledMediaNotification = () => {
  notificationService.show({
    title: translationService.get('userMediaError'),
    body: translationService.get('userMediaAttention'),
  });
};

const permissionsChangeHandler = (commit, deviceType, {target: {state}}) => {
  const permission = state !== PERMISSION_DENIED;

  switch (deviceType) {
    case DEVICE_VIDEO_INPUT:
      commit('setHasCamera', permission);
      break;
    case DEVICE_AUDIO_INPUT:
      commit('setHasMic', permission);
      break;
  }
};

const state = {
  hasCamera: false,
  hasMic: false,
  online: true,
  reconnects: 3,
  tryReconnect: true,
};

const getters = {
  hasCamera({hasCamera}) {
    return hasCamera;
  },
  hasMic({hasMic}) {
    return hasMic;
  },
  hasMedia({hasMic, hasCamera}) {
    return hasMic || hasCamera;
  },
  isOnline({online}) {
    return online;
  },
  reconnects({reconnects}) {
    return reconnects;
  },
  tryReconnect({tryReconnect}) {
    return tryReconnect;
  },
};

const actions = {
  initPermissionsService({commit}) {
    return Promise.all([
      permissionsService.canUse(
          DEVICE_VIDEO_INPUT,
          permissionsChangeHandler.bind(null, commit, DEVICE_VIDEO_INPUT),
      ),
      permissionsService.canUse(
          DEVICE_AUDIO_INPUT,
          permissionsChangeHandler.bind(null, commit, DEVICE_AUDIO_INPUT),
      ),
    ]).then(([hasCamera, hasMic]) => {
      if (!hasCamera && !hasMic) {
        showDisabledMediaNotification();
      }

      socketService.dispatch('permissions/setPermissions', {
        [DEVICE_AUDIO_INPUT]: hasMic,
        [DEVICE_VIDEO_INPUT]: hasCamera,
      });

      commit('setHasCamera', hasCamera);
      commit('setHasMic', hasMic);
    });
  },
  setHasCamera({commit}, hasCamera) {
    commit('setHasCamera', !!hasCamera);
  },
  setHasMic({commit}, hasMic) {
    commit('setHasMic', !!hasMic);
  },
  setOnline({commit}, isOnline) {
    commit('setOnline', !!isOnline);
  },
  decreaseReconnects({commit}) {
    commit('decreaseReconnects');
  },
  setTryReconnect({commit}, tryReconnect) {
    commit('setTryReconnect', !!tryReconnect);
  },
};

const mutations = {
  setHasCamera(state, hasCamera) {
    state.hasCamera = hasCamera;
  },
  setHasMic(state, hasMic) {
    state.hasMic = hasMic;
  },
  setOnline(state, isOnline) {
    state.online = isOnline;
  },
  decreaseReconnects(state) {
    state.reconnects--;
  },
  setTryReconnect(state, tryReconnect) {
    state.tryReconnect = tryReconnect;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
