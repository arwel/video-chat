import socketService from '@js/services/socket';

const state = {
  dialogs: [],
  activeChat: null,
  loading: false,
  lastLoadedCount: -1,
  usersToInvite: [],
  activeDialogMembers: [],
};

const getters = {
  activeDialogMembers: ({activeDialogMembers}) => activeDialogMembers,
  usersToInvite: ({usersToInvite}) => usersToInvite,
  dialogs({dialogs}) {
    return dialogs;
  },
  dialog: ({dialogs}) => (id) => dialogs.find((dialog) => dialog.id === id),
  activeChat({activeChat}) {
    return activeChat;
  },
  loading({loading}) {
    return loading;
  },
  lastLoadedCount({lastLoadedCount}) {
    return lastLoadedCount;
  },
};

const actions = {
  removeMember({getters}, {id}) {
    const activeChat = getters['activeChat'] || {};
    socketService.dispatch('dialog/removeMember', {
      id,
      roomId: activeChat.id,
    });
  },
  loadActiveDialogMembers({getters}) {
    const {id} = getters['activeChat'] || {};

    socketService.dispatch('dialog/getMembers', {id});
  },
  setActiveDialogMembers({commit}, users) {
    commit('setActiveDialogMembers', users);
  },
  setUsersToInvite({commit}, users) {
    commit('setUsersToInvite', users);
  },
  findUsersToInvite(_, {query}) {
    socketService.dispatch('room/usersToInvite', {
      query,
    });
  },
  updateDialog({getters, commit}, newData) {
    if (!newData.id) {
      return;
    }

    let dialogs = getters['dialogs'];
    let updatedDialog = null;
    let oldDialogIndex = 0;
    for (const dialog of dialogs) {
      if (dialog.id === newData.id) {
        updatedDialog = {
          ...dialog,
          ...newData,
        };
        dialogs.splice(oldDialogIndex, 1);
        break;
      }

      oldDialogIndex++;
    }

    updatedDialog && commit('setDialogs', [
      updatedDialog,
      ...dialogs,
    ]);
  },
  invite({getters}, {id}) {
    const activeChatId = getters['activeChat'].id;
    socketService.dispatch('room/invite', {
      roomId: activeChatId,
      id,
    });
  },
  setLastLoadedCount({commit}, count) {
    commit('setLastLoadedCount', +count);
  },
  setLoading({commit}, flag) {
    commit('setLoading', !!flag);
  },
  getDialogs(_, {offset}) {
    socketService.dispatch('dialog/getDialogs', {
      offset,
    });
  },
  setActiveChat({commit, dispatch, getters}, chat) {
    if (!chat) {
      commit('setActiveChat', chat);

      return;
    }

    const stats = chat.stats;
    if (stats) {
      stats.notReadCount = 0;
      dispatch('updateDialog', {
        id: chat.id,
        stats,
      });
    }

    dispatch('chats/resetProps', null, {root: true})
        .then(dispatch('setActiveDialogMembers', []))
        .then(dispatch('chats/setLoading', true, {root: true}))
        .then(dispatch('chats/setActiveMessage', null, {root: true}))
        .then(commit('setActiveChat', chat))
        .then(dispatch('loadActiveDialogMembers', null))
        .then(dispatch('chats/getChatMessages', {
          chatId: chat.id,
          offset: 0,
        }, {root: true}))
        .then(() => {
          const isActive = stats && stats.members && stats.members.length > 0;

          return dispatch('app/setReadOnly', !isActive, {root: true});
        });
  },
  setActiveChatBySlug(_, {slug}) {
    socketService.dispatch('dialog/setActiveChatBySlug', {slug});
  },
  addDialogs({commit, getters}, dialogs) {
    const oldDialogs = getters['dialogs'];
    commit('setDialogs', [
      ...oldDialogs,
      ...dialogs,
    ]);
  },
  remove({getters, dispatch}, {id}) {
    const activeChat = getters['activeChat'];
    if (activeChat && activeChat.id === id) {
      dispatch('setActiveChat', null);
    }

    socketService.dispatch('room/removeMe', {
      roomId: id,
    });
  },
};

const mutations = {
  setActiveDialogMembers(state, users) {
    state.activeDialogMembers = users;
  },
  setUsersToInvite(state, users) {
    state.usersToInvite = users;
  },
  setLastLoadedCount(state, count) {
    state.lastLoadedCount = count;
  },
  setLoading(state, flag) {
    state.loading = flag;
  },
  setDialogs(state, payload) {
    state.dialogs = payload;
  },
  setActiveChat(state, chat) {
    state.activeChat = chat;
  },
  remove(state, {id}) {
    const index = state.dialogs.findIndex((dialog) => {
      return dialog.id === id;
    });
    state.dialogs.splice(index, 1);

    state.dialogs = [...state.dialogs];
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
