import socketService from '@js/services/socket';
import {STATUS_OFFLINE, STATUS_ONLINE, STATUS_PENDING} from '@js/const/status';
import {TYPE_BROADCAST} from '@shared/const/room-types';


const state = {
  status: STATUS_OFFLINE,
  broadcaster: null,
  isBroadcaster: false,
};

const getters = {
  status({status}) {
    return status;
  },
  broadcaster({broadcaster}) {
    return broadcaster;
  },
  isBroadcaster({isBroadcaster}) {
    return isBroadcaster;
  },
};

const actions = {
  updateStatus({dispatch}, {roomSlug, status}) {
    dispatch(status === STATUS_ONLINE ? 'record/start' : 'record/stop', {
      type: TYPE_BROADCAST,
      slug: roomSlug,
    }, {root: true});

    socketService.dispatch('broadcast/updateStatus', {
      roomSlug,
      status,
    });
  },
  init(_, {roomSlug}) {
    socketService.dispatch('broadcast/init', {
      roomSlug,
    });
  },
  requestOffer(_, {roomSlug}) {
    socketService.dispatch('broadcast/requestOffer', {
      roomSlug,
    });
  },
};

const mutations = {
  setBroadcaster(state, broadcaster) {
    state.broadcaster = broadcaster;
  },
  setIsBroadcaster(state, isBroadcaster) {
    state.isBroadcaster = !!isBroadcaster;
  },
  setStatus(state, payload) {
    state.status = [STATUS_ONLINE, STATUS_PENDING, STATUS_OFFLINE].indexOf(payload) >= 0
      ? payload
      : STATUS_OFFLINE;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
