import socketService from '@js/services/socket';

const state = {
  loading: {},
  loaded: {},
  forUpload: {},
};

const getters = {
  loading({loading}) {
    return loading;
  },
  loaded({loaded}) {
    return loaded;
  },
  forUpload({forUpload}) {
    return forUpload;
  },
};

const actions = {
  setForUpload({commit}, payload) {
    if (payload instanceof Array) {
      const files = {};
      for (const file of payload) {
        files[file.lastModified] = file;
      }
      commit('setForUpload', files);
    } else if (typeof payload === 'object') {
      commit('setForUpload', payload);
    }
  },
  addForUpload({commit}, file) {
    commit('addForUpload', file);
  },
  removeForUpload({commit}, file) {
    commit('removeForUpload', file);
  },
  startLoad({getters, commit}, {id}) {
    if (!getters['loading'][id] && !getters['loaded'][id]) {
      commit('setLoading', {id});
      socketService.dispatch('uploadedFiles/getContent', {
        id,
      });
    }
  },
  removeAll({commit}, {id}) {
    commit('removeLoading', {id});
    commit('removeLoaded', {id});
  },
};

const mutations = {
  setForUpload(state, payload) {
    state.forUpload = payload;
  },
  addForUpload(state, file) {
    state.forUpload = {
      ...state.forUpload,
      [file.lastModified]: file,
    };
  },
  removeForUpload(state, file) {
    delete state.forUpload[file.lastModified];

    state.forUpload = {...state.forUpload};
  },
  setLoading(state, {id}) {
    state.loading = {
      ...state.loading,
      [id]: id,
    };
  },
  removeLoading(state, {id}) {
    delete state.loading[id];

    state.loading = {...state.loading};
  },
  setLoaded(state, {id, content, mimeType}) {
    state.loaded = {
      ...state.loaded,
      [id]: URL.createObjectURL(new Blob([content], {type: mimeType})),
    };
  },
  removeLoaded(state, {id}) {
    delete state.loaded[id];

    state.loaded = {...state.loaded};
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
