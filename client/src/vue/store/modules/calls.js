import socketService from '@js/services/socket';
import {STATUS_OFFLINE, STATUS_ONLINE} from '@js/const/status';
import {filterObject, getObjectRem, objectLength} from '@js/util/etc';

const state = {
  incomingCalls: {},
  activeCalls: {},
  outgoingCalls: {},
  forceCalls: {},
  status: STATUS_OFFLINE,
};

const getters = {
  incomingCalls({incomingCalls}) {
    return Object.values(incomingCalls);
  },
  incomingCallsObject({incomingCalls}) {
    return incomingCalls;
  },
  activeCalls({activeCalls}) {
    return Object.values(activeCalls);
  },
  activeCallsObject({activeCalls}) {
    return activeCalls;
  },
  outgoingCalls({outgoingCalls}) {
    return Object.values(outgoingCalls);
  },
  outgoingCallsObject({outgoingCalls}) {
    return outgoingCalls;
  },
  forceCallsObject({forceCalls}) {
    return forceCalls;
  },
  status({status}) {
    return status;
  },
};

const actions = {
  forceCall(context, {id}) {
    socketService.dispatch('calls/forceCall', {
      id,
    });
  },
  outgoing({commit}, {id, isForce}) {
    commit('addOutgoingCall', id);
    socketService.dispatch('calls/outgoing', {
      receiverId: id,
      isForce,
    });
  },
  decline({dispatch}, id) {
    dispatch('removeIncomingCall', id);
    dispatch('removeOutgoingCall', id);

    socketService.dispatch('calls/decline', {
      id,
    });
  },
  removeIncomingCall({commit, dispatch}, id) {
    dispatch('removeForceCall', id);
    commit('removeIncomingCall', id);
  },
  removeForceCall({commit}, id) {
    commit('removeForceCall', id);
  },
  addActiveCall({commit}, id) {
    commit('addActiveCall', id);
  },
  addForceCall({commit}, id) {
    commit('addForceCall', id);
  },
  moveIncomingToActive({dispatch, state, getters}, id) {
    const isForce = !!getters['forceCallsObject'][id];

    dispatch('addActiveCall', id);
    dispatch('removeIncomingCall', id);
    dispatch('removeForceCall', id);

    if (isForce) {
      socketService.dispatch('calls/accept', {
        id,
      });
      dispatch('callToAll');

      return;
    }

    socketService.dispatch('calls/accept', {
      id,
    });
  },
  removeActiveCall({commit, dispatch}, id) {
    commit('removeActiveCall', id);
    dispatch('removeOutgoingCall', id);
    dispatch('removeIncomingCall', id);

    dispatch('rtc/closeStream', id, {root: true});
    dispatch('rtc/closeConnection', id, {root: true});

    socketService.dispatch('calls/end', {
      id,
    });
  },
  removeOutgoingCall({commit}, id) {
    commit('removeOutgoingCall', id);
  },
  setStatus({commit}, {status}) {
    commit('setStatus', status);
  },
  callToAll({rootGetters, dispatch, getters}) {
    const usersInRoom = filterObject(
        rootGetters['roomUsersList/onlineUsersObject'],
        (item) => {
          return item.status === STATUS_ONLINE;
        }
    );
    const activeCalls = getters['activeCallsObject'];
    const incomingCalls = getters['incomingCallsObject'];
    const outgoingCalls = getters['outgoingCallsObject'];
    const calls = {
      ...activeCalls,
      ...incomingCalls,
      ...outgoingCalls,
    };
    const freeUsers = getObjectRem(
        usersInRoom,
        Object.keys(calls)
    );

    const isForce = objectLength(activeCalls) > 0;

    for (const {id} of Object.values(freeUsers)) {
      dispatch('outgoing', {
        id,
        isForce,
      });
    }
  },
};

const mutations = {
  addForceCall(state, id) {
    state.forceCalls[id] = id;
    state.forceCalls = {...state.forceCalls};
  },
  addIncomingCall(state, id) {
    state.incomingCalls[id] = id;
    state.incomingCalls = {...state.incomingCalls};
  },
  removeIncomingCall(state, id) {
    delete state.incomingCalls[id];
    state.incomingCalls = {...state.incomingCalls};
  },
  addActiveCall(state, id) {
    state.activeCalls[id] = id;
    state.activeCalls = {...state.activeCalls};
  },
  removeActiveCall(state, id) {
    delete state.activeCalls[id];
    state.activeCalls = {...state.activeCalls};
  },
  addOutgoingCall(state, id) {
    state.outgoingCalls[id] = id;
    state.outgoingCalls = {...state.outgoingCalls};
  },
  removeForceCall(state, id) {
    delete state.forceCalls[id];
    state.forceCalls = {...state.forceCalls};
  },
  removeOutgoingCall(state, id) {
    delete state.outgoingCalls[id];
    state.outgoingCalls = {...state.outgoingCalls};
  },
  setStatus(state, status) {
    state.status = status;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
