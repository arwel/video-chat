import userMediaService from '@js/services/media';
import socketService from '@js/services/socket';
import notificationService from '@js/services/notification';
import translationService from '@js/services/translation';
import Peer from 'peerjs';
import PEER_CONFIG from '@js/config/peer';

const state = {
  connections: {},
  streams: {},
  currentPeer: null,
};

const getters = {
  streams({streams}) {
    return streams;
  },
  connections({connections}) {
    return connections;
  },
  currentPeer({currentPeer}) {
    return currentPeer;
  },
};

const actions = {
  callAddedHandler({commit}, {call}) {
    const userId = call.peer;

    commit('addConnection', {
      id: userId,
      rtc: call,
    });

    userMediaService.getStream().then((mediaStream) => {
      call.answer(mediaStream);
      call.on('stream', (remoteStream) => {
        commit('addStream', {
          id: userId,
          stream: remoteStream,
        });
      });
    });
  },
  createPeer({commit, dispatch}, {currentUser}) {
    const {host, p2pPort} = socketService.credentials;
    const peer = new Peer(`${currentUser.peer}`, {
      config: PEER_CONFIG,
      host: host.split('://')[1],
      port: p2pPort,
      secure: true,
    });
    peer.on('call', (call) => {
      dispatch('callAddedHandler', {call});
    });
    commit('setCurrentPeer', peer);
  },
  createOffer({commit, dispatch, getters, rootGetters}, {id}) {
    const peer = getters['currentPeer'];
    if (!peer) {
      return;
    }

    return userMediaService.getStream().then((mediaStream) => {
      const call = peer.call(`${id}`, mediaStream);

      if (!call) {
        const currentUser = rootGetters['user/currentUser'];
        socketService.socket.emit('forceLogout', {
          id: currentUser.id,
          peer: currentUser.peer,
        });

        console.log('call error');

        notificationService.show({
          title: translationService.get('error'),
          body: translationService.get('reloadPageTryAgain'),
        });

        return;
      }

      call.on('stream', (stream) => {
        commit('addStream', {
          id,
          stream,
        });
      });
      commit('addConnection', {id, rtc: call});

      return call;
    });
  },
  closeStream({commit}, id) {
    userMediaService.release();
    commit('closeStream', id);
  },
  closeConnection({commit}, id) {
    commit('closeConnection', id);
  },
  disconnect({commit}) {
    commit('disconnect');
  },
};

const mutations = {
  setCurrentPeer(state, peer) {
    state.currentPeer = peer;
  },
  addStream(state, {id, stream}) {
    state.streams[id] = stream;
    state.streams = {...state.streams};
  },
  addConnection(state, {id, rtc}) {
    state.connections[id] = rtc;
    state.connections = {...state.connections};
  },
  setConnectionAnswer(state, {id, answer, ice}) {
    const rtc = state.connections[id];
    if (!rtc) {
      throw new Error(`No connection with id ${id}`);
    }

    return new Promise((resolve) => {
      resolve(rtc.setRemoteDescription(answer));
    }).then(() => {
      return rtc.addIceCandidate(ice);
    });
  },
  closeStream(state, id) {
    const stream = state.streams[id];
    if (!stream) {
      return;
    }

    for (const track of stream.getTracks()) {
      track.stop();
    }

    delete state.streams[id];

    state.streams = {...state.streams};
  },
  closeConnection(state, id) {
    const connection = state.connections[id];
    if (!connection) {
      return;
    }

    state.connections[id].close && state.connections[id].close();

    delete state.connections[id];
    state.connections = {...state.connections};
  },
  disconnect(state) {
    userMediaService.closeForce();

    if (state.currentPeer) {
      console.log('destroy peer');

      state.currentPeer.disconnect();
      state.currentPeer.destroy();
    }

    state.currentPeer = null;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
