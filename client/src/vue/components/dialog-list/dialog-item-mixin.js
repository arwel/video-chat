import {TYPE_ANONYMOUS_CHAT} from '@shared/const/room-types';
import {STATUS_ONLINE} from '@shared/const/status';

export default {
  computed: {
    isDialogAlive() {
      return this.dialog.type === TYPE_ANONYMOUS_CHAT ||
          this.stats.members.length < 1;
    },
    stats() {
      return this.dialog.stats || {
        members: [],
        notReadCount: 0,
        lastMessage: null,
      };
    },
    title() {
      return this.isDialogAlive
        ? this.dialog.title
        : this.stats.members.map(({nick}) => nick).join(', ');
    },
    previews() {
      return this.isDialogAlive
        ? [
          {
            id: Date.now(),
            status: STATUS_ONLINE,
            avatar: null,
            nick: this.dialog.title,
          },
        ]
        : this.stats.members;
    },
  },
  methods: {
    previewsClass(prefix) {
      return `${prefix} avatar-previews-${this.previews.length}`;
    },
  },
};
