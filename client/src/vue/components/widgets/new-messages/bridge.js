import component from './index';
import renderer from '../../panels/component-renderer';

export default renderer(component);
