import Preloader from '@vue/components/loading/preloader';

export default {
  props: {
    file: {
      type: Object,
      required: true,
    },
    message: {
      type: Object,
      required: true,
    },
  },
  components: {
    preloader: Preloader,
  },
  computed: {
    loadedContentUrl() {
      return this.$store.getters['files/loaded'][this.file.id];
    },
    canShow() {
      return !!this.loadedContentUrl;
    },
  },
  mounted() {
    this.$store.dispatch('files/startLoad', {
      id: this.file.id,
    });
  },
  beforeDestroy() {
    this.$store.dispatch('files/removeAll', {
      id: this.file.id,
    });
  },
};
