import Vue from 'vue';
import store from '@vue/store';
import socket from '@js/services/socket/vue-service-bridge';
import notify from '@js/services/notification/vue-service-bridge';
import userMedia from '@js/services/media/vue-service-bridge';
import t from '@js/services/translation/vue-service-bridge';
import params from '@js/services/params/vue-service-bridge';
import eventBus from '@js/services/event/vue-service-bridge';
import TextareaAutosize from 'vue-textarea-autosize';

Vue.use(TextareaAutosize);

export default (vueComponent) => {
  return (wrapper, {roomSlug, locale, serverToken, chatSlug}) => {
    store.dispatch('app/setServerToken', serverToken);

    const payload = {};
    if (roomSlug) {
      payload.roomSlug = roomSlug;
    }

    if (chatSlug) {
      payload.chatSlug = chatSlug;
    }

    store.dispatch('user/updateCurrentUser', payload);

    (new Vue({
      render: (h) => h(vueComponent),
      store,
      socket,
      notify,
      userMedia,
      t,
      params,
      eventBus,
    })).$mount(wrapper);
  };
};
