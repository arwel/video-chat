import component from './read-messages';
import renderer from './component-renderer';

export default renderer(component);
