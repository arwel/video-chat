import BroadcastPanel from './broadcast';
import renderer from './component-renderer';

export default renderer(BroadcastPanel);
