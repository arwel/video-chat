import component from './chat';
import renderer from './component-renderer';

export default renderer(component);
