import component from './video-chat';
import renderer from './component-renderer';

export default renderer(component);
