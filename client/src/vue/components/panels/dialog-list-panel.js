import component from './dialog-list';
import renderer from './component-renderer';

export default renderer(component);
