const {
  TIME_SPAN_YEAR,
  TIME_SPAN_MONTH,
  TIME_SPAN_DAY,
  TIME_SPAN_HOUR,
  TIME_SPAN_MINUTE,
  TIME_SPAN_SECOND,
} = require('../const/time-span');

const normalizeChatMessage = (message) => {
  message.date = new Date(message.date || '');

  return message;
};

const singleTimeAgo = (date) => {
  const SECONDS_IN_YEAR = 31536000;
  const SECONDS_IN_MONTH = 2592000;
  const SECONDS_IN_DAY = 86400;
  const SECONDS_IN_HOUR = 3600;
  const SECONDS_IN_MINUTE = 60;

  let timeSpan = ((new Date()).getTime() / 1000 - date.getTime() / 1000);

  if (timeSpan < 0) {
    timeSpan = 0;
  }

  if (timeSpan > SECONDS_IN_YEAR) {
    return {
      count: Math.floor(timeSpan / SECONDS_IN_YEAR),
      measure: TIME_SPAN_YEAR,
    };
  } else if (timeSpan > SECONDS_IN_MONTH) {
    return {
      count: Math.floor(timeSpan / SECONDS_IN_MONTH),
      measure: TIME_SPAN_MONTH,
    };
  } else if (timeSpan > SECONDS_IN_DAY) {
    return {
      count: Math.floor(timeSpan / SECONDS_IN_DAY),
      measure: TIME_SPAN_DAY,
    };
  } else if (timeSpan > SECONDS_IN_HOUR) {
    return {
      count: Math.floor(timeSpan / SECONDS_IN_HOUR),
      measure: TIME_SPAN_HOUR,
    };
  } else if (timeSpan > SECONDS_IN_MINUTE) {
    return {
      count: Math.floor(timeSpan / SECONDS_IN_MINUTE),
      measure: TIME_SPAN_MINUTE,
    };
  }

  return {
    count: Math.floor(timeSpan),
    measure: TIME_SPAN_SECOND,
  };
};

const fixEntityDate = (entity) => {
  if (entity.createdAt) {
    entity.createdAt = new Date(entity.createdAt);
  }

  if (entity.updatedAt) {
    entity.updatedAt = new Date(entity.updatedAt);
  }

  return entity;
};

const dateToStr = (date) => {
  let month = `${date.getMonth() + 1}`;
  if (month.length < 2) {
    month = `0${month}`;
  }

  let day = `${date.getUTCDate()}`;
  if (day.length < 2) {
    day = `0${day}`;
  }

  let hours = `${date.getHours()}`;
  if (hours.length < 2) {
    hours = `0${hours}`;
  }

  let minutes = `${date.getMinutes()}`;
  if (minutes.length < 2) {
    minutes = `0${minutes}`;
  }

  let seconds = `${date.getSeconds()}`;
  if (seconds.length < 2) {
    seconds = `0${seconds}`;
  }

  return `${date.getFullYear()}-${month}-${day}T${hours}:${minutes}:${seconds}`;
};

module.exports = {
  normalizeChatMessage,
  singleTimeAgo,
  fixEntityDate,
  dateToStr,
};
