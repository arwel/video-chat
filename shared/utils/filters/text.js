module.exports = {
  filterToPlainText(text) {
    return text
        .trim()
        .replace(/([\n\t\r])+/i, '')
        .replace(/\s{2,}/i, ' ')
        .replace(/(<.+>)?/i, '');
  },
};
