const {getBlobMimeData} = require('../../../server/app/utils/fs');
const {
  ALLOWED_UPLOAD_FILE_SIZE,
  ALLOWED_UPLOAD_FILE_TYPES,
} = require('../../../shared/const/threasholds');

const validateFiles = (files, rules) => {
  const errors = [];

  for (const file of files) {
    for (const rule of rules) {
      const validator = typeof rule === 'function' ? rule : validators[rule];
      if (!validator) {
        continue;
      }

      const result = validator(file);
      result && errors.push(result);
    }
  }

  return errors;
};

const validators = {
  size(file) {
    return file.content.length > ALLOWED_UPLOAD_FILE_SIZE
      ? {
        name: file.name,
        error: 'File is too large',
      }
      : null;
  },
  mimeType(file) {
    const {mime} = getBlobMimeData(file.content);
    return ALLOWED_UPLOAD_FILE_TYPES.indexOf(mime) < 0
      ? {
        name: file.name,
        error: 'File type is not allowed',
      }
      : null;
  },
};

module.exports = {
  ...validators,
  validateFiles,
};
