const validateEmail = (text) => {
  return /^[a-zA-Z0-9_\-.]{3,}@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/i.test(text);
};

module.exports = {
  validateEmail,
};
