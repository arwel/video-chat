module.exports = {
  TIME_SPAN_YEAR: 'year',
  TIME_SPAN_MONTH: 'month',
  TIME_SPAN_DAY: 'day',
  TIME_SPAN_HOUR: 'hour',
  TIME_SPAN_MINUTE: 'minute',
  TIME_SPAN_SECOND: 'second',
};
