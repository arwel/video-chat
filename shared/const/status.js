module.exports = {
  STATUS_ONLINE: 'online',
  STATUS_OFFLINE: 'offline',
  STATUS_PENDING: 'pending',
  STATUS_ACTIVE: 'active',
  STATUS_DELETED: 'deleted',
  STATUS_BLOCKED: 'blocked',
};
