module.exports = {
  STATE_ACTIVE: 'active',
  STATE_INACTIVE: 'inactive',
  STATE_RECORDING: 'recording',
  STATE_PAUSED: 'paused',
  STATE_DISCONNECTED: 'disconnected',
};
