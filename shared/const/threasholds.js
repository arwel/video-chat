module.exports = {
  JWT_LIFETIME: 43200,
  ITEMS_PER_PAGE: 20,
  ALLOWED_UPLOAD_FILE_SIZE: 31457280, // 30Mb
  ALLOWED_UPLOAD_FILE_TYPES: [
    'image/jpeg',
    'image/jpg',
    'image/gif',
    'image/png',
    'audio/webm',
    'audio/mp3',
    'audio/wav',
    'video/webm',
    'video/mp4',
    'application/pdf',
  ],
  ALLOWED_UPLOAD_FILES_COUNT: 5,
};
