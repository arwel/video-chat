module.exports = {
  PERMISSION_DENIED: 'denied',
  PERMISSION_PROMPT: 'prompt',
  PERMISSION_GRANTED: 'granted',
  PERMISSION_DEFAULT: 'default',
};
