module.exports = {
  TYPE_BROADCAST: 'broadcast',
  TYPE_CALL: 'call',
  TYPE_CHAT: 'chat',
  TYPE_ANONYMOUS_CHAT: 'anonymous_chat',
  TYPE_GROUP_CHAT: 'group_chat',
};
