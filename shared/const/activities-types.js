module.exports = {
  ACTIVITY_TYPING: 'typing',
  ACTIVITY_VOICE_RECORDING: 'voice_recording',
  ACTIVITY_FILES_UPLOADING: 'files_uploading',
};
