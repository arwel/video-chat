const {DEVICE_AUDIO_INPUT, DEVICE_VIDEO_INPUT} = require('./device-kinds');
const {PERM_NAME_CAMERA, PERM_NAME_MICROPHONE} = require('./permission-names');

const map = {};
map[DEVICE_VIDEO_INPUT] = PERM_NAME_CAMERA;
map[DEVICE_AUDIO_INPUT] = PERM_NAME_MICROPHONE;

module.exports = map;
